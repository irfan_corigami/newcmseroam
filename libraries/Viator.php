<?php
namespace Libraries;
use GuzzleHttp\Client;

class Viator
{

	public function __construct(){
            $this->url  = config('viator_variables.viator_url');
	 		$this->api_key  = config('viator_variables.api_key');
    }


    public function activities($data){
    	$path = '/search/products/';
    	return $this->process_request($data,'post',$path,null);
    }
    
    public function bookActivities($data){
        $path = '/booking/book';
        return $this->process_request($data,'book',$path,null);
    }
    public function getTourCode($data){
        $path = '/booking/availability/tourgrades';
        return $this->process_request($data,'book',$path,null);
    }

    public function hotels($data){
    	$path = '/booking/hotels/';
    	if(isset($data['destId'])){
    		$string = '&destId='.$data['destId'];
    	}else{
    		$string = '&productCode='.$data['productCode'];
    	}

    	

    	return $this->process_request(null,'get',$path,$string);
    }

    public function location(){
    	$path = '/taxonomy/locations';	    
	    return $this->process_request(null,'get',$path,null);
    }

    public function activity_view($data){
    	$path = '/product';
    	$string = '&code='.$data['code'].'&currencyCode=AUD';
    	return $this->process_request(null,'get',$path,$string);
    }
    

    public function activity_cache_image($data){
        $path = '/product/photos';
        $string = '&code='.$data['code'].'&topX=1-15&showUnavailable=false';
        return $this->process_request(null,'get',$path,$string);
    }


    public function calculate_price($data){
        $path = '/booking/calculateprice';
        return $this->process_request(null,'post',$path,null);

    }

    public function getTourDatesRates($data) // add by dhara chauhan
    {
        $path = "/booking/availability/tourgrades/pricingmatrix/";
        return $this->process_request($data,'post',$path,null);
    }

    public function save_category(){
        $path = '/taxonomy/categories';
        return $this->process_request(null,'get',$path,null);

    }

    public function process_request($body,$method,$path,$string){
    	$client = new Client();
        // return  Response::json(array(
        //          'status' => 200,
        //          'data' => []
        //     ));

    	switch ($method) {
    		case 'get':
    		    
    			$response = $client->get( $this->url.$path.'?apiKey='.$this->api_key.$string);
    			
    			break;
    		
    		case 'post':

    		    $response = $client->post( $this->url.$path.'?apiKey='.$this->api_key,[
			    'headers' => ['Content-Type' => 'application/json','Accept' => 'application/json'],"body" => json_encode($body)]);
    			break;

            case 'book':
         
            $response = $client->post( $this->url.$path.'?apiKey='.$this->api_key,[
            'headers' => ['Content-Type' => 'application/json','Accept' => 'application/json'],"body" => $body]);

            break;  
    	}
        if($method == 'book'){
            $response = $response->json();
            return $response;
        }else{
            $status = $response->getStatusCode();
            $response = $response->json();
            
            return  Response::json(array(
                 'status' => $status,
                 'data' => $response['data']
            ));
        }
    	
    }
	
}