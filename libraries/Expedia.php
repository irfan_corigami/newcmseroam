<?php
namespace Libraries;
use Carbon\Carbon;
use App\HotelMapping;
use App\CardInfo;

/*
	All method name containing v2 in last is call from front-end
*/
class Expedia
{

	public static function getConfig($key){
        //parent::__construct();
        $config['eroamPercentage'] = 20;
        $config['host'] = 'http://api.ean.com/';
        $config['apiKey'] = '1jn4t7g731vbgetaifib3skism';
        $config['secret'] = '5c6s845m2vhid';
        $config['cid'] = '502698';
        $config['minorRev'] = '30';
        $config['ver'] = 'v3/';
        $config['path'] = "ean-services/rs/hotel/{$config['ver']}";
        $config['locale'] = "en_AU";
        $config['currencyCode'] = "AUD";
        $config['timestamp'] = gmdate('U');
        $config['sig'] = md5($config['apiKey'] . $config['secret'] . $config['timestamp']);
        $config['customerIpAddress'] = $_SERVER['REMOTE_ADDR'];
        $config['query'] = "?cid={$config['cid']}&apikey={$config['apiKey']}&sig={$config['sig']}&minorRev={$config['minorRev']}&locale={$config['locale']}&currencyCode={$config['currencyCode']}&customerIpAddress={$config['customerIpAddress']}";

        return $config[$key];	      
	}

	/**
	 * Display a listing of the Hotel Resource.
	 *
	 * @return Response
	 */
	
	public static function getHotels($data='')
	{


		/*$hotel_request_data['rooms'] = 2;
        $hotel_request_data['search_input']['num_of_adults'] = array(1,1,1);
        $hotel_request_data['child'] = array('0'=>array(2,3));
        $hotel_request_data['search_input']['start_location'] = 'Esperance, Australia';
        $hotel_request_data['date_from'] = '2018-05-03';
        $hotel_request_data['city_default_night'] = 2;
        $hotel_request_data['searchCityName'] ='Perth';
        $hotel_request_data['searchCountryCode'] = 'Au';*/

  		
		$numberOfResults 	= '30';
        $method 			= 'list';
        $hotel_request_data = $data;

        $starRatingStr = '';
		$starRating = '';
  		$preferences = isset($hotel_request_data['search_input']['travel_preferences']) ? $hotel_request_data['search_input']['travel_preferences'] : array();
		if(isset($preferences[0]['accommodation'][0])){
			$starRating = $preferences[0]['accommodation'][0];
			if($starRating == 5){
				$starRating = 2;
			}elseif($starRating == 2){
				$starRating = 3;
			}elseif($starRating == 3){
				$starRating = 4;
			}elseif($starRating == 9){
				$starRating = 5;
			}
			$starRatingStr = '&minStarRating='.$starRating.'&maxStarRating='.$starRating.'';
			if($starRating == 0){
				$starRatingStr = '';
			}
		}
        $includeDetails = 'true';
	    $options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';			
        $rateType= 'MerchantPackage';
        $num_of_rooms = $hotel_request_data['rooms'];
        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }
        
        $expedia_request  = '';
        
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.$hotel_request_data['search_input']['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.$hotel_request_data['search_input']['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
        
        $city = str_replace(" ","",$hotel_request_data['searchCityName']);
        $code = $hotel_request_data['searchCountryCode'];
        $arrivalDate 		= date('m/d/Y', strtotime($hotel_request_data['date_from'])); 
		$departureDate		= date('m/d/Y', strtotime($hotel_request_data['date_from'].'+'.$hotel_request_data['city_default_night'].'Days'));
    	$query  = self::getConfig('query');
    	
    	$query .= "&rateType={$rateType}&city={$city}&countryCode={$code}";
    	$hotelIdList = self::expediaMappedHotel($hotel_request_data['searchCityId']);
    		if(!empty($hotelIdList)){
    			$query .= "&hotelIdList={$hotelIdList}";
    		}
    	
    	$query .= $starRatingStr;
    	$query .= "&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$expedia_request}&numberOfResults={$numberOfResults}";
    	// initiate curl
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::getConfig('host').self::getConfig('path')."{$method}" . $query);
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		// close curl
		curl_close($ch);
		

		if ($headers['http_code'] != '200') {
			$data = '';
			return $data;

		}else{
			$status = $headers['http_code'];
			$data1 = json_decode($data);
			if($data1->HotelListResponse && array_key_exists('EanWsError', $data1->HotelListResponse)){
				$data = '';
				return $data;
			}else{
				return $data;
			}
		}
	}

	public static function getHotels_v2($data='')
	{
		//For prefernces
		
		$preferences = isset($data['travel_preferences']) ? $data['travel_preferences'] : array();

		$starRatingStr = '';
		$starRating = '';
		if(isset($preferences[0]['accommodation'][0])){
			$starRating = $preferences[0]['accommodation'][0];
			if($starRating == 5){
				$starRating = 2;
			}elseif($starRating == 2){
				$starRating = 3;
			}elseif($starRating == 3){
				$starRating = 4;
			}elseif($starRating == 9){
				$starRating = 5;
			}
			$starRatingStr = '&minStarRating='.$starRating.'&maxStarRating='.$starRating.'';
			if($starRating == 0){
				$starRatingStr = '';
			}
		}

        $numberOfResults 	= 30;
        $method 			= 'list';
        $rateType= 'MerchantPackage';
        $customerSessionId = $data['customerSessionId'];
        $num_of_rooms = $data['rooms'];
        $hotel_request_data = $data['search_input'];

        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.$hotel_request_data['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.$hotel_request_data['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}
      	
		$nights = 0;
        $nights = $data['city']['default_nights'];
        $cityId = $data['city']['id'];

        $numberOfAdults     =$data['numberOfAdults'];
        $arrivalDate 		= date('m/d/Y', strtotime($data['arrivalDate'])); 
		$departureDate		= date('m/d/Y', strtotime($data['departureDate']));

    	$query  = self::getConfig('query');
    	$query .= "&customerSessionId={$customerSessionId}&rateType={$rateType}&city={$data['city']['name']}&countryCode={$data['countryCode']}";
    	
    	$hotelIdList = self::expediaMappedHotel($cityId);
		if(!empty($hotelIdList)){
			$query .= "&hotelIdList={$hotelIdList}";
		}
    	$query .= $starRatingStr;
    	if(isset($data['cacheKey']) && !empty($data['cacheKey']) && isset($data['cacheLocation']) && !empty($data['cacheLocation'])){
    		$numberOfResults = 30;
    		$cacheKey = $data['cacheKey'];
    		$cacheLocation = $data['cacheLocation'];
    		$query .= "&cacheKey={$cacheKey}&cacheLocation={$cacheLocation}&supplierType=E";
    	}
    	$query .= "&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$expedia_request}&numberOfResults={$numberOfResults}";

    	// initiate curl
    	//echo $this->host . $this->path."{$method}" . $query;exit;
    	//$file_path_query = storage_path()."\expedia_logs\query-".time().'.txt';
		//File::put($file_path_query,$this->host . $this->path."{$method}" . $query);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::getConfig('host') . self::getConfig('path')."{$method}" . $query);
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		

		// close curl
		curl_close($ch);

		if ($headers['http_code'] != '200') {
			$data = [];
			return $data;
		} else {
			$status = $headers['http_code'];
			$temp_result = json_decode($data,true);
			$data = json_decode($data);
			

			if($data->HotelListResponse && array_key_exists('EanWsError', $data->HotelListResponse)){
		 	 	$data = [];
				return $data;	
		 	} else {

		 		if(!isset($temp_result['HotelListResponse']['HotelList']['HotelSummary'][0])){
        	 		$HotelSummary['0'] = $temp_result['HotelListResponse']['HotelList']['HotelSummary'];
        	 		$hotelLists = $HotelSummary;
        	 		$hotelLists = json_decode(json_encode($hotelLists));
        	 	}else{
        	 		$hotelLists= $data->HotelListResponse->HotelList->HotelSummary;
        	 	}
				
				$hotels  = array();

				if($hotelLists){
						$hotel['customerSessionId'] =  $data->HotelListResponse->customerSessionId;
						$hotel['moreResultsAvailable'] =  $data->HotelListResponse->moreResultsAvailable;
						$hotel['cacheKey'] =  $data->HotelListResponse->cacheKey;
						$hotel['cacheLocation'] =  $data->HotelListResponse->cacheLocation;
					 	foreach ($hotelLists as $key => $hotelList ) { 
					 		if(isset($hotelList->thumbNailUrl)){
						 		$hotel['hotelId'] =  $hotelList->hotelId;
						 		$hotel['code'] =  $hotelList->hotelId;
								$hotel['name'] = $hotelList->name;
								$hotel['address'] = $hotelList->address1;
								$hotel['city'] = $hotelList->city;
								$hotel['stateProvinceCode'] = '';//$hotelList->stateProvinceCode;
								if(array_key_exists('postalCode', $hotelList)){
									$hotel['postalCode'] = $hotelList->postalCode;
								}else{
									$hotel['postalCode'] = '';
								}
								$hotel['hotelRating'] = $hotelList->hotelRating;
								$hotel['propertyCategory'] = $hotelList->propertyCategory;
								$hotel['rateCurrencyCode'] = $hotelList->rateCurrencyCode;
								$hotel['description'] = html_entity_decode($hotelList->shortDescription);
								$hotel['image'] = self::changeImage("http://media.expedia.com".$hotelList->thumbNailUrl);
			
								$hotel['highRate'] = $hotelList->highRate;
								$hotel['lowRate'] = $hotelList->lowRate;
								$hotel['latitude'] = $hotelList->latitude;
								$hotel['longitude'] = $hotelList->longitude;
								$hotel['countryCode'] = $hotelList->countryCode;
								$hotel['hotelRating'] = $hotelList->hotelRating;
								$hotel['hotelRatingDisplay'] = $hotelList->hotelRatingDisplay;

								$hotel['rooms'] = [];

								$groups = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo;//RoomGroup->Room->ChargeableNightlyRates;
								$i=0;
								if(count($groups->ChargeableRateInfo) > 0){

									$hotel['rooms'][$i]['roomTypeCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->roomTypeCode;
		                            $hotel['rooms'][$i]['rateCode'] = $hotelList->RoomRateDetailsList->RoomRateDetails->rateCode;
		                            $hotel['rooms'][$i]['maxRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->maxRoomOccupancy;
		                            $hotel['rooms'][$i]['quotedRoomOccupancy'] = $hotelList->RoomRateDetailsList->RoomRateDetails->quotedRoomOccupancy;
		                           
		                            $hotel['rooms'][$i]['roomDescription']  = $hotelList->RoomRateDetailsList->RoomRateDetails->roomDescription;
		                           
		                            $hotel['rooms'][$i]['expediaPropertyId'] = $hotelList->RoomRateDetailsList->RoomRateDetails->expediaPropertyId;
									
	                                $hotel['rooms'][$i]['currentAllotment'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->currentAllotment;
	                                $hotel['rooms'][$i]['nonRefundable'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->nonRefundable;
		                            $hotel['rooms'][$i]['rateType'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->rateType;
	                                $temp_array = json_decode(json_encode($hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup),true);
	                                if(isset($temp_array['Room'][0])){
	                                	$nights = count($temp_array['Room'][0]['ChargeableNightlyRates']);
	                                	$hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room[0]->rateKey;
	                                }else{
	                                	$nights = count($temp_array['Room']['ChargeableNightlyRates']);
	                                	$hotel['rooms'][$i]['rateKey'] = $hotelList->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->RoomGroup->Room->rateKey;
	                                }
	                               
									
									foreach ($groups->ChargeableRateInfo as $key1 => $val1){
										
										if($key1 == '@total'){
											$eroamPercentage = self::getConfig('eroamPercentage');
											$total = (($val1 * $eroamPercentage) / 100) + $val1;
											$total = $total / $numberOfAdults; 
											$total = $total / $nights; 
											$hotel['rooms'][$i]['rate'] = round($total,2);
											$hotel['rooms'][$i]['exptotal'] = $val1;
											break;
										}
									}

									$hotels[] = $hotel;	
								}
							
					 		}
					 	}
					
				}

				return $hotels;
			}
		}
	}
	public static function getHotelDetails($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,$type=1,$rateCode=''){
        
        $apiExperience = 'PARTNER_WEBSITE';
    	$query  = self::getConfig('query')."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    	if($type == 1){
    		$query  .= "&includeDetails={$includeDetails}&options={$options}";
    		$return_data = self::callExpediaAvaibility($query);
    	}else{
    		$query  .= "&rateCode={$rateCode}&roomTypeCode={$roomTypeCode}&includeDetails={$includeDetails}&options={$options}";
    		$return_data = self::callExpediaAvaibility($query);
    		$data = $return_data['data'];
    		$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
    		if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){
				$query = self::getConfig('query')."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    			$query  .= "&includeDetails={$includeDetails}&options={$options}";
    			$return_data = self::callExpediaAvaibility($query);
    		}else{
    			$hotelDetail = $data->HotelRoomAvailabilityResponse;
		 		return json_encode(array(
					'status' => 200,
					'data' => $hotelDetail));		
    		}
    	}

    	$query  .= "&includeDetails={$includeDetails}&options={$options}";
    	$return_data = self::callExpediaAvaibility($query);
    	$headers = $return_data['headers'];
    	$data = $return_data['data'];

		if ($headers['http_code'] != '200') {
			$data = [];
		 	return $data;
		} else {

				$data = json_decode($data);
				$data->latitude = $latitude;
				$data->longitude = $longitude;
				if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){		 	 	
					$data = [];
		 			return $data;
		 	} else {
		 		$hotelDetail = $data->HotelRoomAvailabilityResponse;
			
		 		return $hotelDetail;
			}
		}
		
	}
	//Function to get expedia hotels mapped in eroam
	public static function expediaMappedHotel($id){
		$results = HotelMapping::select('EANHotelID')->where('city_id',$id)->get();

		$hotelId = array();
		foreach ($results as $key => $value) {
			array_push($hotelId, $value->EANHotelID);
		}
		$hotelId = implode(',',$hotelId);
		return $hotelId;
	}

	public static function changeImage($uri){
		$hotelImage = '';
		$hotelImage1 = $uri;
		$hotelImage2 = str_replace("_t.", "_y.", $uri);
		$hotelImage3 = str_replace("_t.", "_s.", $uri);

		if (self::existsImage($hotelImage2) == 200) {
			 $hotelImage = $hotelImage2;
		} elseif(self::existsImage($hotelImage3) == 200) {
			 $hotelImage = $hotelImage3;
		} else {
			 $hotelImage = $hotelImage1;
		}
		return $hotelImage;
	}

	public static function existsImage($uri)
	{
	    $ch = curl_init($uri);
	    curl_setopt($ch, CURLOPT_NOBODY, true);
	    curl_exec($ch);
	    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);

	    return $code == 200;
	}

	public static function get_hotel_details_v2($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,$type=1,$rateCode=''){
        
        $apiExperience = 'PARTNER_WEBSITE';
    	$query  = self::getConfig('query')."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    	if($type == 1){
    		$query  .= "&includeDetails={$includeDetails}&options={$options}";
    		$return_data = self::callExpediaAvaibility($query);
    	}else{
    		$query  .= "&rateCode={$rateCode}&roomTypeCode={$roomTypeCode}&includeDetails={$includeDetails}&options={$options}";
    		$return_data = self::callExpediaAvaibility($query);
    		$data = $return_data['data'];
    		$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
    		if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse)){
				$query  = self::getConfig('query')."&customerSessionId={$customerSessionId}&apiExperience={$apiExperience}&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}{$room}";
    			$query  .= "&includeDetails={$includeDetails}&options={$options}";
    			$return_data = self::callExpediaAvaibility($query);
    		}else{
    			$hotelDetail = $data->HotelRoomAvailabilityResponse;
		 		return $hotelDetail;		
    		}
    	}
    	$query  .= "&includeDetails={$includeDetails}&options={$options}";
    	$return_data = self::callExpediaAvaibility($query);
    	$headers = $return_data['headers'];
    	$data = $return_data['data'];
		if ($headers['http_code'] != '200') {
		 		$data = [];
				return $data;
		} else {

			$data = json_decode($data);
			$data->latitude = $latitude;
			$data->longitude = $longitude;
			if($data->HotelRoomAvailabilityResponse && array_key_exists('EanWsError', $data->HotelRoomAvailabilityResponse))
			{		 	 	
					$data = [];
					return $data;
		 	} else {
		 		$hotelDetail = $data->HotelRoomAvailabilityResponse;
				return $hotelDetail;
			}
		}
		
	}

	public static function callExpediaAvaibility($query){
		$method = 'avail';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::getConfig('host') . self::getConfig('path')."{$method}" . $query);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);
		$return['headers'] = $headers;
		$return['data'] = $data;
		curl_close($ch);
		return $return;
	}

	public static function get_hotel_info_v2($hotelID){
		$method 			= 'info';
    	$query  = self::getConfig('query')."&hotelId={$hotelID}&currencyCode={self::getConfig('currencyCode')}";
    	// initiate curl
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, self::getConfig('host') . self::getConfig('path')."{$method}" . $query);
		//curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		// close curl
		curl_close($ch);

		if ($headers['http_code'] != '200') {
		 	return $data;
		} else {
				$data = json_decode($data);
				return $data;
		}

	}

	//check price variation for expedia
	public static function hotel_price_check_v2($data){
		        
        
        $priceCheck = array();
        $num_of_rooms = $data['rooms'];
       	$hotel_request_data = $data['search_input'];
       	$children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.$hotel_request_data['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.$hotel_request_data['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
			}
		}

		$nights = $data['city']['default_nights'];
        $hotelId = $data['hotelId'];			
        $arrivalDate  = date('m-d-Y',strtotime($data['arrivalDate']));
		$departureDate = date('m-d-Y',strtotime($data['departureDate']));
		$room = $expedia_request;			
		$roomTypeCode = $data['roomTypeCode'];			
        $rateCode =  $data['rateCode'];
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$rateKey = $data['rateKey'];				
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		$customerSessionId = $data['customerSessionId'];
        		

		$response = self::getHotelDetails($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId);
	
		$total = '@total';
		$t_priceCheck = array();
		
		if(isset($response->HotelRoomResponse->rateCode)){
	    	$temp_array[0] = $response->HotelRoomResponse;
		    $response->HotelRoomResponse = $temp_array;
		}
		$lowest_price = array();
	    foreach($response->HotelRoomResponse as $key2 => $RoomResponse){
	    	$total = '@total';
	    	$lowest_price[$key2] = $RoomResponse->RateInfos->RateInfo->ChargeableRateInfo->$total;
	    }
		$min_array = array_keys($lowest_price, min($lowest_price));
	    
	    $singleRate = '@nightlyRateTotal';
	    foreach ($response->HotelRoomResponse as $key1 => $value1) {
	      

	      $current_hotel = json_decode(json_encode($value1),true);
	      

		  $singleRate = $data['nightlyRateTotal'];
          $taxes = $data['taxRate'];
          $currentRate = self::getExpediaHotelPriceWithEroamMarkup($singleRate,$nights,$taxes,self::getConfig('eroamPercentage'));

          $singleRate = $data['nightlyRateTotal'];
          $taxes = $data['taxRate'];
		 
          $selectedRate = self::getExpediaHotelPriceWithEroamMarkup($singleRate,$nights,$taxes,self::getConfig('eroamPercentage'));
          
	      if($currentRate != $selectedRate && $key1 == $min_array[0]){
	      		$t_priceCheck['oldRate'] = $selectedRate;
	      		$t_priceCheck['newRate'] = $currentRate;
	      		$t_priceCheck['hotelName'] = $selected_hotel['name'];
	      		$t_priceCheck['hotelId'] = $selected_hotel['hotelId'];
	      		$t_priceCheck['rateCode'] = $rateCode;
	      		$t_priceCheck['city'] = $value['city']['name'];
	      		$priceCheck[$key] = $t_priceCheck;
	      }
	    }
        	
    	return $priceCheck;
	}

	public static function hotel_booking_v2($hotelId,$arrivalDate,$departureDate,$rateKey,$roomTypeCode,$rateCode,$chargeableRate,$passengers_info,$bedtypes,$customerSessionId,$data){

		//Test Booking Credentials Static data provided by Expedia
		 $headers1 = [
             'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
             'Origin' => url('')
         ];

        $response = self::getCardInfo();
	    
	    $creditCardNumber = self::safe_b64decode($response[0]['cardNumber']);
        $creditCardIdentifier = self::safe_b64decode($response[0]['cvvNumber']);
        $creditCardExpirationMonth = self::safe_b64decode($response[0]['expiryMonth']);
        $creditCardExpirationYear = self::safe_b64decode($response[0]['expiryYear']);
        $email = 'test@travelnow.com';
	    $firstName = 'Test Booking';
	    $lastName = 'Test Booking';
	    $homePhone = '2145370159';
	    $workPhone = '2145370159';
	    $creditCardType = 'CA';
	    $address1 = 'travelnow';
	    $city = 'Atlantic City';
	    $stateProvinceCode = 'NJ';
	    $countryCode = 'AU';
	    $postalCode = '08401';

        $num_of_rooms = $data['rooms'];
       	$hotel_request_data = $data['search_input'];

        $children = array();
        if(isset($hotel_request_data['child']) && !empty($hotel_request_data['child'])){
        	$children = $hotel_request_data['child'];
        }

        $expedia_request  = '';
		if($num_of_rooms == 1){
			$expedia_request .='&room1='.$hotel_request_data['num_of_adults'][0];
			if(isset($children[0]) && !empty($children[0])){
				$c_array = implode(',',$children[0]);
				$c_array = str_replace('0','1',$c_array);
				$expedia_request .=','.count($children[0]).','.$c_array;
			}
			$expedia_request .='&room1FirstName='.$passengers_info['passenger_first_name'][0].'&room1LastName='.$passengers_info['passenger_last_name'][0].'&room1BedTypeId='.$bedtypes.'&room1SmokingPreference=NS';
		}else{
			for ($i=0;$i<$num_of_rooms;$i++){
				$n = $i+1;
				$expedia_request .='&room'.$n.'='.$hotel_request_data['num_of_adults'][$i];
				if(isset($children[$i]) && !empty($children[$i])){
					$c_array = implode(',',$children[$i]);
					$c_array = str_replace('0','1',$c_array);
					$expedia_request .=','.count($children[$i]).','.$c_array;
				}
				$expedia_request .='&room'.$n.'FirstName='.$passengers_info['passenger_first_name'][0].'&room'.$n.'LastName='.$passengers_info['passenger_last_name'][0].'&room'.$n.'BedTypeId='.$bedtypes.'&room'.$n.'SmokingPreference=NS';
			}
		}
		$rateType= 'MerchantPackage';
		

    	
	    $query = "&hotelId={$hotelId}&arrivalDate={$arrivalDate}&departureDate={$departureDate}&supplierType=E&rateKey={$rateKey}&roomTypeCode={$roomTypeCode}&rateCode={$rateCode}{$expedia_request}&chargeableRate={$chargeableRate}&email={$email}&firstName={$firstName}&lastName={$lastName}&homePhone={$homePhone}&workPhone={$workPhone}&creditCardType={$creditCardType}&creditCardNumber={$creditCardNumber}&creditCardIdentifier={$creditCardIdentifier}&creditCardExpirationMonth={$creditCardExpirationMonth}&creditCardExpirationYear={$creditCardExpirationYear}&address1={$address1}&city={$city}&stateProvinceCode={$stateProvinceCode}&countryCode={$countryCode}&postalCode={$postalCode}&rateType={$rateType}";

		
		$host = 'https://book.api.ean.com/';
		$ver 		= 'v3/';
		$method 	= 'res';
		$path 		= "ean-services/rs/hotel/{$ver}{$method}";
		
		$query = self::getConfig('query').'&customerSessionId={$customerSessionId}'.$query;	
		//Calling curl for booking
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $host . $path . self::getConfig('query'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$query);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Accept:application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		$headers = curl_getinfo($ch);

		curl_close($ch);	
		$data = json_decode($data);
		$return_data = array();
		if($data->HotelRoomReservationResponse && array_key_exists('EanWsError', $data->HotelRoomReservationResponse)){
		 	 	$return_data['itineraryId'] = '';
				$return_data['numberOfRoomsBooked'] = '';
				$return_data['reservationStatusCode'] = 'NC';
		} else {
			$return_data['itineraryId'] = $data->HotelRoomReservationResponse->itineraryId;
			$return_data['numberOfRoomsBooked'] = $data->HotelRoomReservationResponse->numberOfRoomsBooked;
			$return_data['reservationStatusCode'] = $data->HotelRoomReservationResponse->reservationStatusCode;
			$return_data['entireResponse'] = $data;
		}
		return $return_data;
		
	}

	public static function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public static function getCardinfo(){
	    $results = CardInfo::where('id',4)->get();
		return $results; 						   
	}

	public static function getExpediaHotelPriceWithEroamMarkup($Rate,$nights,$taxes,$eroamPercentage)
	{
		 $Rate = round(($Rate * $eroamPercentage) / 100 + $Rate,2); 
         $subTotal = $nights * $Rate;
         $Rate = $subTotal + $taxes;
         return $Rate;            
	}
}
