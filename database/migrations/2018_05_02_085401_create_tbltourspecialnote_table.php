<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltourspecialnoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbltourspecialnote'))
        {
        Schema::create('tbltourspecialnote', function (Blueprint $table) {
            $table->increments('tour_special_id');
            $table->integer('special_note_id');
            $table->integer('tour_id');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltourspecialnote');
    }
}
