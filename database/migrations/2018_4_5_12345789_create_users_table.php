<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')){  
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name',255)->nullable();
                $table->string('username',255)->nullable();
                $table->string('password',255)->nullable();
                $table->string('type',150)->nullable()->index()->comment('admin licensee customer agent');
                $table->string('social_type',50)->nullable();
                $table->string('social_id',150)->nullable();
                $table->integer('active')->nullable();
                $table->string('img_path')->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        
    }
}
