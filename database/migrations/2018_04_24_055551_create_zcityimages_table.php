<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZcityimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('zaecountries'))
        {
        Schema::create('zcityimages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->tinyInteger('is_primary');
            $table->text('description')->nullable();
            $table->text('original')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('small')->nullable();
            $table->text('medium')->nullable();
            $table->text('large')->nullable();
            $table->timestamps();
            
            $table->foreign('city_id')->references('id')->on('zcities');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zcityimages');
    }
}
