<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbltourpickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbltourpickups'))
        {
        Schema::create('tbltourpickups', function (Blueprint $table) {
            $table->increments('tour_pickup_id');
            $table->integer('pickup_id');
            $table->integer('tour_id');
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbltourpickups');
    }
}
