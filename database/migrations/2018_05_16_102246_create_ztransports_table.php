<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZtransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ztransports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_city_id')->nullable();
            $table->integer('to_city_id')->nullable();
            $table->string('via')->nullable();
            $table->integer('transport_type_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('default_transport_supplier_id')->nullable();
            $table->integer('operator_id')->nullable();
            $table->string('[default]')->nullable();
            $table->tinyInteger('is_international');
            $table->string('etd')->nullable();
            $table->string('eta')->nullable();
            $table->string('phone')->nullable();
            $table->text('voucher_comments')->nullable();
            $table->text('notes')->nullable();
            $table->text('address_from')->nullable();
            $table->text('address_to')->nullable();
            $table->timestamps();
            $table->text('arrives_on')->nullable();
            $table->softDeletes();
            $table->tinyInteger('is_publish')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ztransports');
    }
}
