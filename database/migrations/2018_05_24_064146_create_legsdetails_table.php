<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegsdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legdetails', function (Blueprint $table) {
            $table->increments('leg_detail_id');
            $table->integer('itenary_leg_id')->nullable();
            $table->integer('itenary_order_id')->nullable();
            $table->string('booking_id')->nullable();
            $table->integer('leg_id')->nullable();
            $table->string('leg_name')->nullable();
            $table->string('leg_type')->nullable();
            $table->text('leg_type_name')->nullable();
            $table->integer('from_city_id')->nullable();
            $table->string('from_city_name')->nullable();
            $table->dateTime('from_date')->nullable();
            $table->bigInteger('to_city_id')->nullable();
            $table->string('to_city_name')->nullable();
            $table->dateTime('to_date')->nullable();
            $table->string('nights')->nullable();
            $table->string('hotel_id')->nullable();
            $table->text('hotel_room_type_name')->nullable();
            $table->string('hotel_room_type_id')->nullable();
            $table->integer('hotel_total_room')->nullable();
            $table->integer('hotel_room_code')->nullable();
            $table->integer('hotel_rate_code')->nullable();
            $table->decimal('hotel_price_per_night',18,2)->nullable();
            $table->decimal('hotel_tax',18,2)->nullable();
            $table->decimal('hotel_expedia_subtotal',18,2)->nullable();
            $table->decimal('hotel_expedia_total',18,2)->nullable();
            $table->decimal('hotel_eroam_subtotal',18,2)->nullable();
            $table->decimal('price',18,2)->nullable();
            $table->string('currency')->nullable();
            $table->string('fare_source_code')->nullable();
            $table->string('fare_type')->nullable();
            $table->text('booking_summary_text')->nullable();
            $table->text('departure_text')->nullable();
            $table->text('arrival_text')->nullable();
            $table->text('duration')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('provider')->nullable();
            $table->text('booking_error_code')->nullable();
            $table->text('provider_booking_status')->nullable();
            $table->text('voucher_key')->nullable();
            $table->text('voucher_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legdetails');
    }
}
