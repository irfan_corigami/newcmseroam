<?php
namespace App;
use Eloquent;
class TransportType extends Eloquent {
    protected $table = 'zTransportTypes';
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'id';
    
    public function transport(){
    	 return $this->hasMany('\App\Transport');
    }

  
    
}