<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelBasePrice extends Model
{
    protected $table = 'zhotelbaseprices';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
