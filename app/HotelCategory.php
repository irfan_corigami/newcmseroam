<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelCategory extends Model
{
    protected $table = 'zhotelcategories';
    protected $fillable = [
        'name','category','sequence','hotel_stars','is_default','description'
    ];
    protected $primaryKey = 'id';
    
    public static function geHotelCategoryList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord) 
    {
        return HotelCategory::from('zhotelcategories as hc')
                            ->when($sSearchStr, function($query) use($sSearchStr) {
                                    $query->where('name','like','%'.$sSearchStr.'%');
                                })
                            ->orderBy($sOrderField, $sOrderBy)
                            ->select('*')
                            ->paginate($nShowRecord);
    }
}
