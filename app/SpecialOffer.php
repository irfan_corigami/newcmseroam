<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class SpecialOffer extends Model
{

	protected $fillable = [
		'offer_name','offer_type','inventory_name','product_name','selling_start_date','selling_end_date','travel_end_date','travel_start_date',
		'original_price','children_discount','discount_offer','discount_price','children_discount_offer','children_discount_price','term_option'
	];
    
    public static function getAccommodations() {
    	return Hotel::select('name','id')->get();
    }
   	
   	public static function getActivities() {
    	return Activity::select('name','id')->get();
    }

    public static function getTours() {
    	return Tour::select('tour_code as name','tour_id as id')->get();
    }

    public static function getTransports() {
    	return Transport::leftJoin('zcities as c1', 'c1.id','=','ztransports.from_city_id')
    					->leftJoin('zcities as c2','c2.id','=','ztransports.to_city_id')
    					->leftJoin('ztransportsuppliers','ztransportsuppliers.id','=','ztransports.default_transport_supplier_id')
    					->select('ztransports.id as id','c1.name as from_city_name','c2.name as to_city_name', 'ztransportsuppliers.name as supplier_name')
    					->get();
    }

    public static function getAccommodationPrice($product_id) {
    	return Hotel::select('fees as price')->where('id',$product_id)->first();
    }

    public static function getActivityPrice($product_id) {
    	return ActivityBasePrice::select('base_price as price')->where('activity_price_id',$product_id)->first();
    }

    public static function getTourPrice($product_id) {
    	return Tour::select('price')->where('tour_id',$product_id)->first();
    }

    public static function getTransportPrice($product_id) {
    	return TransportPrice::select('transport_base_price_id as price')->where('transport_id',$product_id)->first();
    }

    public static function getList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10) {
        
        if($sSearchBy || $sSearchStr) {
            return SpecialOffer::when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })->paginate($nShowRecord);

        } 
        return SpecialOffer::paginate($nShowRecord);
    }
}
