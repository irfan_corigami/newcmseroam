<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class City extends Model
{
    protected $fillable = [
        'name','region_id','goehash','optional_city','description','small_image','row_id','country_id','default_nights',
        'is_disabled','airport_codes','timezone_id'
    ];
    protected $table = 'zcities';
    protected $primaryKey = 'id';
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    
    public function timezone()
    {
        return $this->hasOne('App\Timezone','id','timezone_id');
    }
    
    public function country(){
    	return $this->hasOne('App\Country','id','country_id');
    }
    
    public function image(){
        return $this->hasMany('App\CityImage');
    }
    
    public function latlong(){
        return $this->hasOne('App\CitiesLatLong');
    }
    
    public function hb_destinations_mapped()
    {
        return $this->hasMany('App\HBDestinationsMapped', 'city_id', 'id')->select('city_id', 'hb_destination_id')->with([
            'hbDestination' => function($query)
            {
                $query->addSelect('id','destination_name', 'destination_code')->orderBy('destination_name')->with([
                    'hbZones' => function($q)
                    {
                        $q->addSelect('id', 'hb_destination_id', 'zone_name', 'zone_number')->orderBy('zone_name');
                    }
                ]);
            }
        ]);
    }
    
    public function aot_supplier_locations_mapped(){
        return $this->hasMany('App\AOTMapSupplierCity', 'city_id', 'id')->with([
            'locations' => function($query)
            {
                $query->orderBy('LocationType')->orderBy('LocationName');
            }
        ]);
    }
    
    public function viator_locations_mapped(){
        return $this->hasMany('App\ViatorLocationsMapped', 'city_id', 'id')->with([
            'viator_location' => function($query)
            {
                $query->orderBy('destinationType')->orderBy('destinationName');
            }
        ]);
    }  
    
    public function ae_city_mapped()
    {
        return $this->hasMany('App\AECitiesMapped', 'eroam_city_id', 'id')->with(['ae_city' => function( $query )
                {
                    $query->orderBy('Name')->with('ae_region');
                }
            ]);
    }
    
    public static function geCityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return City::from('zcities as c')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'c.id as id',
                        'c.name as name',
                        'c.optional_city as optional_city',
                        'c.created_at as created_at',
                        'c.updated_at as updated_at',
                        'co.name as country_name'
                        )
                    ->whereNull('c.deleted_at')
                    ->withTrashed()
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }

    public static function getCityDetail($city_id) {
        return City::select('zcities.description','zcities.name','ci.small')->where('zcities.id',$city_id)->leftJoin('zcityimages as ci','ci.city_id','=', 'zcities.id')->first();
    }
}
