<?php
/**
 * Created by PhpStorm.
 * User: staff777
 * Date: 6/23/2016
 * Time: 11:07 AM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ActivityNew extends Model {
    
    use SoftDeletes;
    

    protected $dates = ['deleted_at'];

    protected $table = 'zActivity';

    protected $guarded = array('id');
    public $timestamps = false; 

    public function City()
    {
        return $this->hasOne('App\City','id','city_id')->with('country');
    }

    public function Pivot(){
        return $this->hasMany('App\ActivityLabelPivot','activity_id','id')->with('Label');
    }

    public function currency(){
        return $this->hasOne('App\Currency','id','currency_id');
    }

    public function ActivityPrice(){
        return $this->hasOne('App\ActivityPrice','activity_id','id')->with('markup_obj', 'base_price_obj', 'supplier', 'currency')->price_summary();
    }

    public function activity_price(){
        return $this->hasMany('App\ActivityPrice','activity_id','id')->with('markup_obj','base_price_obj', 'supplier', 'currency')->price_summary();   
    }

    public function price(){
        return $this->hasMany('App\ActivityPrice','activity_id','id')->with('markup_obj','base_price_obj', 'supplier', 'currency')->price_summary();   
    }

    public function image(){
        return $this->hasMany('App\ActivityImage','activity_id','id')->orderByPrimary();
    }

    public function labels()
    {
        return $this->belongsToMany('App\Label', 'zActivityLabelsPivot', 'activity_id', 'label_id');
    }

    public function Supplier(){
        return $this->hasOne('App\ActivitySupplier','id','default_activity_supplier_id');
    }

    public function Operator(){
        return $this->hasOne('App\ActivityOperator','id','activity_operator_id');
    }

    // created by miguel; used in TransportPricesController @ index to shorten code;
    public function scopeGetRelationships($query, $whereIn_condition)
    {
        $query->join('zCities as fc', 'fc.id', '=', 'zTransports.from_city_id')
            ->select('zTransports.id', 
                'from_city_id', 
                'to_city_id', 
                'transport_type_id', 
                'operator_id', 
                'fc.name as from_city_name')
            ->with([ 
                'price' => function($query) use ($whereIn_condition)
                {
                    if($whereIn_condition)
                    {
                        $query->whereIn('zTransportPrices.id', $whereIn_condition);
                    }
                    $query->join('zTransportSuppliers as ts', 'ts.id', '=', 'zTransportPrices.transport_supplier_id')
                        ->addSelect('ts.name as transport_supplier_name')
                        ->with([
                            'supplier' => function($query)
                            {
                                $query->addSelect('id', 'name');
                            },
                            'currency' => function($query)
                            {
                                $query->addSelect('id', 'name', 'code');
                            }
                        ])
                        ->orderBy('transport_supplier_name', 'ASC')
                        ->orderBy('name', 'ASC');
                },
                'transporttype' => function($query)
                {
                    $query->addSelect('id', 'name');
                }, 
                'from_city' => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'to_city'   => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'operator' => function($query)
                {
                    $query->addSelect('id', 'name');
                }
            ]);
    }   


    public function scopeSeason($query,$from_date){
          $query->Has('activity_price', function($q) use($from_date) {
            return $q->where('date_from','<=',$from_date)->where('date_to','>=',$from_date);
        });
    } 

     public function scopeSupplierName($query,$supp_name){
        $query->wherehas('supplier',function($q) use($supp_name){
            return $q->where('name','like','%'.$supp_name.'%');
        });
    }

}
 