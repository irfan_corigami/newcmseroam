<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\City;
use App\Country;

class Tour extends Model
{
    protected $fillable = [
        'code','tour_code','tour_title','tour_url','discount','destination','saving_per_person','departure','tour_type_logo_id','start_date','end_date'
        ,'no_of_days_text','no_of_days','durationType','price','short_description','transport','long_description','grade','region_id','groupsize_min'
        ,'groupsize_max','grouptext','mapfile','food','accommodation','travel_guide','additional_info','provider','is_active','_year','url','tripStyle'
        ,'brochureSupplier','tripCountries','tripActivities','tripRegion','serviceLevel','RBCode','LastUpdate','is_sync','is_childAllowed','children_age'
        ,'Date_LastUpdate','views','meta_title','meta_description','meta_keywords','tour_currency','tour_PracticalDetail','IsLive_PracticalDetail'
        ,'xml_PracticalDetail','xml_itinerary','is_reviewed','is_approve','sync_error','tour_remarks','is_deleted','delete_by','id_delete_by','updated_by'
        ,'admin_id_updated','date_admin_updated','agent_id_updeted','date_agent_updated','provider_id_updated','date_provider_updated','updated_msg'
        ,'provider_tour_id','from_city_id','to_city_id','operator_id','supplier_id','de_countries','AdultPriceSingle','ChildPriceSingle','AdultSupplement'
        ,'ChildPrice','retailcost','flightPrice','flight_currency_id','flightDescription','flightReturn','flightDepart','countryData'
    ];
    protected $table = 'tbltours';
    protected $primaryKey = 'tour_id';
    
    public function currency(){
        return $this->hasOne('App\Currency','code','tour_currency');
    }
    
    public static function getTourList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        $oTours = Tour::from('tbltours as t')
                        ->select('tour_code','tour_id','tour_title','no_of_days_text','price'
                                ,'provider_name','t.is_active','t.is_reviewed'
                                ,'views','destination','departure')
                        ->join('tblProviders as p', 't.provider', '=', 'p.provider_id')
                        ->where('t.is_deleted','=', 0)
                        ->orderBy($sOrderField, $sOrderBy);
                        
        if($sSearchBy == 'city' || $sSearchBy == 'country')
        {
            
            $oTours->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                        if($sSearchBy == 'city'){
                            $query->where('t.departure', 'like', '%' . $sSearchStr . '%')
                                  ->orWhere('t.destination', 'like', '%' . $sSearchStr . '%');
                        }
                        else {
                            $country_list = Country::where('name', 'LIKE', '%'.$sSearchStr.'%')->pluck('id');
                            $city_list    = City::wherein('country_id', $country_list)->pluck('name');
                            
                            $query->wherein('departure', $city_list)
                                ->orwherein('destination', $city_list);
                        }
                    });
        }
        else
            $oTours->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        });
                        
        return $oTours->paginate($nShowRecord);
    }
    
    //Api function
    public static function getTour($aSearchData = array()) 
    {   
        $oTours = Tour::from('tbltours as t')
                        ->select('tour_id','start_date','end_date','tour_title', 'tour_url', 'price', 'no_of_days', 'code', 'tour_currency',
                                'long_description','short_description','destination','departure', 'folder_path','durationType','children_age',
                                'is_childAllowed','discount','saving_per_person','transport','accommodation','retailcost',
                                'flightPrice','flightDepart','groupsize_max','groupsize_min',
                                DB::raw("(SELECT i.image_thumb FROM tblimages i WHERE i.tour_id = t.tour_id ORDER BY sort_order ASC limit 1) as thumb"),
                                DB::raw("(SELECT ttl.logo_path FROM tbltourtypelogo ttl join tbltours t on ttl.id = t.tour_type_logo_id limit 1) as logo_path"),
                                DB::raw("(SELECT AVG(tr.rating) FROM tbltourreviews tr WHERE tr.tour_code = t.tour_id) as rating"),
                                DB::raw("(SELECT GROUP_CONCAT(c.name SEPARATOR ', ') from zcountries c JOIN tbltourcountry tc ON c.id = tc.country_id WHERE tc.tour_id= t.tour_id) as CountryList")
                                )
                        ->join( 'tblproviders as p' , 't.provider' ,'=','p.provider_id')
                        ->where('t.is_active','=',1)
                        ->where('t.is_reviewed','=',1)
                        ->where('t.is_deleted','=',0)
                        ->where('price','>',0);
        if(count($aSearchData))
        {
            $offset = $aSearchData['offset'];
            $limit = $aSearchData['limit'];
            if ($limit != '0' && $offset != '0') {
                $oTours->take($limit); 
                $oTours->skip($offset);
            }
            $sTourType = $aSearchData['tour_type'];
            if (!empty($sTourType)) {
                if ($sTourType == "single") {
                    $oTours->where(function($query){
                        $query->where(function($query1){
                            $query1->where('t.durationType','=','d')
                                  ->where('t.no_of_days_text','=',1);
                        });
                        $query->orWhere(function($query1){
                            $query1->where('t.durationType','=','h')
                                  ->where('t.no_of_days_text','<=',24);
                        });  
                    });
                }
                else if ($sTourType == "multi") 
                {
                    $oTours->where('t.durationType','=','d')
                           ->where('t.no_of_days_text','>',1);
                }
            }
            
            $countryIds = explode('# ', $aSearchData['search_value']);
            $search_type 	= $aSearchData['search_type'];
            $cityIds = explode('# ', $aSearchData['search_city_id']);
            if (!empty($search_type)) {
                if (!empty($countryIds) && $search_type != 'city') {
                    foreach ($countryIds as $key1 => $value1) {
                        if ($value1 != '') {
                            $oTours->where('t.CountryData','=',$value1);
                        }
                    }
                }
                if (!empty($cityIds) && $search_type == 'city') {
                    foreach ($cityIds as $key2 => $value2) {
                        $oTours->where('t.from_city_id','=',$value2);
                    }
                }
            }
            $tourTitle = trim($aSearchData['tourTitle']);
            if (!empty($tourTitle)) {
                $oTours->where('t.tour_title','like','%'.$tourTitle.'%');
            }
        }
        else
            $oTours->take(21); 
        
        $oTour = $oTours->get(); 
        
        return $oTour;
    }
    
    public static function getTotalTourByCountry($oValue)
    {
        $nTour = Tour::from('zcountries as c')
                    ->join('tbltourcountry as tc','tc.country_id','=','c.id')
                    ->join('tbltours as t','tc.tour_id','=','t.tour_id')
                    ->where('c.name','=',$oValue)
                    ->count();
        return $nTour;
    }
    public static function getTotalTourByCity($oValue)
    {
        $nTour = Tour::from('zcountries as c')
                    ->join('tbltourcountry as tc','tc.country_id','=','c.id')
                    ->join('zregions as r','tc.region_id','=','r.id')
                    ->join('tbltours as t','tc.tour_id','=','t.tour_id')
                    ->where('from_city_id','=',$oValue)
                    ->orWhere('to_city_id','=',$oValue)
                    ->count();
        return $nTour;
    }
    
    public static function getTourDetail($nTourId,$sUrl)
    {
        $oTours = Tour::from('tbltours as t')
                      ->join('tblproviders as p', 't.provider', '=', 'p.provider_id')
                      ->leftJoin('zcountries as c1', 'c1.id', '=', 't.de_countries')
                      ->leftJoin('zcountries as c2', 'c2.id', '=', 't.dn_countries')
                      ->select(
                            DB::raw("(SELECT ttl.logo_path FROM tbltourtypelogo ttl join tbltours t on ttl.id = t.tour_type_logo_id limit 1) as logo_path"),
                            DB::raw("(SELECT AVG(tr.rating) FROM tbltourreviews tr WHERE tr.tour_code = t.tour_id) as reviews"),
                            't.*',
                            'p.*',
                            'c1.name as departure_country',
                            'c2.name as destination_country'
                        )
                     ->where('t.tour_id',$nTourId)
                     ->where('t.tour_url',$sUrl)
                     ->first();
        return $oTours;
    }
    
    public static function getTourCountries($nTourId) 
    {
        $oTourCountries = Tour::from('tbltourcountry as tc')
                              ->leftJoin('zcountries as c', 'tc.country_id', '=', 'c.id')
                              ->select('c.id as country_id', 'c.name as country_name')
                              ->where('tc.tour_id', '=', $nTourId)
                              ->distinct()
                              ->get();
        return $oTourCountries;
    }
    public static function getTourImages($nTourId) 
    {
        $oTourImages = Tour::from('tblimages as ti')
                              ->select('image_thumb', 'image_small')
                              ->where('ti.tour_id', '=', $nTourId)
                              ->orderBy('sort_order', 'ASC')
                              ->distinct()
                              ->get();
        return $oTourImages;
    }
    public static function getTourCategory($nTourId) 
    {
        $oTourCategory = Tour::from('tbltourcategory as tc')
                             ->leftJoin('tblcategorydef as cd', 'tc.category_id', '=', 'cd.category_id')
                             ->where('tc.tour_id', '=', $nTourId)
                             ->get();
    }
    
    public static function getAvailableTourCities()
    {
        $oCitits = Tour::from('zcities as ci')
                        ->leftJoin('zcountries as co', 'ci.country_id', '=', 'co.id')
                        ->leftJoin('zregions as r', 'co.region_id', '=', 'r.id')
                        ->whereIn('ci.id',function($query){
                            $query->select('from_city_id as city_id')->from('tbltours')
                                  ->where('is_active','=', 1)
                                  ->where('is_reviewed','=', 1)
                                  ->where('is_deleted','=', 0)
                                  ->where('price','>', 0)
                                  ->whereNotNull('from_city_id');
                        })
                        ->select('ci.id as city_id','ci.country_id','co.region_id','ci.name','r.name as rname','co.name as cname')
                        ->get();
        return $oCitits;
    }
    public static function getAvailableTourCountry($aData = array())
    {
        $oCitits = Tour::from('zcountries as co')
                        ->leftJoin('tbltourcountry as tc', 'tc.country_id', '=', 'co.id')
                        ->leftJoin('tbltours as t', 'tc.tour_id', '=', 'tc.tour_id')
                        ->leftJoin('zregions as r', 'co.region_id', '=', 'r.id')
                        ->where('is_active','=', 1)
                        ->where('is_reviewed','=', 1)
                        ->where('is_deleted','=', 0)
                        ->where('price','>', 0)
                        ->distinct('co.id')
                        ->select('co.id','co.name','co.region_id','r.name as rname')
                        ->orderBy('co.name','asc');
        if(isset($aData['tour_type'])){
            $sTourType = $aData['tour_type'];
        }
        if(count($aData))
        {
            if (!empty($aData['date'])) {
                $oCitits->leftJoin('tbldates as d', 'd.tour_id', '=', 't.tour_id')
                        ->where('d.StartDate',$aData['date'].' 00:00:00.000');
            }
        }
        if(!empty($sTourType)) {
            if ($sTourType == "single") 
            {
                $oCitits->where(function($query){
                    $query->where(function($query1){
                        $query1->where('t.durationType','=','d')
                              ->where('t.no_of_days_text','=',1);
                    });
                    $query->orWhere(function($query1){
                        $query1->where('t.durationType','=','h')
                              ->where('t.no_of_days_text','<=',24);
                    });  
                });
            }
            else if ($sTourType == "multi") 
            {
                $oCitits->where('t.durationType','=','d')
                       ->where('t.no_of_days_text','>',1);
            }
        }
        return $oCitits->get();
    }
}