<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelMarkupPrice extends Model
{
    protected $table = 'zhotelmarkupprices';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
