<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItenaryOrder extends Model
{
    protected $fillable = [
                            'user_id', 'invoice_no','from_city_id','to_city_id','from_city_name','to_city_name','num_of_travellers','travel_date',
                            'total_itenary_legs', 'total_amount','total_days','currency','cost_per_day','total_per_person','evay_transcation_id','card_number',
                            'expiry_month', 'expiry_year','cvv','from_date','to_date','from_city_to_city','voucher','status'
                        ];
    protected $table = 'itenaryorders';
    protected $primaryKey = 'order_id';
    
    public function Passenger()
    {
        return $this->hasMany('App\PassengerInformation', 'itenary_order_id', 'order_id');
    }
    public function Customer()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
    public function ItenaryLegs()
    {
        return $this->hasMany('App\ItenaryLeg', 'itenary_order_id', 'order_id');
    }
    public function LegDetails()
    {
        return $this->hasMany('App\LegDetail', 'itenary_order_id', 'order_id');
    }
    public static function getItenaryList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10){
        $oItenaryOrder = ItenaryOrder::from('itenaryorders as i')
                                    ->select('invoice_no','order_id','i.created_at','status','total_amount'
                                            ,'currency','voucher','first_name','last_name')
                                    ->leftJoin('passengerinformations as p', 'p.itenary_order_id', '=', 'i.order_id')
                                    ->when($sSearchStr, function($query) use($sSearchStr) {
                                                $query->where('invoice_no','LIKE', '%'.$sSearchStr.'%');
                                            })
                                    ->orderBy($sOrderField, $sOrderBy)
                                    ->groupBy('order_id')
                                    ->paginate($nShowRecord);
        return $oItenaryOrder;
    }
}
