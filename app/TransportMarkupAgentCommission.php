<?php
namespace App;

use Eloquent;
class TransportMarkupAgentCommission extends Eloquent {

	protected $table = 'zTransportMarkupAgentCommissions';

    protected $guarded = array('id');
    protected $dates = ['deleted_at'];       

    public function markup(){
    	return $this->belongsTo('TransportMarkup','transport_markup_id','id');
    }    

}
 
?>