<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class LatLong extends Model {

	//use TypeCast;

	protected $table = 'zCitiesLatLang';


    protected $guarded = array('id');

    public $timestamps = false;
    public function city(){
    	return $this->BelongsTo('App\City');
    }

     protected $castedAttributes = [
        'lat'            => 'double',
        'lng'        => 'double',
       
    ];


}