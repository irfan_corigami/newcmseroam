<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardInfo extends Model
{
    protected $fillable = ['cardNumber', 'cvvNumber','expiryMonth','expiryYear'];
    protected $table = 'carddetail';
    protected $primaryKey = 'id';
    
}
