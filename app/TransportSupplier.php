<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportSupplier extends Model {

    protected $table = 'ztransportsuppliers';

    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    use SoftDeletes;

}

?>