<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AOTLocation extends Model
{
    protected $fillable = [
        'LocationType','LocationCode','CountryCode','StateCode','LocationName'
    ];
    protected $table = 'zaotlocations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public static function getAotlocations($sType) 
    {
    return AotLocation::select('id', 'LocationCode', 'LocationName', 'LocationType')
                    ->where('LocationType', $sType)
                    ->orderBy('LocationName')
                    ->get();    
    }
}
