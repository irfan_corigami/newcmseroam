<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class AotSupplier extends Model {

    protected $table = 'zAOTSuppliers';

    protected $guarded = array('id');
    
    public $timestamps = false;
    public function updated_supplier(){
        return $this->hasOne('AotSupplierUpdated','hotel_supplier_code','SupplierCode');
    }



    public function rooms(){
    	return $this->hasMany('AotRooms','SupplierCode','SupplierCode');
    }

}

?>