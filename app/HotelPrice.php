<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelPrice extends Model
{
    protected $table = 'zhotelprices';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public $timestamps = false;
    protected $dates = ['deleted_at']; 
    
    public function room_type(){
    	return $this->hasOne('App\HotelRoomType','id','hotel_room_type_id');
    }
    
    public function season(){
        return $this->hasOne('App\HotelSeason','id','hotel_season_id');
    }
    
    public function seasons(){
        return $this->hasMany('App\HotelSeason','id','hotel_season_id');
    }
    
    public function hotel(){
        return $this->belongsTo('App\Hotel','hotel_id','id');
    }

    public function markup_obj(){
        return $this->hasOne('App\HotelMarkup','id','hotel_markup_id');
    }

    public function base_price_obj(){
        return $this->hasOne('App\HotelBasePrice','id','hotel_base_price_id');
    }  
    
    public function scopePrice_summary($query){
        return $query
            ->join('zHotelBasePrices AS bs', 'bs.id', '=', 'zHotelPrices.hotel_base_price_id')
            ->join('zHotelMarkupPercentages AS ms', 'ms.id', '=', 'zHotelPrices.hotel_markup_percentage_id')
            ->selectRaw(
                    'zHotelPrices.*, 
                    bs.base_price, ms.percentage, 
                    (bs.base_price / ( 100 - ms.percentage ) ) * 100  AS price'
                );
    }

    public function scopeOrderByPrice($query){
        return $query->orderBy('price','ASC');
    }

    public function scopeFilterSeason($query,$date){
        $query->whereHas('seasons',function ($sub) use ($date){
            return $sub->where('from','>=',$date)->where('to','<=',$date);
        });
    }
    
}
