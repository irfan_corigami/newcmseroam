<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegDetail extends Model
{
    protected $fillable = [
                            'itenary_leg_id', 'itenary_order_id','booking_id','leg_id','leg_name','leg_type','leg_type_name','from_city_id',
                            'from_city_name', 'to_city_name','to_city_id','to_date','nights','hotel_id','hotel_room_type_name','hotel_room_type_id',
                            'hotel_rate_code','hotel_total_room','hotel_price_per_night', 'hotel_tax','hotel_expedia_subtotal',
                            'hotel_expedia_total','hotel_room_code','hotel_rate_code','hotel_eroam_subtotal','price','currency','fare_source_code','fare_type','booking_summary_text',
                            'departure_text','arrival_text','duration','supplier_id','provider','booking_error_code','provider_booking_status','voucher_key',
                            'voucher_url'
                        ];
    protected $table = 'legdetails';
    protected $primaryKey = 'leg_detail_id';
    
    public function ActivitySuppliers()
    {
        return $this->hasMany('App\ActivitySupplier', 'id', 'supplier_id');
    }
    public function HotelSuppliers()
    {
        return $this->hasMany('App\HotelSupplier', 'id', 'supplier_id');
    }
    public function TransportSuppliers()
    {
        return $this->hasMany('App\TransportSupplier', 'id', 'supplier_id');
    }
}
