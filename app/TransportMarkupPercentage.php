<?php
namespace App;
use Eloquent;

class TransportMarkupPercentage extends Eloquent {

	protected $table = 'zTransportMarkupPercentages';

    protected $guarded = array('id');
    
    protected $dates = ['deleted_at'];   

    public function markup(){
    	return $this->belongsTo('TransportMarkup','transport_markup_id','id');
    }    
}
 
?>