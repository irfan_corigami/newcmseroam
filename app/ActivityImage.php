<?php
/**
 * Created by PhpStorm.
 * User: staff777
 * Date: 6/24/2016
 * Time: 4:17 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ActivityImage extends Model {

    protected $table = 'zActivityImages';

    protected $guarded = array('id');

    // added by miguel
    public function scopeOrderByPrimary($query){
        return $query->orderBy('is_primary','DESC');
    }

    public function Activity(){
        return $this->hasOne('ActivityNew','id','activity_id');
    }

}

?>