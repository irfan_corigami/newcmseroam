<?php
namespace App;

use Eloquent;

class TransportOperator extends Eloquent {

	protected $table = 'ztransportoperators';

    protected $guarded = [];
    protected $primaryKey = 'id';

}

?>