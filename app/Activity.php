<?php

namespace App;
///priya
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'name','city_id','[default]','pickup','dropoff','has_accomodation','has_transport','approved_by_eroam',
        'activity_category_id','default_activity_supplier_id','currency_id','ranking','description','voucher_comments','address_1','address_2','notes',
        'reception_phone','reception_email','website','pax_type','activity_operator_id','duration','country_id','eroam_stamp','destination_city_id','accommodation_nights',
        'accommodation_details','transport_details','is_publish'
    ];
    protected $table = 'zactivity';
    protected $primaryKey = 'id';
    
    public static function getActivityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Activity::from('zactivity as a')
                    ->leftJoin('zcities as c','c.id','=','a.city_id')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->leftJoin('zcurrencies as cu','cu.id','=','a.currency_id')
                    ->leftJoin('zactivitysuppliers as acs','acs.id','=','a.default_activity_supplier_id')
                    ->leftJoin('zactivityoperators as ao','ao.id','=','a.activity_operator_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'a.id as id',
                        'a.name as name',
                        'a.eroam_stamp as eroam_stamp',
                        'a.duration as duration',
                        'c.name as city_name',
                        'cu.code as currency_code',
                        'acs.name as supplier_name',
                        'ao.name as operator_name'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
    
    public static function getActivitySeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
       
        $query = Activity::from('zactivity as a')
                        ->leftJoin('zcities as c','c.id','=','a.city_id')
                        ->leftJoin('zactivityoperators as ao','ao.id','=','a.activity_operator_id')
                        ->select(
                            'a.id as id',
                            'a.name as name',
                            'a.eroam_stamp as eroam_stamp',
                            'a.duration as duration',
                            'c.name as city_name',
                            'ao.name as operator_name'
                            )
                        ->orderBy($sOrderField, $sOrderBy);
                        
        if($sSearchStr != '' && ($sSearchBy == 'c.name' ||  $sSearchBy == 'ao.name' || $sSearchBy == 'a.name' ))
        {
            $query->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->with('price');
        }
        return $query->paginate($nShowRecord);
    }
    
    public function price(){
        return $this->hasMany('App\ActivityPrice','activity_id','id')->with('supplier', 'currency','base_price')->price_summary();   
    }
    public function City()
    {
        return $this->hasOne('App\City','id','city_id')->with('country');
    }
    public function Operator(){
        return $this->hasOne('App\ActivityOperator','id','activity_operator_id');
    }
}
