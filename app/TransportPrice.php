<?php
namespace App;
use Eloquent;
use Illuminate\Database\Eloquent\Model;

class TransportPrice extends Model {

    protected $table = 'zTransportPrices';

    protected $guarded = [];

    protected $dates = ['deleted_at'];   
    protected $primaryKey = 'id';
    public $timestamps = false; 
    
    public function passenger(){ 
        return $this->hasOne('\App\TransportPassengerType','id','passenger_type_id');
    }

    public function transport(){
        return $this->hasOne('\App\Transport','id','transport_id')->with('supplier','operator','to_city','from_city','transporttype');
    }

    public function currency(){
        return $this->hasOne('\App\Currency','id','currency_id');
    }    

    public function supplier(){
        return $this->hasOne('\App\TransportSupplier', 'id', 'transport_supplier_id');
    }

    public function markup_obj(){
        return $this->hasMany('\App\TransportMarkup','id','transport_markup_id');
    }

    public function base_price_obj(){
        return $this->hasOne('\App\TransportBasePrice','id','transport_base_price_id');
    }    

    public function scopePrice_summary($query){
        return $query
            ->join('zTransportBasePrices AS bs', 'bs.id', '=', 'zTransportPrices.transport_base_price_id')
            ->join('zTransportMarkupPercentages AS ms', 'ms.id', '=', 'zTransportPrices.transport_markup_percentage_id')
            ->selectRaw('zTransportPrices.*, bs.base_price, ms.percentage, (bs.base_price / ( 100 - ms.percentage ) ) * 100 AS price');
    }



}

?>