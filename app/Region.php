<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Region extends Model
{
    protected $fillable = [
        'name','description','show_on_eroam'
    ];
    protected $table = 'zregions';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function country(){
        return $this->hasMany('App\Country');
    }
    
    public static function geRegionList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return Region::withCount('country')
                    ->when($sSearchStr, function($query) use($sSearchStr) {
                            $query->where('name','like','%'.$sSearchStr.'%');
                        })
                    ->orderBy($sOrderField, $sOrderBy)
                    ->get();
    }
}
