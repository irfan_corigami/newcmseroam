<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItineraryPaxInformation extends Model
{

	protected $table = 'zItinerariesPaxInformation';

    protected $guarded = array('id');

    use SoftDeletes;

    protected $dates = ['deleted_at']; 

}

?>