<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ActivitySupplier extends Model
{
    protected $fillable = [
        'name','logo','description','special_notes','remarks','marketing_contact_name','marketing_contact_email','marketing_contact_title',
        'marketing_contact_phone','reservation_contact_name', 'reservation_contact_email', 'reservation_contact_landline','reservation_contact_free_phone',
        'accounts_contact_name','accounts_contact_email','accounts_contact_title','accounts_contact_phone',
    ];
    protected $table = 'zactivitysuppliers';
    protected $primaryKey = 'id';
    
    public static function getActivitySupplierList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return ActivitySupplier::from('zactivitysuppliers as acs')
                                ->when($sSearchStr, function($query) use($sSearchStr) {
                                        $query->where('name','like','%'.$sSearchStr.'%');
                                    })
                                ->select(
                                    'acs.id as id',
                                    'acs.name as name',
                                    'acs.marketing_contact_name as marketing_contact_name',
                                    'acs.marketing_contact_phone as marketing_contact_phone',
                                    'acs.marketing_contact_email as marketing_contact_email'
                                    )
                                ->orderBy($sOrderField, $sOrderBy)
                                ->paginate($nShowRecord);
    }
}
