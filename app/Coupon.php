<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'title','code','start_date','end_date','coupon_currency','coupon_type','amount','is_active','is_deleted','deleted_by'
    ];
    protected $table = 'zcoupons';
    protected $primaryKey = 'id';
    
    public function currency(){
        return $this->hasOne('App\Currency','id','coupon_currency');
    }
    
    public static function geCouponList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy){
        return Coupon::from('zcoupons')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->leftjoin('zCurrencies as c', 'c.id', '=', 'zcoupons.coupon_currency')
                    ->select(
                        'zcoupons.id',
                        'title',
                        'coupon_type',
                        'zcoupons.code',
                        'c.code as currency_code',
                        'amount',
                        'is_active'
                        )
                    ->where('is_deleted',0)
                    ->orderBy($sOrderField, $sOrderBy)
                    ->get();
    }
}
