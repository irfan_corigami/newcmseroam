<?php
namespace App;

use Eloquent;

class TransportMarkup extends Eloquent {

	protected $table = 'zTransportMarkups';

    protected $guarded = array('id');
    protected $softDelete = true;
    protected $dates = ['deleted_at'];       

    public function markedup_price(){
    	return $this->belongsTo('TransportMarkedupPrice','transport_markup_id','id');
    }    

    public function markup_percentage(){ 
        return $this->hasOne('\App\TransportMarkupPercentage','transport_markup_id','id');
    }        

    public function agent_commission(){
        return $this->hasOne('\App\TransportMarkupAgentCommission','transport_markup_id','id');
    }           

    public function supplier_commission(){ 
        return $this->hasOne('\App\TransportMarkupSupplierCommission','transport_markup_id','id');
    }         

    public function scopeGet_transport($query){
        $query->join('zTransport', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zTransport.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'transport');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zTransport.name AS allocation_name', 'zTransportMarkups.created_at');
    }

    public function scopeGet_city($query){
        $query->join('zCities', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zCities.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'city');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zCities.name AS allocation_name', 'zTransportMarkups.created_at');
    }    

    public function scopeGet_country($query){
        $query->join('zCountries', function($join)
            {
                $join->on('zTransportMarkups.allocation_id', '=', 'zCountries.id')
                     ->where('zTransportMarkups.allocation_type', '=', 'country');
            })
            ->select('zTransportMarkups.id', 'zTransportMarkups.name', 'zTransportMarkups.allocation_type', 'zTransportMarkups.is_active', 'zCountries.name AS allocation_name', 'zTransportMarkups.created_at');
    }    

    public function scopeActive($query){
        $query->where(['is_active' => 1]);
    }

    public function scopeDefault($query){
        $query->where(['is_default' => 1]);
    }
}
 
?>