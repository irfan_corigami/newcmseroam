<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AOTMapSupplierCity extends Model
{
    protected $fillable = [
        'city_id','location_id'
    ];
    protected $table = 'zaotmapsuppliercities';
    protected $primaryKey = 'id';
    use SoftDeletes;
    public $timestamps = false;
    
    protected $dates = ['deleted_at'];
    
    public function locations(){
    	return $this->hasOne('App\AOTLocation', 'LocationCode', 'location_id');
    }
}
