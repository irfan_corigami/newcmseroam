<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licensee extends Model
{
    protected $fillable = [
        'user_id', 'description', 'domain','expiry_date'
    ];
    protected $table = 'licensees';
    protected $primaryKey = 'id';   
}
