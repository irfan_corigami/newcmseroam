<?php

namespace App;
use Eloquent;
// created by migs

class TransportPassengerType extends Eloquent {

    protected $table = 'zTransportPassengerTypes';

    protected $guarded = [];
    protected $primaryKey = 'id';
}

?>