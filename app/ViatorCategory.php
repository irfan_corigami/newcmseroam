<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ViatorCategory extends Model{

	protected $table = 'zViatorCategories';

	protected $guarded = array('id');

	public function category_label(){
		return $this->hasMany('ViatorCategoryLabel','category_id','id');
	}


	public function scopeSelect_label($query){
		 return $query->whereHas('category_label', function($q) {            
	            $q->select('label_id');
	        });
	}


}