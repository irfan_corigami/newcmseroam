<?php
/**
 * Created by PhpStorm.
 * User: staff777
 * Date: 6/23/2016
 * Time: 11:07 AM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class AgeGroup extends Model
{
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'zAgeGroups';

    protected $guarded = array('id');
    
}
 