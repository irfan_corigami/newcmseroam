<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'user_id','email','search_history','social_id','currency','is_confirmed'
    ];
    protected $table = 'zcustomers';
    protected $primaryKey = 'id';
}
