<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLabelPivot extends Model
{
    protected $table = 'zActivityLabelsPivot';
    protected $fillable = ['activity_id','label_id'];
    protected $primaryKey = 'id';
    
    public function Label(){
        return $this->hasOne('App\Label','id','label_id');
    }
}
