<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalPayment extends Model
{
    protected $fillable = [
        'tour_id', 'Price','description','currency','season_id','isMandatory'
    ];
    protected $table = 'tbllocalpayment';
    protected $primaryKey = 'Payment_Id';
    public $timestamps = false;
    
    public static function getLocalPayment($nTourId) 
    {
        $oLocalPayment = LocalPayment::from('tblLocalPayment as lp')
                                    ->select('code','Price','description','Payment_Id','season_id')
                                    ->where('tour_id','=', $nTourId)
                                    ->leftJoin('zCurrencies as c', 'lp.currency', '=', 'c.code')
                                    ->orderBy('season_id','Asc')
                                    ->get();
        return $oLocalPayment;
    }
}
