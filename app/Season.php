<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = [
        'tour_id', 'StartDate','EndDate','AdultPrice','ChildPrice','Availability','adv_purchase','is_mon','is_tue','is_wed','is_thu','is_fri','is_sat'
        ,'is_sun','SeasonName','AdultPriceSingle','ChildPriceSingle','AdultSupplement','ChildSupplement','flight_currency_id','flightPrice','flightDescription'
        , 'flightReturn','flightDepart','sumPrice'
    ];
    protected $table = 'tblseason';
    protected $primaryKey = 'season_id';
    public $timestamps = false;
    
    public static function getSeasonList($nTourId,$sSearchStr)
    {
        return Season::where('tour_id', '=', $nTourId)
                       ->when($sSearchStr, function($query) use($sSearchStr) {
                           $query->where('SeasonName','LIKE', '%'.$sSearchStr.'%');
                       })
                       ->get();
    }
}
