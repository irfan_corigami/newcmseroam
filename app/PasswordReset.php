<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class PasswordReset extends Model
{

    protected $table = 'zPasswordResets';
    protected $fillable = array('email','token', 'expired_date', 'expired' );

    public static function store_token() {
        $token = hash_hmac('sha256', str_random(40), Config::get('app.key'));
        return $token;
    }
}
