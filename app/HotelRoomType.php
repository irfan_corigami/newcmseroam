<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomType extends Model
{
    protected $table = 'zhotelroomtypes';
    protected $guarded = [];
    protected $primaryKey = 'id';
}
