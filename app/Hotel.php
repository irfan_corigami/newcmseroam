<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'zhotels';
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    public function city(){
    	return $this->hasOne('App\City','id','city_id')->with('country');
    }

    public function category(){
    	return $this->hasOne('App\HotelCategory','id','hotel_category_id');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency','id','currency_id');
    }
    
    public function prices(){
        return $this->hasMany('App\HotelPrice', 'hotel_id', 'id');
    }
    
    public static function geHotelList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord = 10)
    {
        return Hotel::from('zhotels as h')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->orderBy($sOrderField, $sOrderBy)
                    ->with('city','category','currency')
                    ->paginate($nShowRecord);
    }
    
    public static function getHotelPriceList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return Hotel::from('zhotels')
            ->select('zhotels.id',
                'zhotels.name', 
                'city_id', 
                'hotel_category_id'
                )
            ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
            ->orderBy('zhotels.name', 'ASC')
            ->with([ 
                'prices' => function($query)
                {
                    $query->with([
                            'room_type' => function($query)
                            {   
                                $query->addSelect('id', 'name');
                            },
                            'season' => function($query)
                            {
                                $query->join('zhotelsuppliers as hs', 'hs.id', '=', 'zhotelseasons.hotel_supplier_id')
                                    ->addSelect('zhotelseasons.id',
                                        'zhotelseasons.name',
                                        'hotel_supplier_id',
                                        'currency_id',
                                        'hs.name as hotel_supplier_name')
                                    ->orderBy('hotel_supplier_name', 'ASC')
                                    ->orderBy('zhotelseasons.name', 'ASC')
                                    ->with([
                                        'supplier' => function($query)
                                        {
                                            $query->addSelect('id', 'name');
                                        },
                                        'currency' => function($query)
                                        {
                                            $query->addSelect('id', 'name', 'code');
                                        }
                                    ]);
                            }
                        ]);
                },
                'city' => function($query)
                {
                    $query->addSelect('id', 'name');
                },
                'category' => function($query)
                {
                    $query->addSelect('id', 'name');
                }
            ])->paginate($nShowRecord);
           
    }
}
