<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Suburb extends Model {
    
    
    protected $dates = ['deleted_at'];

    protected $table = 'zSuburbs';

    protected $guarded = array('id');

    protected $primaryKey = 'id';
    public $timestamps = false;
    use SoftDeletes;
    public function city()
    {
        return $this->hasOne('App\City','id','city_id')->with('Country');
    }

    public function hb_zone()
    {
    	return $this->hasOne('App\HBZone','id','hb_zone_id');
    }

}
 