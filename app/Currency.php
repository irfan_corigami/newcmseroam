<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name', 'code'
    ];
    protected $table = 'zcurrencies';
    protected $primaryKey = 'id';
    
    public static function geCurrencyList($sSearchStr,$sOrderField,$sOrderBy){
        return Currency::when($sSearchStr, function($query) use($sSearchStr) {
                            $query->where('name','like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        '*'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->get();
    }
}
