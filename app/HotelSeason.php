<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelSeason extends Model
{
    protected $table = 'zhotelseasons';
    protected $guarded = [];
    protected $primaryKey = 'id';
    
    public function supplier(){
    	return $this->belongsTo('App\HotelSupplier','hotel_supplier_id','id');
    }
    public function currency(){
        return $this->belongsTo('App\Currency');
    }
    
    public static function geHotelSeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)
    {
        return HotelSeason::from('zhotelseasons as hs')
                    ->leftJoin('zcurrencies as cu','cu.id','=','hs.currency_id')
                    ->leftJoin('zhotelsuppliers as s','s.id','=','hs.hotel_supplier_id')
                    ->leftJoin('zhotels as h','h.id','=','hs.hotel_id')
                    ->leftJoin('zcities as c','c.id','=','h.city_id')
                    ->leftJoin('zcountries as co','co.id','=','c.country_id')
                    ->when($sSearchStr, function($query) use($sSearchStr,$sSearchBy) {
                            $query->where($sSearchBy,'like','%'.$sSearchStr.'%');
                        })
                    ->select(
                        'hs.id as id',
                        'hs.name as name',
                        'h.eroam_code as eroam_code',
                        's.name as supplier_name',
                        'hs.from as from',
                        'hs.to as to'
                        )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate($nShowRecord);
    }
}
