<?php
namespace App;
use Eloquent;

class Airline extends Eloquent {

	protected $table = 'zAirlines';

    protected $guarded = array('id');

    public $timestamps = false;
   
}

?>