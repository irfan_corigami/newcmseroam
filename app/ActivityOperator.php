<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ActivityOperator extends Model
{
    protected $fillable = [
        'name','logo','description','special_notes','remarks','marketing_contact_name','marketing_contact_email','marketing_contact_title',
        'marketing_contact_phone','reservation_contact_name', 'reservation_contact_email', 'reservation_contact_landline','reservation_contact_free_phone',
        'accounts_contact_name','accounts_contact_email','accounts_contact_title','accounts_contact_phone',
    ];
    protected $table = 'zactivityoperators';
    protected $primaryKey = 'id';
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
