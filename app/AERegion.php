<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class AERegion extends Model
{

	protected $table = 'zAERegions';

    protected $guarded = array('id');

    use SoftDeletes;

    protected $dates = ['deleted_at'];    
}
 
?>