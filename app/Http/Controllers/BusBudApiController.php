<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;
use DB;


class BusBudApiController extends Controller
{

    public function __construct(){
        $this->api_Token       = "PARTNER_IwaIR8QCQtujtyNa4cQAVw";
        $this->client        = new Client();
        $this->header        = [
            "X-Busbud-Token"      => $this->api_Token,
            "version"  => 2,
            "Content-Type" => "application/json",
            "Accept"       => "application/json"
        ];
        $this->url           = 'https://napi-preview.busbud.com/';
        //$this->url_content   = 'https://api.hotelbeds.com/hotel-content-api/1.0/';
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getAllTransport()
    {
        $data = Input::all();
        $OriginLocationCode = 'u09tvm';
        $DestinationLocationCode = 'dre2em';


        $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
        // Note: when using the POST or PUT methods, you must add the header "Content-Type: application/json"
         $where = "?adult=".$data["adult"]."&child=".$data["child"]."";
        
        $url = $this->url.'x-departures/'.$data["OriginLocationCode"].'/'.$data["DestinationLocationCode"].'/'.$data["DepartureDate"].$where;
        $OriginLocationCode = $data["OriginLocationCode"];
        $DestinationLocationCode = $data["DestinationLocationCode"];
        $DepartureDate = $data["DepartureDate"];
       
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);

        // Workaround to access SSL server and slide in through side door
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // Hide curl response
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       
        $result = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
       //echo "<pre>"  ;
        curl_close($curl);
        $data =  json_decode($result);
         
        $transport = array();
       
        if($data->departures){

            foreach($data->operators as $val) {
                $data1[$val->id] = $val->name;
            }

            foreach($data->departures as $key => $value) { 

                    $d = ['departure_time' => date('G:i',strtotime($value->departure_time)),
                                'arrival_time' => date('G:i',strtotime($value->arrival_time)),
                                'price' => $value->prices->total/100,
                                'timeInHours' => $this->convertToHoursMins($value->duration, '%02d hr(s) %02d min(s)'),
                                'transportTypeName' => $data1[$value->operator_id],
                                'transportId' => $value->id,
                                'duration' => $value->duration,
                                'transportClass' => $value->class,
                                'currency' => $value->prices->currency,
                                'departure_time1' => $value->departure_time,
                                'arrival_time1' => $value->arrival_time                             
                          ];
                    $transport[$key] =  $d;
                    
                 
             }   
          
            if (!$data->complete) { 
            sleep(2);
              $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $DepartureDate, $where);
             
            }

            return Response::json(array(
                    'status' => 'sucess',
                    'data' => $transport));
        }else{
          sleep(2);
         
          $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $DepartureDate, $where);
          return Response::json(array(
                    'status' => 'sucess',
                    'data' => $transport));
        }
    }

    public function pollCall($transport,$OriginLocationCode,$DestinationLocationCode,$date,$where){

      if(count($transport) == 0){
        $pushData = 'No';
      }else{
        $pushData = 'Yes';
      }
      //echo $pushData;
      $urll  = 'https://napi-preview.busbud.com/';
      $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
      $url = $urll.'x-departures/'.$OriginLocationCode.'/'.$DestinationLocationCode.'/'.$date.'/poll'.$where;
     
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($curl);
     
      curl_close($curl);
      $data =  json_decode($result);
      
      
      if($data->departures) { 

          foreach ($data->operators as $val) {
              $data1[$val->id] = $val->name;
          }

          foreach ($data->departures as $key => $value) {

              $d = ['departure_time' => date('G:i', strtotime($value->departure_time)),
                  'arrival_time' => date('G:i', strtotime($value->arrival_time)),
                  'price' => $value->prices->total / 100,
                  'timeInHours' => $this->convertToHoursMins($value->duration, '%02d hr(s) %02d min(s)'),
                  'transportTypeName' => $data1[$value->operator_id],
                  'transportId' => $value->id,
                  'duration' => $value->duration,
                  'transportClass' => $value->class,
                  'currency' => $value->prices->currency,
                  'departure_time1' => $value->departure_time,
                  'arrival_time1' => $value->arrival_time
              ];
              $transport[$key] = $d;
          }

          if (!$data->complete) { 
              $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $date, $where);
          }
      }else{
        $transport = $this->pollCall($transport, $OriginLocationCode, $DestinationLocationCode, $date, $where);
      }
      return $transport;  
  }

   public function createBusBudCart(){
      $data = Input::all(); 
      $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
      $params = "origin_city=".$data['originlocationcode']."&destination_city=".$data['destinationlocationcode']."&outbound_date=".$data['departuredate']."&adult=".$data['travellers']."&child=0&senior=0&departure_id=".$data['departureId'];
        $url = 'https://napi-preview.busbud.com/cart';
        
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
        // Workaround to access SSL server and slide in through side door
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // Hide curl response
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result1 = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result1, 0, $header_size);
        $body = substr($result1, $header_size);

        curl_close($curl);
        $data1 =  json_decode($result1);

        if(isset($data1->error)){
             $dataError = $data1->error;
             Log::error( '----------------------------------------------' );
                    Log::error( [
                       'headers' => $header,
                       'body' => $body
                   ] );
            return Response::json([
                'result' => 'No',
                'data' => $dataError ]);
        }else{
            $cartId = $data1->data->id;
            $passengers = $data1->data->passengers;
            return Response::json([
                'result' => 'Yes',
                'cartId' => $cartId,
                'passengers' => $passengers  ]);
        }
   }

    public function bookingProcess(){
        $data = Input::all();         
       
        $cartId = $data['cartId'];
        $passangerArray = $data['passangerArray'];

         $passengers = array();
        for($j=0;$j<count($passangerArray);$j++){

           if($j == 0){
             $passengers['passengers'][$j] = array(
                                'id' => $passangerArray[$j]['id'],
                                'first_name' => $data['passenger_first_name'][$j],
                                'last_name' => $data['passenger_last_name'][$j],
                                'phone_number' => $data['passenger_contact_no'],
                                'questions' =>
                                    array (
                                        'dob' => $data['passenger_dob'][$j],
                                    ),
                                'passenger_category' => 'adult',
                                'source_passenger_category' => 'adult',
                                'age' => NULL,
                                'outbound_selected_seat' => NULL,
                                'return_selected_seat' => NULL,
                                'primary_contact' => true,
                                'email' => $data['passenger_email'],
                            );
           }else{
            $passengers['passengers'][$j] = array(
                                'id' => $passangerArray[$j]['id'],
                                'first_name' => $data['passenger_first_name'][$j],
                                'last_name' => $data['passenger_last_name'][$j],
                                'phone_number' => $data['passenger_contact_no'],
                                'questions' =>
                                    array (
                                        'dob' => $data['passenger_dob'][$j],
                                    ),
                                'passenger_category' => 'adult',
                                'source_passenger_category' => 'adult',
                                'age' => NULL,
                                'outbound_selected_seat' => NULL,
                                'return_selected_seat' => NULL,
                                'primary_contact' => false,
                                'email' => '',
                            );
           }
          
           

        }
                    // functionality for update passenger information
            $data['passenger'] =    $passengers;
            $data_json = json_encode($data['passenger']);

             $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2','Content-type:application/json');
           
            $url = 'https://napi-preview.busbud.com/cart/'.$cartId;

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_URL, $url);
            //curl_setopt($curl, CURLOPT_PUT, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($curl, CURLOPT_POSTFIELDS,$data_json);
            // Workaround to access SSL server and slide in through side door
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            // Hide curl response
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result2 = curl_exec($curl);
            //print_r($result);
            curl_close($curl);
            $data2 =  json_decode($result2);
            //echo "<pre>";print_r($data12);
            if(isset($data2->error)){
             $dataError = $data2->error;
                return Response::json([
                    'result' => 'No',
                    'data' => $dataError ]);
            }else{     
             // functionality for final payment

                $header = array('X-Busbud-Token: PARTNER_GjUXjuHSRjitXRG7icgj-w','version:2');
                
                $params1 = "email_opt_in=false&payment_type=iou";
                $url = 'https://napi-preview.busbud.com/cart/'.$cartId.'/purchase';
               
                $curl = curl_init();

                curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POSTFIELDS,$params1);
                // Workaround to access SSL server and slide in through side door
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                // Hide curl response
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
               
                $result3 = curl_exec($curl);
              
                curl_close($curl);
                $data3 =  json_decode($result3);
                if(isset($data3->error)){
                 $dataError = $data3->error;
                    return Response::json([
                        'result' => 'No',
                        'data' => $dataError ]);
                 }else{
                   return Response::json([
                    'result' => 'Yes',
                    'data' => $data3 ]);
                }
            }
    }

    function bookingProcess1(){
          $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');
        // Note: when using the POST or PUT methods, you must add the header "Content-Type: application/json"

        $url = 'https://napi-preview.busbud.com/x-departures/u09tvm/u05kq2/2018-07-05';
        $param="adult=2";
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_POSTFIELDS,$param);
        // Workaround to access SSL server and slide in through side door
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // Hide curl response
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
         $result = curl_exec($curl);

        curl_close($curl);
        $data =  json_decode($result);
       
        $originID = 'u09tvm';
        $destinationId = 'u05kq2';
        $dapartureId = $data->departures[0]->busbud_departure_id;
        echo $dapartureId1 = $data->departures[0]->id;


        $header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2');

        $params = "origin_city=u09tvm&destination_city=u05kq2&outbound_date=2018-04-05&adult=2&child=0&senior=0&departure_id=".$dapartureId1;
        $url = 'https://napi-preview.busbud.com/cart';


$curl = curl_init();

curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POSTFIELDS,$params);
// Workaround to access SSL server and slide in through side door
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
// Hide curl response
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result1 = curl_exec($curl);
//print_r($result);
curl_close($curl);
$data1 =  json_decode($result1);
if($data1->error){
    echo $data1->error->details;
}else{
    echo "2";
}
echo "<pre>";print_r($data1);exit;
$cartId = $data1->data->id;
$pid = $data1->data->passengers[0]->id;
exit;
// functionality for update passenger information
$data = array (
    'passengers' =>
        array (
            0 =>
                array (
                    'id' => $pid,
                    'first_name' => 'Mary',
                    'last_name' => 'Johnson',
                    'phone_number' => NULL,
                    'questions' =>
                        array (
                            'dob' => 'asdasd123123',
                        ),
                    'passenger_category' => 'adult',
                    'source_passenger_category' => 'adult',
                    'age' => NULL,
                    'outbound_selected_seat' => NULL,
                    'return_selected_seat' => NULL,
                    'primary_contact' => true,
                    'email' => 'mary.johnson@example.com',
                ),
        ),
);

echo $data_json = json_encode($data);

echo "------------------------------------------------------------------------------";
$header = array('X-Busbud-Token: PARTNER_IwaIR8QCQtujtyNa4cQAVw','version:2','Content-type:application/json');

$url = 'https://napi-preview.busbud.com/cart/'.$cartId;

$curl = curl_init();

curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
curl_setopt($curl, CURLOPT_URL, $url);
//curl_setopt($curl, CURLOPT_PUT, true);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($curl, CURLOPT_POSTFIELDS,$data_json);
// Workaround to access SSL server and slide in through side door
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
// Hide curl response
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result1 = curl_exec($curl);
//print_r($result);
curl_close($curl);
$data1 =  json_decode($result1);
echo "<pre>";print_r($data1);


// functionality for final payment
echo "------------------------------------------------------------------------------";
//$cartId = 'q4op1IGMRtGM9Q4hOIWu6Q';

$header = array('X-Busbud-Token: PARTNER_GjUXjuHSRjitXRG7icgj-w','version:2');

$params1 = "email_opt_in=false&payment_type=iou";
echo $url = 'https://napi-preview.busbud.com/cart/'.$cartId.'/purchase';


$curl = curl_init();

curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POSTFIELDS,$params1);
// Workaround to access SSL server and slide in through side door
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
// Hide curl response
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_HEADER, 1);
$result2 = curl_exec($curl);
$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
$header = substr($result2, 0, $header_size);
$body = substr($result2, $header_size);

curl_close($curl);
$data2 =  json_decode($result2);
echo "<pre>";print_r($data2);
    }

    function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }
}
