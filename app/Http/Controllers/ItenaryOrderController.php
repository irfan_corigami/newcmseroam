<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Carbon\Carbon;
use App\ItenaryOrder;
use App\Itinerary;
use App\ItenaryLeg;
use App\LegDetail;
use App\PassengerInformation;


use App\Country;
use App\CitiesLatLong;
use App\City;
use DB;
class ItenaryOrderController extends Controller
{
	public function __construct(){
	}

	public function saveOrderDetail(){

		$data = Input::All();
		$ItenaryOrder = new  ItenaryOrder;
        if(isset($data['user_id']) && $data['user_id'] != ''){
		$ItenaryOrder->user_id = $data['user_id'];
		}else{
			$ItenaryOrder->user_id = '';
		}
		$ItenaryOrder->invoice_no = $data['invoiceNumber'];
		$ItenaryOrder->from_city_id = $data['from_city_id'];
		$ItenaryOrder->to_city_id = $data['to_city_id'];
		$ItenaryOrder->from_city_name = $data['from_city_name'];
		$ItenaryOrder->to_city_name = $data['to_city_name'];
		$ItenaryOrder->num_of_travellers = $data['travellers'];
		$ItenaryOrder->travel_date = $data['travel_date'];
		$ItenaryOrder->total_itenary_legs = $data['total_itenary_legs'];
		$ItenaryOrder->total_amount = $data['total_amount'];
		$ItenaryOrder->total_days = $data['total_days'];
		$ItenaryOrder->currency = $data['currency'];
		$ItenaryOrder->total_per_person = $data['total_per_person'];
		$ItenaryOrder->evay_transcation_id = $data['evay_transcation_id'];
		$ItenaryOrder->card_number = $data['card_number'];
		$ItenaryOrder->expiry_month = $data['expiry_month'];
		$ItenaryOrder->expiry_year = $data['expiry_year'];
		$ItenaryOrder->cvv = $data['cvv'];
		$ItenaryOrder->cost_per_day = $data['cost_per_day'];
		$ItenaryOrder->from_date = $data['from_date'];
		$ItenaryOrder->to_date = $data['to_date'];
		$ItenaryOrder->voucher = $data['voucher'];
		$ItenaryOrder->status = $data['status'];
		$ItenaryOrder->from_city_to_city = $data['from_city_to_city'];
        	
		$ItenaryOrder->save();
   
        $ItenaryOrderLastId = $ItenaryOrder->id;
        foreach($data['leg'] as $key => $leg){
        	$ItenaryLeg = ItenaryLeg::create([
				'itenary_order_id'=> $ItenaryOrderLastId,
				'from_city_id'=>$leg['from_city_id'],
				'to_city_id'=>$leg['to_city_id'],
				'from_date'=>$leg['from_date'],
				'to_date'=>$leg['to_date'],
				'country_code'=>$leg['country_code'],
				'to_city_name'=>$leg['to_city_name'],
				'from_city_name'=>$leg['from_city_name'],
			]);
			$ItenaryLegLastId = $ItenaryLeg->id;

			if(!empty($leg['hotel'])){
				foreach($leg['hotel'] as $key => $hotel){
					$hotel_price = $hotel['price'];
					if(empty($hotel['price'])){
						$hotel_price = 0.00;
					}else{
						$hotel_price = $hotel['price'];
						$hotel_price = str_replace(',','',$hotel_price);
					}
					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $hotel['leg_type'],
						'leg_id'=> 0,
						'leg_name'=> $hotel['leg_name'],
						'hotel_room_type_id'=> $hotel['hotel_room_type_id'],
						'hotel_room_type_name'=> $hotel['hotel_room_type_name'],
						'hotel_room_code'=> $hotel['hotel_room_code'],
						'hotel_rate_code'=> $hotel['hotel_rate_code'],
						'hotel_price_per_night'=> $hotel['hotel_price_per_night'],
						'hotel_tax'=> $hotel['hotel_tax'],
						'hotel_expedia_subtotal'=> $hotel['hotel_expedia_subtotal'],
						'hotel_expedia_total'=> $hotel['hotel_expedia_total'],
						'hotel_eroam_subtotal'=> $hotel['hotel_eroam_subtotal'],
						'currency'=> $hotel['currency'],
						'booking_id'=> $hotel['booking_id'],
						'provider_booking_status'=> $hotel['provider_booking_status'],
						'hotel_total_room'=> $hotel['hotel_total_room'],
						'booking_error_code'=> $hotel['booking_error_code'],
						'nights'=> $hotel['nights'],
						'from_date'=> $hotel['checkin'],
						'to_date'=> $hotel['checkout'],
						'price'=> $hotel_price
					]);
				}
			}else{
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'hotel',
						'leg_name'=> 'Own Arrangement',
					]);
			}
			if(!empty($leg['activities'])){
				foreach($leg['activities'] as $key => $activities){
					$activities_price = $activities['price'];
					if(empty($activities['price'])){
						$activities_price = 0.00;
					}else{
						$activities_price = $activities['price'];
						$activities_price = str_replace(',','',$activities_price);
					}
					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $activities['leg_type'],
						'leg_id'=> 0,
						'leg_name'=> $activities['leg_name'],
						'from_date'=> $activities['from_date'],
						'price'=> $activities_price,
						'currency'=> $activities['currency'],
						'provider'=>$activities['provider'],
						'supplier_id'=>$activities['supplier_id'],
						'booking_id'=>$activities['booking_id'],
						'provider_booking_status'=>$activities['provider_booking_status'],
						'booking_error_code'=>$activities['booking_error_code'],
						'leg_type_name'=>$activities['leg_type_name'],
						'voucher_key'=>$activities['voucher_key'],
						'voucher_url'=>$activities['voucher_url']
					]);
				}
			}else{
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'activities',
						'leg_name'=> 'Own Arrangement',
					]);
			}
			if(!empty($leg['transport'])){
				foreach($leg['transport'] as $key => $transport){
					$transport_price = $transport['price'];
					if(empty($transport['price'])){
						$transport_price = 0.00;
					}else{
						$transport_price = $transport['price'];
						$transport_price = str_replace(',','',$transport_price);
					}
					$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> $transport['leg_type'],
						'leg_id'=> 0,
						'from_city_id'=> $transport['from_city_id'],
						'to_city_id'=> $transport['to_city_id'],
						'from_city_name'=> $transport['from_city_name'],
						'to_city_name'=> $transport['to_city_name'],
						'departure_text'=> $transport['departure_text'],
						'arrival_text'=> $transport['arrival_text'],
						'booking_summary_text'=> $transport['booking_summary_text'],
						'duration'=> $transport['duration'],
						'price'=> $transport_price,
						'currency'=> $transport['currency'],
						'provider'=>$transport['provider'],
						'supplier_id'=>$transport['supplier_id'],
						'booking_id'=>$transport['booking_id'],
						'provider_booking_status'=>$transport['provider_booking_status'],
						'booking_error_code'=>$transport['booking_error_code'],
						'leg_type_name'=>$transport['leg_type_name']
					]);
				}
			}else{
				$LegDetail = LegDetail::create([
						'itenary_order_id'=> $ItenaryOrderLastId,
						'itenary_leg_id'=> $ItenaryLegLastId,
						'leg_type'=> 'transport',
						'leg_name'=> 'Own Arrangement',
					]);
			}
			
        }
        foreach($data['passenger_info'] as $key => $passenger){
        	$is_lead = 'No';
        	if($key == 0){
				$is_lead ='Yes';
			}
   		$PassengerInformation = PassengerInformation::create([
				'itenary_order_id'=> $ItenaryOrderLastId,
				'first_name'=>$passenger['passenger_first_name'],
				'last_name'=>$passenger['passenger_last_name'],
				'dob'=> date('Y-m-d',strtotime($passenger['passenger_dob'])),
				'email'=>$passenger['passenger_email'],
				'contact'=>$passenger['passenger_contact_no'],
				'is_lead'=>$is_lead
				
			]);
        }

        
   		$PassengerInformation = PassengerInformation::create([
			'itenary_order_id'=> $ItenaryOrderLastId,
			'first_name'=>$data['billing_info']['passenger_first_name'],
			'last_name'=>$data['billing_info']['passenger_last_name'],
			'dob'=> date('Y-m-d',strtotime($data['billing_info']['passenger_dob'])),
			'country'=>$data['billing_info']['passenger_country'],
			'email'=>$data['billing_info']['passenger_email'],
			'contact'=>$data['billing_info']['passenger_contact_no'],
			'address_one'=>$data['billing_info']['passenger_address_one'],
			'address_two'=>$data['billing_info']['passenger_address_two'],
			'suburb'=>$data['billing_info']['passenger_suburb'],
			'state'=>$data['billing_info']['passenger_state'],
			'zip'=>$data['billing_info']['passenger_zip'],
			'is_billing'=>1
		]);

        return Response::json([
				'successful' => true,
				'message' => 'Payment done Successfully.'
		]);
	}

	public function getItineraryDetail($order_id) {
     $itinerary_order = ItenaryOrder::where('order_id',$order_id)->first();
     $itinerary_leg = ItenaryLeg::where('itenary_order_id',$order_id)->get();
     $passenger_information = PassengerInformation::where('itenary_order_id',$order_id)->get();

     $response['itinerary_order'] = $itinerary_order;
     $response['passenger_information'] = $passenger_information;

     foreach ($itinerary_leg as $leg) {
      $country = Country::getCountryByCode($leg['country_code']);
      $city = City::getCityDetail($leg['from_city_id']);
            $cityLatLong = CitiesLatLong::where('city_id',$leg['from_city_id'])->first();
      if(!empty($country['name'])) {
       $leg['country_name'] = $country['name'];
      } else {
       $leg['country_name'] = '';
      }

            if(!empty($city['description'])) {
                $leg['city_description'] = $city['description'];
            } else {
                $leg['city_description'] = '';
            }
            if(!empty($city['small'])) {
                $leg['city_image'] = 'https://cms.eroam.com/'.$city['small'];
            } else {
                $leg['city_image'] = 'http://dev.eroam.com/assets/images/no-image-2.jpg';
            }

      $response['itinerary_leg'][] = $leg;
              
            $leg_detail = LegDetail::where('itenary_order_id',$order_id)->where('itenary_leg_id',$leg['itenary_leg_id'])->get();
            $leg['leg_detail'] = $leg_detail;
            $city_detail = [];
            
            $city_detail = ['cityId'=>$cityLatLong['city_id'],'lat'=>$cityLatLong['lat'],'lng'=>$cityLatLong['lng'],'name'=> $city['name']];   
             $response['city_detail'][] = $city_detail;


        }
     return \response()->json([
    'successful' => true, 'response' => $response
    ]);
    }
}