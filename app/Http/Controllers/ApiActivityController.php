<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\ActivityPrice;
use App\ActivityNew;


use DB;
use Carbon\Carbon;

class ApiActivityController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function get_all_activity_by_city_id()
	{
		$data = Input::Only('city_ids','interests');

		$activity_ids = ActivityPrice::where('allotment','>','0')
			->lists('activity_id');

		$price_ids = ActivityPrice::where('allotment','>','0')
			->lists('id');

			//echo '123<pre>'; print_r($price_ids);


		$activities = ActivityNew::whereIn('city_id', $data['city_ids'])
			->whereIn('zActivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use($data)
				{
		         	if(!empty($data['interests']))
		         	{
		         		//$query->whereIn('label_id',$data['interests']);
		         		// foreach($data['interests'] as $label)
		         		// {
		         		// 	$query->orderByRaw("FIELD(transport_type_id, ".$data['transport_type_id'].") ASC");
		         		// }
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zActivityPrices.id', $price_ids);
				}
			])
		    ->orderBy('city_id', 'Desc')
		    ->get();

		// if( !empty($data['interests']) )
		// {
		// 	$labels = implode(',', $data['interests']);
  //     		$activities->orderByRaw("FIELD(transport_type_id, ".$labels.") ASC");
  //     	}		    

		$result = array();
		foreach($activities as $activity_key => $activity)
		{	
			$price_count = count($activity->activity_price);
			foreach($activity->activity_price as $price_key => $price)
			{
				if($price->activity_supplier_id != $activity->default_activity_supplier_id)
				{
					$price_count--;
					unset($activity->activity_price[$price_key]); // remove price/room if the supplier is not the activity's default supplier
				}
			}
			if($price_count > 0) // remove activity if there is no price/room
			{
				array_push($result, $activity);
			}			
		}

		return Response::json([
			'status' => 200,
			'data' => $result
		]); 

	}


	public function get_all_activity_by_city_id_v2()
	{	
		
		$data = Input::Only('city_ids', 'interests', 'date_from', 'date_to');


		$activity_ids = ActivityPrice::where('allotment', '>', '0')->lists('activity_id');

		// echo var_dump( $activity_ids );die;

		$price_ids = ActivityPrice::where('allotment','>','0')->lists('id');

		$activities = ActivityNew::whereIn('city_id', $data['city_ids'])
			->where('zActivity.is_publish','=', 1)
			->where('zActivity.duration', '=', 1)
			->whereIn('zActivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use ($data)
				{
		         	if(!empty($data['interests'])){
		         		// foreach($data['interests'] as $label)
		         		// {
		         		// 	$query->orderByRaw("FIELD(transport_type_id, ".$data['transport_type_id'].") ASC");
		         		// }
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zActivityPrices.id', $price_ids);
				}
			])
		    ->orderBy('default')
		    ->limit(1)->get();
		//dd(DB::getQueryLog()); die;

		//echo '<pre>'; print_r($activities); 

		// if( !empty($data['interests']) )
		// {
		// 	$labels = implode(',', $data['interests']);
  		//  $activities->orderByRaw("FIELD(transport_type_id, ".$labels.") ASC");
  		// }		    

		$result = array();

		foreach($activities as $activity_key => $activity)
		{	
			if( count( $activity->image ) < 1 )
			{
				//$activities[ $activity_key ]->image[0] = create_blank_img_obj();
			} else {

				$price_count = count($activity->activity_price);

				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{
					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						//$price_count--;	
						//unset($activity->activity_price[ $price_key ]); // remove price/room if the supplier is not the activity's default supplier
					}else{
						//array_push($temp_price, $price);
						//filter season jayson added
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
				//addition filter for interest
				$label_count = 0;
				if(count($data['interests']) > 0)
				{
					foreach($activity->pivot as $key => $label)
					{
						if(in_array($label->label_id, $data['interests']))
						{
							$label_count = ++$label_count;
						}
					}
				}else{
					$label_count = 1;
				}	
				//end
				if($price_count > 0 && $label_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);

				}	
			}
			

		}
		
		

		return Response::json([
			'status' => 200,
			'data' => $result
		]); 

	}






}


