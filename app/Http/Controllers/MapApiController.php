<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

use App\City;
use App\Region;
use App\Transport;
use App\LatLong;
use App\RoutePlan;
use App\ActivityNew;
use App\TransportType;
use App\HotelRoomType;
use App\Hotel;
use App\AotSupplier;
use App\ActivityPrice;
use App\TransportPrice;
use App\ViatorLocation;
use App\Currency;
use App\Label;
use App\Suburb;
use App\HotelPrice;
use App\Country;
use App\CitiesLatLong;
use App\Timezone;
use App\HBDestination;
use App\AECity;
use App\AOTLocation;
use App\AOTMapSupplierCity;
use App\HBDestinationsMapped;
use App\AECitiesMapped;
use App\ViatorLocationsMapped;
use App\CityImage;
use App\Coupon;
use App\HotelCategory;
use App\Nationality;
use App\AgeGroup;
use DB;
use Carbon\Carbon;

use Libraries\Expedia;

class MapApiController extends Controller 
{
	
	/**
	 * Get all related cities using.
	 * Parameters city_id
	 * @return Response
	 */
	
	public function getAllRelatedCities($country_id){
		
		$cities = City::select('id','name','optional_city')->with('latlong', 'iata','iatas')->where('country_id',$country_id)->orderBy('name')->get();
		return \Response::json(['status'=>200,'data'=>$cities]);
	}


	/**
	 * Get all related Countries using.
	 * 
	 * @return Response
	 */
	
	public function getAllCountries(){

		$countries = Region::with('country')->get();

		return \Response::json([ 'status' => 200, 'data' => $countries ]);

	}


	/**
	 * get city data using city_id
	 * @return Response
	 */

	public function getCityByCityId($id){
		$city = city::select('id','name','region_id','geohash','optional_city','default_nights','description','small_image','row_id','country_id','country_id','is_disabled','airport_codes')->with('latlong','iata','iatas','timezone' )->where('id', $id)->first();
		return \Response::json(['status'=>200,'data'=>$city]);
	}

	/**
	 * get nearby city if has transport 
	 * @return Response
	 */

	public function getNearbyCities(){

	$input = Input::all();


    $transports = Transport::where( 'from_city_id', $input['cityId'] )->pluck( 'to_city_id' );
	
	$transports = json_decode(json_encode($transports));	   
   	
	$in_bounds = latlong::all();

	$cities = [];
	foreach ($in_bounds as $key => $in_bound) {
		if($in_bound['lat'] > $input['slat'] && $in_bound['lat'] < $input['nlat']
		  && $in_bound['lng'] < $input['nlng'] && $in_bound['lng'] > $input['slng']  ){
			$cities[] = $in_bound['city_id']; 
		}
	}		                  
	
	$nearby_cities = City::with('latlong', 'iata')
	->whereIn( 'id', $transports )
	->orwhereIn( 'id', $cities )
	->get();

	foreach ( $nearby_cities as $city ) {
	   $city->has_transport = ( in_array( $city->id, $transports ) ) ? 1 : 0;
	}
		return \Response::json(['status'=>200,'data'=>$nearby_cities]);

	}
	/**
	 * get transport
	 * @return Response
	 */

	public function getTransportByCitieIDs(){
		$input = Input::all();
		$input['to_city_id'] = 9;
		$input['from_city_id'] = 26;
		$transports = Transport::with('price','transporttype', 'currency', 'operator')->where('from_city_id',$input['from_city_id'])
							   ->where('to_city_id',$input['to_city_id'])
							   ->where('default',1)->get();					   


						   
		$result = array();

		foreach($transports as $transport_key => $transport)
		{	
			$price_count = count($transport->price);
			foreach($transport->price as $price_key => $price)
			{
				if($price->transport_supplier_id != $transport->default_transport_supplier_id)
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

				if( $price->price == "0.0" )
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

			}

			if($price_count > 0) // remove hotel if there is no price/room
			{
		
				array_push($result, $transport);
			}			
		}					   
		return \Response::json(['status'=>200,'data'=>$result]);


	}


	/**
	 * get autoroutes
	 * @return Response
	 */

	public function getFixedCities() { // UPDATED BY RGR
		$data = [];
		$route_plan = RoutePlan::where([
			'from_city_id' => Input::get('from_city_id'),
			'to_city_id' => Input::get('to_city_id')
		])->with('routes')->first();

		if ($route_plan && $route_plan->routes) {
			$data = $route_plan->routes;
		}

		return \Response::json(['status'=>200, 'data'=> $data]);
	}


	/**
	 * get all cities
	 * @return Response
	 */
	public function getAllCities(){

		// start add by miguel to filter out cities without latlong
		$city_ids_with_latlong = LatLong::pluck('city_id');
		$city_ids_with_latlong = json_decode(json_encode($city_ids_with_latlong));
		// end add by miguel to filter out cities without latlong
		
		$cities = City::select(
				'zCities.*', 
 		    	'zCountries.Name as country_name'
			)
			->join('zCountries', 'zCountries.id', '=', 'zCities.country_id'
			)
			
			->with('image','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped', 'latlong','iata','iatas', 'suburbs')
			->where('zCities.is_disabled', 0)
			->whereIn('zCities.id', $city_ids_with_latlong) // add by miguel to filter out cities without latlong
			->get();
			echo '<pre>';print_r($cities);exit();
		// code to manually append hb_zone data in the suburbs because if parameter limit error on mssql; fix by miguel on 2017-04-26
		foreach( $cities as $c_key => $city ){
			foreach( $city->suburbs as $s_key => $suburb ){
				$s = Suburb::where( 'id', $suburb->hb_zone_id )->first();
				$cities[ $c_key ]->suburbs[ $s_key ]->hb_zone = $s;
			}
		}
		
		return Response::json(['status'=>200,'data'=>$cities]);


	}

	/**
	 * get all the default hotel,activity,transport
	 * @return Response
	 */
	public function findAllDefault(){


		$data   = Input::all();
		// $data = '{"city_ids":["37","30"],"date_from":"2018-09-09","auto_populate":"1","traveller_number":"2","search_input":{"country":"513","city":"37","to_country":"513","to_city":"30","auto_populate":"1","countryId":"","countryRegion":"","countryRegionName":"","option":"auto","searchValType":"","searchVal":"","option2":"search-box5","start_location":"Hanoi, Vietnam","end_location":"Singapore, Singapore","start_date":"2018-04-23","travellers":"2","num_of_adults":["1","1","1"],"num_of_children":["0","2","0"],"child":[{"1":"2","2":"3"}],"interests":[]},"rooms":"2","pax_information":[],"child":[{"1":"2","2":"3"}]}';
		
		// $data = json_decode($data,true);
		$aData = $data;
		//echo "<pre>";print_r($data);exit();
		
		$hotel_category_id = isset($data['search_input']['hotel_category_id']) ? $data['search_input']['hotel_category_id']:[] ;
		$transport_types   = isset($data['search_input']['transport_types']) ? $data['search_input']['transport_types']:[];
		$interests         = isset($data['search_input']['interests']) ? array_filter($data['search_input']['interests']):[];
		$room_type_id      = isset($data['search_input']['room_type_id']) ? array_filter( is_string( $data['search_input']['room_type_id']) ? explode( ",", $data['search_input']['room_type_id'] ) : $data['search_input']['room_type_id']  ):[];
		$gender			   = isset($data['search_input']['gender']) ? $data['search_input']['gender']:null;
		$date              = carbon::parse($data['date_from'])->format('Y-m-d');

		$all_default_data = []; 
		$total_price      = [];
		Input::merge(['hotel_category_id' => $hotel_category_id ]);
		Input::merge(['transport_types' => $transport_types ]);
		Input::merge(['interests' => $interests]);
		Input::merge(['room_type_id' => $room_type_id]);
		Input::merge(['gender' => $gender]);


		$aData['hotel_category_id'] = $hotel_category_id;
		$aData['transport_types'] = $transport_types;
		$aData['interests'] = $interests;
		$aData['room_type_id'] = $room_type_id;
		$aData['gender'] = $gender;


		$prev_city_default_night = 0;



		foreach ($data['city_ids'] as $key => $value)
		{	
			  	
			$activity_data           = [];
			$data['date_from']       = carbon::parse($date)->addDays($prev_city_default_night)->format('Y-m-d');
			$date                    = $data['date_from'];

			$city_data               = City::with('latlong','timezone', 'ae_city_mapped','hb_destinations_mapped', 'viator_locations_mapped', 'aot_supplier_locations_mapped','country','iata','iatas','image')->where('id',$value)->first();

			$prev_city_default_night = $city_data['default_nights'];

			$city = City::select('id','name','country_id')->with('Country')->where('id',$value)->first();
			
			Input::merge(['searchCityName' => $city->name]);
			Input::merge(['searchCityId' => $value]);
			Input::merge(['searchCountryCode' => $city['Country']['code']]);

			$aData['searchCityName'] = $city->name;
			$aData['searchCityId'] = $value;
			$aData['searchCountryCode'] = $city['Country']['code'];
			

			if( $data['auto_populate'] )
			{

				Input::merge(['city_ids' => [$value]]);
				Input::merge(['date_to' => $data['date_from']]);
				Input::merge(['date_from' => $data['date_from']]);

				$aData['city_ids'] = $value;
				$aData['date_to'] = $data['date_from'];
				$aData['date_from'] = $data['date_from'];

				$result = $this->get_all_activity_by_city_id($aData);

	  		    $activity_data = $result->getData()->data;
				
				if(count($activity_data) > 0)
				{
					foreach ($activity_data as $k => $activity)
					{
						
						$activity->price = [];
						foreach ($activity->activity_price as $element => $price)
						{	
							if( strtotime($price->date_from) < strtotime($date) && strtotime($price->date_to) > strtotime($date) )
							{
								array_push( $activity->price, $price );
							}
						}
						$activity->activity_price = $activity->price;

						$activity_data = array_filter($activity_data);	
					}
				}
		   	}

				if(count($data['city_ids']) > 1 && isset($data['city_ids'][$key+1]))
				{
					Input::merge(['date_to' => $data['date_from']]);
					Input::merge(['from_city_id' => $value]);
					Input::merge(['to_city_id' => $data['city_ids'][$key+1]]);
					Input::merge(['day' => 0]);
					Input::merge(['date_from' => $data['date_from']]);
					
					$aData['date_to'] = $data['date_from'];
					$aData['from_city_id'] =  $value;
					$aData['to_city_id'] = $data['city_ids'][$key+1];
					$aData['day'] = 0;
					$aData['date_from'] = $data['date_from'];
					
					$result = array();
					$transport_data = array();
					$preferences = isset($data['search_input']['travel_preferences']) ? $data['search_input']['travel_preferences'] : array();
					if(isset($preferences[0]['transport'][0])){
						if($preferences[0]['transport'][0] == 2){
							$result = $this->get_all_transport_by_city_ids($aData);
	  		        		$transport_data = $result->getData()->data;
						}else{
							$result = array();
							$transport_data = array();
						}
					}else{
						$result = $this->get_all_transport_by_city_ids($aData);
	  		        	$transport_data = $result->getData()->data;
					}
					
													  	
					if(count($transport_data) > 0)
					{
					 	foreach ($transport_data as $k => $transport)
					 	{
							foreach ($transport->price as $element => $price)
							{	
								// EDITED BY MIGUEL ON 2017-03-28
								// IF season start date is beyond the selected date or the season end date is behind the selected date THEN unset the season/price
								if( strtotime($price->from) > strtotime($date) || strtotime($price->to) < strtotime($date) )
								{
									unset($transport_data[$k]->price[$element]);
									
								}
							}
							// EDITED BY MIGUEL ON 2017-03-28
							// IF the transport does not have prices THEN unset the transport
							if( count( $transport_data[$k]->price ) < 1 )
							{
							   unset( $transport_data[$k] );
							}

							if(count($transport_types) > 0  && $transport->transport_type_id == $transport_types[0]){
								$transport_data[0] = $transport;
								break;
							}

							if(empty($transport_types) && $transport->transport_type_id == 2 ){ //default coach or minivan
								$transport_data[0] = $transport;
								break;
							}



				  		}	
				    }														  
					
			  	
				}
			Input::merge(['city_default_night' => $prev_city_default_night]);

			$aData['city_default_night'] = $prev_city_default_night;

			$result = $this->get_hotel_by_city_id_v2($aData);

			
  		    $hotel_data = $result;
  			
			
			// COMMENTED OUT BY MIGUEL ON 2017-03-13 1:41 AM HONGKONG SPRINT
			// THIS LINE OF CODE INCREASES THE DEFAULT NIGHTS BASED ON THE NUMBER OF ACTIVITIES RETURNED
		 	

			$all_default_data[] = [
									'activities' => isset($activity_data[0]) ? array($activity_data[0]) : NULL,
									'transport'  => isset($transport_data[0]) ? $transport_data[0]: NULL,
									'hotel'      => isset($hotel_data) ? $hotel_data : NULL,
									'city'       => $city_data 
								];
		 }
		 //echo "<pre>";print_r($all_default_data);exit();
		return \Response::json(['status'=>200,'data'=>$all_default_data]);


	}
	public function getActivity(){
		$data = Input::all();	
		$activity_data = ActivityNew::with('price')->where('id',$data['id'])->first();
		return \Response::json(['status'=>200,'data'=>$activity_data]);
	}


    public function get_transport_types()
	{
		$transport_type =  TransportType::where('transport_mode', 'single')->orderBy('sequence')->get();
		return \Response::json(['status'=>200,'data'=>$transport_type]);
	}

	 public function get_room_types()
	{
		$room_types =  HotelRoomType::orderBy('sequence')->get();
		return \Response::json(['status'=>200,'data'=>$room_types]);
	}


	public function viewEroamHotel($id){
		
		$data  = Hotel::with('city','image','price')->where('id',$id)->first();
		return \Response::json(['status'=>200,'data'=>$data]);

	}
	public function viewEroamActivity($id){
		$data  = ActivityNew::with(['City', 'Currency', 'image', 'activity_price'])->where('id',$id)->first();
		return \Response::json(['status'=>200,'data'=>$data]);
	}


	public function viewAOTHotel($code){
		$data = AotSupplier::where('SupplierCode',$code)->first();
		return \Response::json(['status'=>200,'data'=>$data]);
	}

	public function get_all_activity_by_city_id($data)
	{	

		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');


		$price_ids = ActivityPrice::where('allotment','>','0')->pluck('id');
		


		$activities = ActivityNew::where('city_id', $data['city_ids'])
			->where('zActivity.is_publish','=', 1)
			->where('zActivity.duration', '=', 1)
			->whereIn('zActivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use ($data)
				{
		         	if(!empty($data['interests'])){
		         		
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zActivityPrices.id', $price_ids);
				}
			])
		    ->orderBy('default')
		    ->limit(1)->get();

		$result = array();

		foreach($activities as $activity_key => $activity)
		{	
			if( count( $activity->image ) < 1 )
			{
			} else {

				$price_count = count($activity->activity_price);
				
				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{

					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						//unset($activity->activity_price[ $price_key ]); // remove price/room if the supplier is not the activity's default supplier
					}else{
						//array_push($temp_price, $price);
						//filter season jayson added
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
				//addition filter for interest
				$label_count = 0;
				if(count($data['interests']) > 0)
				{
					foreach($activity->pivot as $key => $label)
					{
						if(in_array($label->label_id, $data['interests']))
						{
							$label_count = ++$label_count;
						}
					}
				}else{
					$label_count = 1;
				}	
				//end
				
				if($price_count > 0 && $label_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);

				}	
			}
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
	}
	public function get_all_transport_by_city_ids($data)
	{
		
		$from_city_id          = $data['from_city_id'];
		$to_city_id            = $data['to_city_id'];
		$date_from             = $data['date_from'];
		$date_to               = $data['date_to'];
		$transport_types 	   = $data['transport_types'];	
		$transport_etd_is_null = Transport::whereNull('etd')->orWhere(['etd' => ""])->pluck('id');
		$transport_eta_is_null = Transport::whereNull('eta')->orWhere(['eta' => ""])->pluck('id');
		
		$transport_ids = TransportPrice::where('allotment','>','0')
		    ->whereRaw("(((zTransportPrices.from <= '".$date_from."') AND (zTransportPrices.to >= '".$date_from."')) AND ((zTransportPrices.from <= '".$date_to."') AND (zTransportPrices.to >= '".$date_to."')))")
		    ->pluck('transport_id');
		   
		$price_ids     = TransportPrice::where('allotment','>','0')
		    ->whereRaw("(((zTransportPrices.from <= '".$date_from."') AND (zTransportPrices.to >= '".$date_from."')) AND ((zTransportPrices.from <= '".$date_to."') AND (zTransportPrices.to >= '".$date_to."')))")
		    ->pluck('id');
		    
		$transports = Transport::whereIn('zTransports.id', $transport_ids);

		if(isset($transport_types) && count($transport_types) > 0){
			$transports = $transports->whereIn('transport_type_id',$transport_types);
		}

		$transports = $transports->where([
				'from_city_id' => $from_city_id,
				'to_city_id' => $to_city_id 
				])
			->where('zTransports.is_publish','=', 1)
		    ->whereNotIn('id', $transport_etd_is_null)
		    ->whereNotIn('id', $transport_eta_is_null)
            ->with(['from_city', 'to_city', 'supplier', 'operator', 'transport_type', 'currency','transporttype',
				'price' => function($query) use ($price_ids)
				{
					$query->whereIn('zTransportPrices.id', $price_ids);
				}

			])->get();


		$result = array();

		foreach($transports as $transport_key => $transport)
		{	
			$price_count = count($transport->price);
			foreach($transport->price as $price_key => $price)
			{
				if($price->transport_supplier_id != $transport->default_transport_supplier_id)
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

				if( $price->price == "0.0" )
				{
					$price_count--;
					unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
				}

			}

			if($price_count > 0) // remove hotel if there is no price/room
			{
				$transport->day = $day;
				array_push($result, $transport);
			}
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function hotel_by_city_id()
	{	
		$data      = Input::all();
		$hotel_ids = HotelPrice::where('allotment','>','0')->pluck('hotel_id');//dd($hotel_ids);
		$price_ids = HotelPrice::where('allotment','>','0')->pluck('id');
		$hotels    = Hotel::whereIn('id', $hotel_ids);
		$data['traveller_number'] = 1;
		if(!isset($data['room_type_id']) || count($data['room_type_id']) == 0 ){
			$room_type = [];
			switch ($data['traveller_number']) {
				case '1':
					$room_type = HotelRoomType::where('pax',1)->orWhere('is_dorm',1)->get();
					break;
				case '2':
					$room_type = HotelRoomType::where('pax',2)->orWhere('is_dorm',1)->get();
					break;
				case '3':
					$room_type = HotelRoomType::where('pax',3)->orWhere('is_dorm',1)->get();
					break;
				default:
					$room_type = HotelRoomType::where('pax','>=',$data['traveller_number'])->get();
					break;	
			}

			if($room_type){
				foreach ($room_type as $key => $value) {
					if(!isset($data['gender'])){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
					
					if(isset($data['gender']) && strtolower($data['gender']) =='male' && $value['female_only'] == 0){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}

					if(isset($data['gender']) && strtolower($data['gender']) =='female'){
						
						$data['room_type_id'][] = $value['id'];//filter for form exclude dorm requires female only
					}
				}	
			}
			
			
		}

		
		$hotels = $hotels->orderBy('id','asc');	

		

		$data['city_ids'] = array();
		$hotels = $hotels->whereIn('city_id', $data['city_ids'])
			->where('zHotels.approved_by_eroam','=', 1)
			->orderBy('id', 'ASC')
			->with(['city', 'currency', 'image', 'category',
				'prices' => function($query) use ($price_ids)
				{
					$query->whereIn('zHotelPrices.id', $price_ids)
						->with(['room_type',
							'season' => function($query)
							{
								$query->with('currency', 'supplier');
							}
						]);
				}
			])->get();

		
		$result = array();
		foreach($hotels as $hotel_key => $hotel)
		{	$temp_price = [];
			$price_count = count($hotel->prices);

			if( $hotel->image == NULL ) // if image is empty then append uploads/no-image.png
			{
				array_push($hotel->image, 'uploads/no-image.png');
			}
			foreach($hotel->prices as $price_key => $price)
			{	
			
				
				if($price->season->hotel_supplier_id != $hotel->default_hotel_supplier_id)
				{

					$price_count--;

					unset($hotel->prices[$price_key]); 
				}else{
					//filter season jayson added
					if((strtotime($price->season->from) > strtotime($data['date_from']) && strtotime($price->season->to) > strtotime($data['date_from'])) || (strtotime($price->season->from) < strtotime($data['date_from']) && strtotime($price->season->to) < strtotime($data['date_from']))  ){
						
						$price_count--;
						unset($hotel->prices[$price_key]); 
						
					}
					
					else if(isset($data['room_type_id']) &&  count($data['room_type_id']) > 0 && !in_array($price->hotel_room_type_id, $data['room_type_id'])){
						$price_count--;
						unset($hotel->prices[$price_key]);

					}else{
							array_push($temp_price, $price); 
	
					}

				}

				// added property to determine if filter hotel_room_type_id is set;
				if(!empty($data['room_type_id']))
				{
					$price->is_room_type_filtered = ($data['room_type_id'] == $price->hotel_room_type_id) ? 1: 0;
				}
				else
				{
					$hotel->is_room_type_filtered = 0;
				}
			}

			if($price_count > 0) // remove hotel if there is no price/room
			{   unset($hotel->prices) ;
				$hotel->prices =  $temp_price;
				array_push($result, $hotel);

			}
			// added property to determine if filter hotel_category_id is set;
			$hotel->is_category_filtered = 0;
			// if(!empty($data['category_id']))
			// {
			// 	$hotel->is_category_filtered = ($data['category_id'] == $hotel->hotel_category_id) ? 1: 0;
			// }
			// else
			// {
			// 	$hotel->is_category_filtered = 0;
			// }
			
		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]);

	}
	public function get_hotel_by_city_id_v2($data)
	{	
		
		$result = Expedia::getHotels($data);//For expedia
		
		$lowest_price = array();
        if(!empty($result)){
        	$result =json_decode($result, TRUE);
        	 if(isset($result['HotelListResponse']['HotelList']['HotelSummary']) && !empty($result['HotelListResponse']['HotelList']['HotelSummary'])){
        	 	
        	 	if(!isset($result['HotelListResponse']['HotelList']['HotelSummary'][0])){
        	 		$HotelSummary['0'] = $result['HotelListResponse']['HotelList']['HotelSummary'];
        	 		$HotelList = $HotelSummary;
        	 	}else{
        	 		$HotelList = $result['HotelListResponse']['HotelList']['HotelSummary'];
        	 	}
        	 	foreach ($HotelList as $key => $value) {
        	 		foreach ($value as $key1 => $value1) {
        	 			if($key1 == 'RoomRateDetailsList'){
        	 				foreach ($value1['RoomRateDetails'] as $key2 => $value2) {
        	 					if($key2 == 'RateInfos'){
        	 						foreach ($value2['RateInfo'] as $key3 => $value3) {
        	 							if($key3 == 'ChargeableRateInfo'){
        	 								$total = '@total';
        	 								$lowest_price[$key] = $value3[$total];
        	 							}
        	 						}
        	 					}
        	 				}
        	 			}
        	 		}
        	 	}
        	 }
        	 $min_array = array_keys($lowest_price, min($lowest_price));
        	 $tmp_data = $result['HotelListResponse'];
        	 $HotelList[$min_array[0]]['provider'] = 'expedia';
        	 $HotelList[$min_array[0]]['customerSessionId'] = $tmp_data['customerSessionId'];
        	 $HotelList[$min_array[0]]['cacheKey'] = $tmp_data['cacheKey'];
        	 $HotelList[$min_array[0]]['cacheLocation'] = $tmp_data['cacheLocation'];  
			 $result = $HotelList[$min_array[0]];
     	}
        return $result;
	}

	public function get_hotels_by_city_id_v2()
	{	
		$data['city'] = 'Perth';
		$data['countryCode'] = 'AU';
		$data['arrivalDate'] = '2018-09-09';
		$data['departureDate'] = '2018-09-11';
		$data['numberOfAdults'] = 1;
		$data['customerSessionId'] = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$data['rooms'] = '1';
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		

		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::getHotels_v2($data);//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function get_hotel_details()
	{	

		$hotelId = '505122';
		$arrivalDate = '09-09-2018'; //m-d-Y format
		$departureDate = '09-11-2018';
		$room = '&room1=2';
		$roomTypeCode = '200970574';
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$rateKey = '45be4e53-24fb-4d6f-9c84-2bfca2858149-5221-024';
		$latitude = '-31.69744';
		$longitude = '115.7093';
		$customerSessionId = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$type =1;
		$rateCode ='204934267';
		
		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::get_hotel_details_v2($hotelId,$arrivalDate,$departureDate,$room,$roomTypeCode,$includeDetails,$options,$rateKey,$latitude,$longitude,$customerSessionId,$type=1,$rateCode='');//For expedia
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function get_hotel_info()
	{	

		$hotelId = '505122';
		
		
		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::get_hotel_info_v2($hotelId);
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function hotel_price_check()
	{	

		$data['city'] = 'Perth';
		$data['countryCode'] = 'AU';
		$data['arrivalDate'] = '2018-09-09';
		$data['departureDate'] = '2018-09-11';
		$data['numberOfAdults'] = 1;
		$data['customerSessionId'] = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$data['latitude'] = '-31.69744';
		$data['longitude'] = '115.7093';
		$data['rooms'] = '1';
		$data['hotelId'] = '505122';
		$data['roomTypeCode'] = '200970570';
		$data['rateCode'] = '204747944';
		$data['rateKey'] = '4cd885bc-d412-445d-b0de-3ce04ccf516a-5221-024';
		$data['nightlyRateTotal'] = '251.46';
		$data['taxRate'] = '25.14';
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		

		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::hotel_price_check_v2($data);
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
		
	}

	public function hotel_booking(){
		$hotelId = '505122';
		$arrivalDate = '09-09-2018'; //m-d-Y format
		$departureDate = '09-11-2018';
		$room = '&room1=2';
		$roomTypeCode = '200970574';
		$includeDetails = 'true';
		$options = 'HOTEL_DETAILS,ROOM_TYPES,ROOM_AMENITIES,PROPERTY_AMENITIES,HOTEL_IMAGES';
		$rateKey = '45be4e53-24fb-4d6f-9c84-2bfca2858149-5221-024';
		$latitude = '-31.69744';
		$longitude = '115.7093';
		$customerSessionId = '0ABAAC10-8071-AB29-1642-87F2E19B992C';
		$type =1;
		$rateCode ='204934267';
		
		$chargeableRate = 251.46 + 25.14;
		$passengers_info['passenger_first_name'] = 'Irfan';
		$passengers_info['passenger_last_name'] ='Bagwala';
		$bedtypes = 15;

		$data['rooms'] = 1;
		$data['search_input'] = array(
			'child' => array(),
			'num_of_adults' => array('0'=>2)
		);

		$data['city'] = array(
			'default_nights' => 2,
			'id' => 30,
			'name' => 'Perth'
		);
		//echo "<pre>";print_r($data);exit;
		//$data = Input::all();

		$result = Expedia::hotel_booking_v2($hotelId,$arrivalDate,$departureDate,$rateKey,$roomTypeCode,$rateCode,$chargeableRate,$passengers_info,$bedtypes,$customerSessionId,$data);

		//echo "<pre>";print_r($result);exit;
		return \Response::json([
			'status' => 200,
			'data' => $result
		]);
	}

	public function get_all_activity_by_city_id_v2()
	{	
		//$data   = Input::all();
		// $data = '{"city_ids":["37","30"],"interests":[],"date_from":"2018-09-09","auto_populate":"1","traveller_number":"2","search_input":{"country":"513","city":"37","to_country":"513","to_city":"30","auto_populate":"1","countryId":"","countryRegion":"","countryRegionName":"","option":"auto","searchValType":"","searchVal":"","option2":"search-box5","start_location":"Hanoi, Vietnam","end_location":"Singapore, Singapore","start_date":"2018-09-09","travellers":"2","num_of_adults":["1","1","1"],"num_of_children":["0","2","0"],"child":[{"1":"2","2":"3"}],"interests":[]},"rooms":"2","pax_information":[],"child":[{"1":"2","2":"3"}]}';
		
		// $data = json_decode($data,true);

		
		//$data = Input::Only('city_ids', 'interests', 'date_from', 'date_to');


		$activity_ids = ActivityPrice::where('allotment', '>', '0')->pluck('activity_id');

		$price_ids = ActivityPrice::where('allotment','>','0')->pluck('id');

		$activities = ActivityNew::whereIn('city_id', $data['city_ids'])
			->where('zActivity.is_publish','=', 1)
			->where('zActivity.duration', '=', 1)
			->whereIn('zActivity.id', $activity_ids)
			->with(['City', 'Currency', 'image', 
				'Pivot' => function($query) use ($data)
				{
		         	if(!empty($data['interests'])){
		         		
		          	}
			   	}, 
				'activity_price' => function($query) use ($price_ids)
				{
					$query->with('currency');
					$query->whereIn('zActivityPrices.id', $price_ids);
				}
			])
		    ->orderBy('default')
		    ->limit(1)->get();
				    

		$result = array();

		foreach($activities as $activity_key => $activity)
		{	
			if( count( $activity->image ) < 1 )
			{
			} else {

				$price_count = count($activity->activity_price);

				$temp_price = [];			
				foreach($activity->activity_price as $price_key => $price)
				{
					if($price->activity_supplier_id != $activity->default_activity_supplier_id)
					{
						
					}else{
					
						if( (strtotime($price->date_from) > strtotime($data['date_from']) && strtotime($price->date_to) > strtotime($data['date_from'])) || (strtotime($price->date_from) < strtotime($data['date_from']) && strtotime($price->date_to) < strtotime($data['date_from'])) ){
							
							$price_count--;
							unset($activity->activity_price[$price_key]); 
							
						}else{
							array_push($temp_price, $price);
						}

					
					}
				}
				//addition filter for interest
				$label_count = 0;
				if(count($data['interests']) > 0)
				{
					foreach($activity->pivot as $key => $label)
					{
						if(in_array($label->label_id, $data['interests']))
						{
							$label_count = ++$label_count;
						}
					}
				}else{
					$label_count = 1;
				}	
				if($price_count > 0 && $label_count > 0 ){
					unset($activity->activity_price) ;
					$activity->activity_price =  $temp_price;
					array_push($result, $activity);

				}	
			}
			

		}

		return \Response::json([
			'status' => 200,
			'data' => $result
		]); 
	}

	public function get_currency_values()
	{	
		$data = array();
        $result = array();
        if (Cache::has('currency'))
        {
            $result = Cache::get('currency');
        }
        else
        {
            $client = new Client();
            $response = $client->get('https://apilayer.net/api/live?access_key=18501dd8f3a1112a586e60281e150213&source=AUD');
            $currency_obj = json_decode( $response->getBody() );
            if( $response->getStatusCode() == 200 )
            {
                if( $currency_obj->success == TRUE )
                {
                    foreach( $currency_obj->quotes as $key => $value)
                    {
                        $data = array_merge( $data, [ substr($key, 3) => $value ] );
                    }
                    $expiresAt = Carbon::now()->addHours(1);
                    
                    Cache::put('currency', $data, $expiresAt);

                    if( Cache::has('currency_backup') )
                    {
                        Cache::forget('currency_backup');
                    }
                    Cache::forever('currency_backup', $data);

                    $result = $data;
                }
                else
                {
                    Cache::forever('currency_backup', $data);
                }
            }
            else
            {
                $result = Cache::get('currency_backup');
            }
        }
        return $result;
		return (new CurrencyLayer())->get();
	}
	public function get_all(){
		return Currency::get();

	}

	public function get_all_label(){
		$labels = Label::orderBy('sequence')->get();
		return \Response::json([
			'status' => 200,
			'data' => $labels]);
	}

	public function get_all_suburb_by_city($city_id){
		
		$data = Suburb::where('city_id',$city_id)->with('hb_zone')->get();
		return \Response::json([
			'status' => 200,
			'data' => $data
		]);
	
	}
	public function getRegions(){
        $regions = Region::with([ 
            'country' => function( $country_query ){ 
                $country_query->with([ 
                    'city' => function( $city_query ){          
                        $city_query->with([ 'hb_destinations_mapped', 'suburbs' ]); 
                    }]); 
            }])
        ->get();
        return \Response::json([ 'status' => 200, 'data' => $regions ]);
    }
	
	public function get_traveler_profile_options()
    {

        $data            = array();
        $categories      = HotelCategory::orderBy('sequence')->get();
        $room_types      = HotelRoomType::orderBy('sequence')->get();
        $transport_types = TransportType::orderBy('sequence')->get();
        $age_groups      = AgeGroup::orderBy('id')->get();
        $featured        = Nationality::where('is_featured', TRUE)->orderBy('name')->get();
        $not_featured    = Nationality::where('is_featured', FALSE)->orderBy('name')->get();
        $nationalities   = array('featured' => $featured, 'not_featured' => $not_featured);
        return compact(
            'categories', 
            'room_types', 
            'transport_types', 
            'age_groups',
            'nationalities'
        );
    }

    public function get_all_hotel_categories(){
		$hotel_category = HotelCategory::get();

		return \Response::json([
			'status' => 200,
			'data' => $hotel_category
		]);

	}
}
