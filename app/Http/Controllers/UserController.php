<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Mail\SendMailable;

use App\User;
use App\Licensee;
use App\AgentCommissionPercentage;
use App\Agent;
use App\Currency;
use App\Customer;
use App\Itinerary;
use App\PasswordReset;
use App\ItenaryOrder;
use App\PassengerInformation;
use Auth;
use Hash;
use DB;
use Crypt;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['callUserList', 'callCreateUserLicensee', 'checkUserNameExists', 'getRandomPassword',
                                                'callCreateUserAgent','callCreateUserCustomer']]);
    }
    
    public function callUserlogin(Request $oRequest)
    {
    	if ($oRequest->isMethod('post'))
    	{
            $aData =  $oRequest->all();
            $sUser = $this->loginUser($aData);
            //if login is correct
            if ($sUser['auth'] == 'valid') {

                    Auth::login($sUser);
                    return Redirect::to('/');

            } elseif ($sUser['auth'] == 'inactive') {
                    // if account is not active
                    return Redirect::back()->withErrors('Account not active.');
            } else {
                    //if invalid credentials
                    return Redirect::back()->withErrors('Invalid username or password.');
            }
            return "this is a test";
        }
        return view('WebView::home.login');
    }
    
    public function callUserLogout() 
    {
        Auth::logout();
        return redirect('/login');
    }

    public function callUserList(Request $oRequest, $sUserType='')
    {
        //remove session when it comes from sidebar
        if(session('page_name') != $sUserType)
            $oRequest->session()->forget('user');
        
       // echo $oRequest->has('search_str');
        session(['page_name' => $sUserType]);
        $aData = session('user') ? session('user') : array();
        $oRequest->session()->forget('user');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        if(count($aData) && $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oUserList = User::getUserList($sUserType,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        //dd(DB::getQueryLog());

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oUserList->currentPage(),'user');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::user._more_user_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::user.user_list' : 'WebView::user._user_list_ajax';
        
        return \View::make($oViewName, compact('oUserList','sUserType','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callCreateUserLicensee(Request $oRequest)
    {
        session(['page_name' => 'licensee']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'domain' => 'required|unique:licensees,domain',
                                    'username' => 'required|email|unique:users,username',
                                    'password' => 'required|min:6',
                                    'duration' => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput( $oRequest->except( 'password' ) );
            }
            $oUser = User::firstOrCreate([
				'name' => $oRequest->name,
				'username' => $oRequest->username,
				'password' => Hash::make( $oRequest->password ),
				'type' => config('constants.USERTYPELICENSEE'),
				'active' => 0,
			]);
            $oLicensee = Licensee::firstOrCreate([
				'user_id' => $oUser->id,
				'domain' => $oRequest->domain,
				'expiry_date' => date( 'Y-m-d', strtotime( '+' . $oRequest->duration .' days' ) )
			]);
            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'licensee_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_licensee');
    }
    
    public function checkUserNameExists(Request $oRequest) 
    {
        $result = User::where(['username' => $oRequest->username ])->first();
        return (count($result) > 0) ? 0: 1;
    }
    
    private function getRandomPassword() 
    {
        $length = 8; // pw length
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    	$password = substr( str_shuffle( $chars ), 0, $length );
    	return $password;
    }
    
    public function callCreateUserAgent(Request $oRequest)
    {
        session(['page_name' => 'agent']);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'     => 'required',
                                    'last_name'      => 'required',
                                    'username'       => 'required|unique:users,username',
                                    'contact_number' => 'required',
                                    'address'        => 'required',
                                    'percentage'     => 'required|numeric'
                                    ]);
            if($oValidator->fails()) 
            {
                //print_r($oValidator->messages());exit;
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = '123456'; //$this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPEAGENT'),
				'active' => 1,
			]);
            $oAgent = Agent::firstOrCreate([
				'user_id' => $oUser->id,
				'contact_number' => $oRequest->contact_number,
                                'address' => $oRequest->address
			]);
            $oAgentCommissionPercentage = AgentCommissionPercentage::firstOrCreate([
                                                    'percentage' => $oRequest->percentage,
                                                    'agent_id' => $oAgent->id
                                            ]);
            Agent::where(['id' => $oAgent->id])->update(['agent_commission_percentage_id' => $oAgentCommissionPercentage->id]);
            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'agent_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_agent');
    }
    
    public function callCreateUserCustomer(Request $oRequest) 
    {
        session(['page_name' => 'customer']);
        $oCurrency = Currency::all();
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'first_name'  => 'required',
                                    'last_name'   => 'required',
                                    'username'    => 'required|unique:users,username',
                                    'currency_id' => 'required',
                                    'address'     => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $sPassword = '123456'; //$this->getRandomPassword();
            $oUser = User::firstOrCreate([
				'name' => $oRequest->first_name.' '.$oRequest->last_name,
				'username' => $oRequest->username,
				'password' => Hash::make( $sPassword ),
				'type' => config('constants.USERTYPECUSTOMER'),
				'active' => 1,
			]);
            $oCurrencyCode = Currency::select('code')->where('id', $oRequest->currency_id)->first();
            
            $customer = Customer::firstOrCreate([
                'first_name' =>$oRequest->first_name,
                'last_name' =>$oRequest->last_name,
                'user_id' => $oUser->id,
                'email' => $oRequest->username,
                'currency' =>$oCurrencyCode->code
            ]);
            Mail::to($customer->email)->send(new SendMailable($oRequest->first_name.' '.$oRequest->last_name,$sPassword));

            return Redirect::back()->with( [
				'message' => 'success', 
				'user_id' => $oUser->id, 
				'agent_name' => $oUser->name 
				]);
        }
        return \View::make('WebView::user.user_create_customer', compact('oCurrency'));
    }
    

    public function check_customer()
    {
        $input = Input::only('email', 'password');
        $data = User::where('username', $input['email'])->first();  
        
        $id = encrypt($data->id);
        $id = decrypt($id);
        if($data)
        {
            if($data->social_type != '')
            {
                $code = [
                'id' => $id           
                ];

                return \Response::json([
                    'successful' => 1,
                    'code'       => $code,
                ]); 
            }
            $error = array(
                'email' => 'Email Already Exists'
                );
            return \Response::json([
                'successful' => 0, 'message' => $error, 'data' => $data
            ]); 
        }
        else
        {
            return \Response::json([
                'successful' => true,
                'data' => $data
            ]); 
        }
    }

    public function customer_registeration() { 
        $input = Input::only('first_name','last_name','social','email','social_id','image_path', 'password','url');
        $check_user = User::where('username', $input['email'])->first();

        $id = encrypt($check_user->id);
        $id = decrypt($id);

        if($check_user)
        {
            $code = [
                'id' => $id     
            ];

            return \Response::json([
                    'successful' => true,
                    'code'       => $code,
                ]); 
        }
        // INSERT TO USERS
        if($input['social'] != '')
        {           
            $user = User::Create([
                'username'  => $input['email'],             
                'name'      => $input['first_name']." ".$input['last_name'],
                'password'  => '',              
                'type'      => 'customer',
                'active'    => 1,
                'social_type'=> $input['social'],
                'social_id' => $input['social_id'],
                'created_at'=> date("Y-m-d H:i:s")
            ]);

            $customer = Customer::Create([
                'first_name'    => $input['first_name'],
                'last_name'     => $input['last_name'],
                'user_id'       => $user->id,
                'is_confirmed'  => 1,
                'email'         => $input['email'],
                'currency'      => '',
            ]); 
        }
        else
        {
            $user = User::Create([
                'username' => $input['email'],
                'password' => Hash::make($input['password']),
                'name' => '',
                'type' => 'customer',
                'active' => 0,
                'created_at' => date("Y-m-d H:i:s")
            ]);

            $customer = Customer::Create([
                'first_name' => '',
                'last_name' => '',
                'user_id' => $user->id,
                'is_confirmed' => 0,
                'email' => $user->username,
                'currency' => '',
            ]); 
        }
        // INSERT TO CUSTOMERS
        

        if ($customer) {
            if($input['social'] == '')
            {
                $id = encrypt($customer->user_id);
                $id = decrypt($id);

                Mail::queue('customer.registration-email', [
                'first_name' => 'User',
                'link'       => $input['url'].'/confirmation/'.$id
                ], function($message) use($input) {
                    $message->to($input['email'])->subject('Welcome to Eroam!');
                });
            }
            $user     = User::where(['username' => $input['email']])->first();
            
            $id = encrypt($user->id);
            $id = decrypt($id);

            $code = [
                'id' => $id          
            ];

            return \Response::json([
                    'successful' => true,
                    'code'       => $code,
                ]); 
        }
        else
        {
            return \Response::json([
                    'successful' => false,
                    'message' => 'Something went wrong.'
                ]);
        }
    }

    public function update_user_profile_step1()
    {
        $input = Input::only('first_name', 'last_name', 'currency', 'contact_no', 'title', 'pref_gender', 'pref_age_group_id', 'pref_nationality_id','old_password','user_id','new_password');
        
        $get_customer = User::where('id', $input['user_id'])->first();
    
        if($input['old_password'] != '')
        {           
            if(!(Hash::check(Input::get('old_password'), $get_customer->password)))
            {
                $error = array(
                    'old_password' => 'Existing password not matching',
                    );
                
                return \Response::json([
                    'successful' => false, 'message' => $error,'adasd'=>'asdad'
                ]); 
            }
        }

        if($input['new_password'] != '')
        {
            
            $new_password = Hash::make($input['new_password']);

            $data_user = array(
                            'password' => $new_password,
                            );
            $customer = User::where('id', $input['user_id'])->update($data_user);
        }
        $data = array(
                'first_name'            => $input['first_name'],
                'last_name'             => $input['last_name'],
                'currency'              => $input['currency'],
                'contact_no'            => $input['contact_no'],
                'title'                 => $input['title'],
                'pref_gender'           => $input['pref_gender'],
                'pref_age_group_id'     => $input['pref_age_group_id'],
                'pref_nationality_id'   => $input['pref_nationality_id'],
                'step_1'                => 1
            );

        $customer = Customer::where('user_id', $input['user_id'])->update($data);

        if($customer)
        {
            return \Response::json(['successful' => true, 'message' => "Your profile updated successfully!"]);
        }
    }

    public function update_user_profile_step2()
    {
        $input = Input::only('phy_address_1', 'phy_address_2', 'phy_state', 'phy_city', 'phy_zip', 'phy_country', 'bill_address_1', 'bill_address_2', 'bill_state','bill_city','bill_zip','bill_country','user_id');

        $data = array(
                'phy_address_1'     => $input['phy_address_1'],
                'phy_address_2'     => $input['phy_address_2'],
                'phy_state'         => $input['phy_state'],
                'phy_city'          => $input['phy_city'],
                'phy_zip'           => $input['phy_zip'],
                'phy_country'       => $input['phy_country'],
                'bill_address_1'    => $input['bill_address_1'],
                'bill_address_2'    => $input['bill_address_2'],
                'bill_state'        => $input['bill_state'],
                'bill_city'         => $input['bill_city'],
                'bill_zip'          => $input['bill_zip'],
                'bill_country'      => $input['bill_country'],
                'step_2'            => 1
            );
        
        $customer = Customer::where('user_id', $input['user_id'])->update($data);

        if($customer)
        {
            return \Response::json([
                'successful' => true, 'message' => "Your profile updated successfully!"
                ]);
        }
    }

    public function update_user_profile_step3()
    {
        $input      = Input::only('user_id');

        $id = encrypt($input['user_id']);
        $id = decrypt($id);

        $user_id    = $id;
        $data       = array(
            'step_3' => 1
        );

        $customer = Customer::where('user_id', $user_id)->update($data);

        if($customer)
        {
            return \Response::json(['successful' => true, 'message' => "Your profile updated successfully!"]);
        }
    }

    public function update_user_profile_step4()
    {
        $input = Input::only('phy_address_1', 'phy_address_2', 'phy_state', 'phy_city', 'phy_zip', 'phy_country', 'contact_no', 'mobile_no','pref_contact_method','user_id');
        
        $data = array(
                'phy_address_1'     => $input['phy_address_1'],
                'phy_address_2'     => $input['phy_address_2'],
                'phy_state'         => $input['phy_state'],
                'phy_city'          => $input['phy_city'],
                'phy_zip'           => $input['phy_zip'],
                'phy_country'       => $input['phy_country'],
                'contact_no'        => $input['contact_no'],
                'mobile_no'         => $input['mobile_no'],
                'pref_contact_method'=> $input['pref_contact_method'],
                'step_4'            => 1
            );
        
        $customer = Customer::where('user_id', $input['user_id'])->update($data);

        if($customer)
        {
            return \Response::json([
                'successful' => true, 'message' => "Your profile updated successfully!"
                ]);
        }
    }
    public function user_get_by_id($id) {
        $id = encrypt($id);
        $id = decrypt($id);
        $user = User::with('customer')->find($id);
        return $user;
    }

    public function update_user_preferences() {
        $customer = Customer::where(['user_id' => Input::get('user_id')])->first();
        if ($customer) {
            $customer->pref_hotel_categories = Input::has('hotel_categories') ? implode(',', Input::get('hotel_categories')) : null;
            $customer->pref_hotel_room_types = Input::has('hotel_room_types') ? implode(',', Input::get('hotel_room_types')) : null;
            $customer->pref_transport_types = Input::has('transport_types') ? implode(',', Input::get('transport_types')) : null;
            $customer->pref_nationality_id = Input::get('nationality');
            $customer->pref_age_group_id = Input::get('age_group');
            $customer->pref_gender = Input::get('gender');
            $customer->interests = Input::has('interests') ? implode(',', Input::get('interests')) : null;
            $customer->save();
        }
    }

    public function get_user_itinerary() {
        $reference_no = Input::has('reference_no') ? Input::get('reference_no') : 0;
        return Itinerary::where(['reference_no' => $reference_no])->first();
    }
    
    public function request_change_password(){
        $email = Input::get('email');
        $url = Input::get('url');
        $logo = Input::get('logo');
        $result = User::where(['username' => $email ])->first();

        
        if($result){

            $first_name = 'eRoam';
            $last_name  = 'User';
            if($result->name != '')
            {
                $name = explode(' ', $result->name);
                $first_name = ucfirst($name[0]);
                $last_name = ucfirst($name[1]);
            }
        
            $today = date('Y-m-d h:i:s A');
            $status = PasswordReset::where(['email' => $email, 'expired' => 0 ])->where('expired_date', '>', $today)->first();

            if($status){
                $token = $status->token;
            }else{
                $token = PasswordReset::store_token();
                $date = date('Y-m-d h:i:s A', strtotime('+24 hours') );

                PasswordReset::create([
                        'email' => $email,
                        'token' => $token,
                        'expired_date' => $date,
                        'expired' => 0
                ]); 
            }

            $data = [
                'link' => $url.$token,
                'logo' => $logo,
                'name' => $first_name
            ];

            Mail::send('emails.forgot-password', $data, function($message) use($email, $first_name, $last_name) {
                $message->to($email, $first_name.' '.$last_name)->subject('Eroam - Password Reset Instructions.');
                $message->from(self::EROAM_EMAIL, 'Eroam');
            });

            return \Response::json([
                'response_status' => true,
                'message' => 'The password reset link has been sent to your email.'
            ]);
        }else{
            return \Response::json([
                'response_status' => false,
                'message' => 'Email Address doesn\'t exist'
            ]);
        }
    }
    public function check_token(){
        $token = Input::get('token');
        $today = date('Y-m-d h:i:s A');
        $result = PasswordReset::where(['token' => $token, 'expired' => 0 ])->where('expired_date', '>', $today)->first();

        if($result){
            $user = User::where(['username' => $result->email, 'type' => 'customer' ])->first();
            if($user){
                return \Response::json([
                    'response_status' => true,
                    'code' => Crypt::encrypt($user->id)
                ]);
            }else{
                return \Response::json([
                    'response_status' => false,
                    'expire' => false,
                ]);
            }
        }else{
            return \Response::json([
                'response_status' => false,
                'expire' => true,
            ]);
        }
    }
    public function reset_user_password_from_app(){
        $token = Input::get('token');

        $id = encrypt(Input::get('id'));
        $id = decrypt($id);

        $user = User::find($id);
        $password = Input::get('password');

        $user->password = Hash::make($password);
        $user->save();

        PasswordReset::where(['token' => $token])->update( ['expired' => 1] );

        return \Response::json([
            'response_status' => true
        ]);
    }

    public function confirm_registration() {
        // Updated by RGR

        $user_id = encrypt(Input::get('id'));
        $user_id = decrypt($user_id);

        $customer = Customer::where(['user_id' => $user_id])->first();
        
        if ($customer->is_confirmed == 1)
        {
            $error = array('message'=>'Account is already confirmed. Login instead.');          
            return \Response::json([
                'result'  => true,
                'message' => 'Account is already confirmed. Login instead.'
                ]);
        }
        elseif ($customer->is_confirmed == 0)
        {

            $customer->update(array('is_confirmed' => 1));
            // Update user active field to 1
            User::where(['id' => $user_id])->update(['active' => 1]);

            return \Response::json([
                'result'    => true,
                'type'      => "customer",
                'message'   => "Your account confirmed successful."
                ]);
            
        }
        else
        {
            $error = array('message' => 'Invalid parameters.');
            return \Response::json([
                'result' => false
                ]);
        }
    }
    public function get_user_itineraries() {
        return Itinerary::where(['customer_id' => Input::get('customer_id')])->orderBy('id', 'desc')->get();
    }

    public function get_user_trips($user_id)
    {
        $user_id = encrypt($user_id);
        $user_id = decrypt($user_id);

        $itenary_orders = ItenaryOrder::where('user_id',$user_id)
                        ->orderBy('order_id','desc')
                        ->get();
        
        $iOrder = [];
        $i = 0;
        foreach ($itenary_orders as $order) {
            $passenger_information = PassengerInformation::where('itenary_order_id',$order->order_id)->get();
            $iOrder[$i] = $order;
            $iOrder[$i]['passenger_information'] = $passenger_information;
            $i++;
        }
        return \Response::json([
                'successful' => true, 'response' => $iOrder
                ]);
    }    
}
