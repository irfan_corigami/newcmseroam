<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Carbon\Carbon;
use PDF;

use App\ItenaryOrder;
use App\LegDetail;
use App\PassengerInformation;
use App\BookingRequest;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function callItenaryList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'itenary')
            $oRequest->session()->forget('itenary');

        session(['page_name' => 'itenary']);
        $aData = session('itenary') ? session('itenary') : array();

        $oRequest->session()->forget('itenary');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'order_id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $oItineraries = ItenaryOrder::getItenaryList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oItineraries->currentPage(),'itenary');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::booking._more_itenary_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.itenary_list' : 'WebView::booking._itenary_list_ajax';
        
        return \View::make($oViewName,compact('oItineraries','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function showItenary($nItenaryId)
    {
        $oItinerary = ItenaryOrder::with('Customer','ItenaryLegs','LegDetails','LegDetails.ActivitySuppliers'
                                        ,'LegDetails.HotelSuppliers','LegDetails.TransportSuppliers','Passenger','Passenger.Country')
                                ->where('order_id',$nItenaryId)
                                ->first();
        return \View::make('WebView::booking.itenary_view',compact('oItinerary'));
    }
    public function viewItenaryPdf($nId)
    {
        $itinerary = ItenaryOrder::with('Customer','ItenaryLegs','LegDetails','Passenger','Passenger.Country')->where('order_id',$nId)->first();    
        $date_today = Carbon::now()->format('Y-m-d');
        $year = Carbon::now()->format('Y');
        $aData = array();
        $aData['param'] = "Jayson Suizo!!";
        $aData['date_today'] = $date_today;
        $aData['year'] = $year;
        $aData['itinerary'] = $itinerary;
        $pdf = new PDF();
        return \View::make('WebView::booking.view_itenary_pdf',compact('aData'));
        $pdf = PDF::loadView('WebView::booking.view_itenary_pdf',compact('aData'));
        return $pdf->stream();
    }
    public function setBookingId(Request $oRequest) 
    {
        $LegDetail = LegDetail::where('leg_detail_id',$oRequest->leg_detail_id)->update(['booking_id' => $oRequest->booking_id]);
        return $oRequest->booking_id;
    }
    
    public function callUpdateStatus(Request $oRequest) 
    {
        $ItenaryOrder = ItenaryOrder::where('order_id',$oRequest->order_id)->update(['status' => $oRequest->status_id]);
        return $oRequest->status_id;
    }

    public function callUpdateVoucher(Request $oRequest) 
    {
        $ItenaryOrder = ItenaryOrder::where('order_id',$oRequest->order_id)->update(['voucher' => $oRequest->voucher_id]);
        return $oRequest->voucher_id;
    }
    
    public function callBookedTourList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'book_tour')
            $oRequest->session()->forget('book_tour');

        session(['page_name' => 'book_tour']);
        $aData = session('book_tour') ? session('book_tour') : array();

        $oRequest->session()->forget('book_tour');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'request_id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        $oBookTours = BookingRequest::getTourBooking($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oBookTours->currentPage(),'book_tour');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::booking._more_booked_tour_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::booking.booked_tour_list' : 'WebView::booking._booked_tour_list_ajax';
        
        return \View::make($oViewName,compact('oBookTours','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callTourBookingShow($nBookId)
    {
        $sSearchStr = Null;
        $sOrderField = 'request_id';
        $sOrderBy =  'desc';
        $nShowRecord = 10;
        $oBookTours = BookingRequest::getTourBooking($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord)->find($nBookId);		
        //echo "<pre>";print_r($oBookTours);exit;
        return \View::make('WebView::booking.booked_tour_detail',compact('oBookTours'));
    }
    
    public function callTourUpdateStatus(Request $oRequest) 
    {
        if ($oRequest->status) {
                BookingRequest::where('request_id',$oRequest->request_id)->update(['status' => $oRequest->status]);
        } else {
                $voucher_sent =  $oRequest->voucher_sent;

                $update =  [
                                        'supplier_paid' => $oRequest->supplier_paid,
                                        'manual_booking_id' => $oRequest->manual_booking_id,
                                        'voucher_sent' => $voucher_sent
                                ];

                if( $voucher_sent == 1){
                        $status =  ['status' => 4 ];
                        $update = array_merge($update, $status);
                }

                BookingRequest::where('request_id',$oRequest->request_id)->update($update);
        }
        return $oRequest->request_id;
    }
}
