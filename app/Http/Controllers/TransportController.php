<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Transport;
use App\TransportOperator;
use App\TransportPassengerType;
use App\TransportPrice;
use App\TransportSupplier;
use App\TransportType;

use Session;
use File;
use Image;
use DB;

class TransportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callTransportList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'transport')
            $oRequest->session()->forget('transport');

        session(['page_name' => 'transport']);
        $aData = session('transport') ? session('transport') : array();

        $oRequest->session()->forget('transport');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 't.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oTransportList = Transport::getTransportList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oTransportList->currentPage(),'transport');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::transport._more_transport_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::transport.transport_list' : 'WebView::transport._transport_list_ajax';
        
        return \View::make($oViewName, compact('oTransportList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
}