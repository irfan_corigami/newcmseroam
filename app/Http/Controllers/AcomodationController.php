<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel;

class AcomodationController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callHotelList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'acomodation')
            $oRequest->session()->forget('acomodation');

        session(['page_name' => 'acomodation']);
        $aData = session('acomodation') ? session('acomodation') : array();

        $oRequest->session()->forget('acomodation');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'h.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oHotelList = Hotel::geHotelList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oItineraries->currentPage(),'acomodation');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::acomodation._more_hotel_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::acomodation.hotel_list' : 'WebView::acomodation._hotel_list_ajax';
        
        return \View::make($oViewName, compact('oHotelList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callHotelCreate(Request $oRequest) 
    {
        
    }
}
