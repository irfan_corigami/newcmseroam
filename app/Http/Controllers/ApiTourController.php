<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\Season;
use App\Dates;
use DB;
use Response;
class ApiTourController extends Controller
{
    public function getTours(Request $oRequest) 
    {
        $oTour = Tour::getTour();
        
        $nTourIds = array_column($oTour->toArray(), 'tour_id');
        //print_r($nTourIds);exit;
        $tourFlightResult = Season::getSeasonFlightList($nTourIds);  
        if (!empty($tourFlightResult)) {

            foreach ($oTour as $key1 => $value1) {
                $oTour[$key1]->sumPrice = 0;
            }
            foreach ($tourFlightResult as $key => $value) {

                foreach ($oTour as $key1 => $value1) {

                    if ($key1->departure == $value->flight_depart_city && $value->tour_id == $value1->tour_id) {
                        $sumPrice = $value->AdultPrice + $value->flightPrice;
                        if (empty($oTour[$key1]->sumPrice) || $oTour[$key1]->sumPrice > $sumPrice) {
                            $oTour[$key1]->sumPrice = $sumPrice;
                            $oTour[$key1]->price = $value->AdultPrice;
                            $oTour[$key1]->season_id = $value->season_id;
                            $oTour[$key1]->flightDepart = $value->name;
                            $oTour[$key1]->flightPrice = $value->flightPrice;
                        }
                    }
                }
            }
        }
        //echo $tourFlightResult ;exit;     
        return \Response::json(array(   
	        'status' => 200,
	        'data' => $oTour));
    }
    
    public function searchTour()
    {
        $data = [   'countryNames'=> 'Australia',
                    'tourTitle'=>'',
                    'provider'=> 'searchTour',
                    'tour_type'=> 'both',
                    'search_type'=> 'city',
                    'search_country_id'=> 37,
                    'search_city_id'=> 37,
                    'search_value'=> 'INDIAN EXPRESS',
                    'date'=> '1970-01-01',
                    'default_selected_city'=> 30,
                    'offset'=> 0,
                    'limit'=> 0];
        $oTour = Tour::getTour($data);

        $nTourIds = array_column($oTour->toArray(), 'tour_id');
        $tourFlightResult = Season::getSeasonFlightList($nTourIds);
        if (!empty($tourFlightResult)) {

            foreach ($oTour as $key1 => $value1) {
                $oTour[$key1]->sumPrice = 0;
            }
            foreach ($tourFlightResult as $key => $value) {
                foreach ($oTour as $key1 => $value1) {
                    if ($flight_depart_city == $value->flight_depart_city && $value->tour_id == $value1->tour_id) {
                        $sumPrice = $value->AdultPrice + $value->flightPrice;
                        if (empty($oTour[$key1]->sumPrice) || $oTour[$key1]->sumPrice > $sumPrice) {
                            $oTour[$key1]->sumPrice = $sumPrice;
                            $oTour[$key1]->price = $value->AdultPrice;
                            $oTour[$key1]->season_id = $value->season_id;
                            $oTour[$key1]->flightDepart = $value->name;
                            $oTour[$key1]->flightPrice = $value->flightPrice;
                        }
                    }
                }
            }
        }

        $data1['results'] = $oTour;
        $data1['count'] = $count;

        return Response::json(array(
                    'status' => 200,
                    'data' => $data1,
                    'query' => $query));
    }
    
    public function getTotalTourByCountry()
    {
        $data['countries'] = "Afghanistan# Australia# Bangladesh# Brunei# Bhutan# Cocos [Keeling] Islands# Cook Islands# China# Christmas Island# Cyprus# Fiji# Micronesia# Georgia# Guam# Hong Kong# Heard Island and McDonald Islands# Indonesia [Bali]# India# British Indian Ocean Territory# Jersey";
        //$data = Input::all();
        $countries = explode('# ', $data['countries']);
        $count = 0;

        $data = [];
        foreach ($countries as $key => $value) {
            $sCName =trim($value);
            if ($value == 'Myanmar [Burma]') {
                $sCName = 'Myanmar';
            } else if ($value == 'Indonesia [Bali]') {
                $sCName = 'Indonesia';
            } else if ($value == 'Malaysia [Borneo]') {
                $sCName = 'Malaysia';
            }
            //DB::enableQueryLog();
            $nTour = Tour::getTotalTourByCountry($sCName);
            //if($value == 'Australia')
            //dd(DB::getQueryLog());
            
            $data[$value] = $nTour;

            $count += $nTour;
        }

        return Response::json(array(
                    'status' => 200,
                    'data' => $data,
                    'count' => $count));
    }
    //remaining
    public function tourBooking()
    {
    	$db_ext = \DB::connection('sqlsrv');
    	$data 	= Input::all();
    	$dateKey = '';
    	$dateValue = '';

    	foreach ($data as $key => $value) {
    	 	if($value != ''){
    	 		$dateKey 	.=  '['.$key.'], ';
    	 		if(!is_numeric($value) || $key == 'contact'){
    	 			$dateValue 	.=  "'".$value."', ";
    	 		} else{
	    	 		$dateValue 	.=  $value.', ';
    	 		}
    	 	}
    	}
    	$dateKey = substr($dateKey, 0,-2);
    	$dateValue = substr($dateValue, 0,-2);

    	$sql = "INSERT INTO tblBookingRequests (".$dateKey.") values (".$dateValue.")";

    	$result['result'] = $db_ext->insert($sql);
        $result['lastInsertId'] = $db_ext->getPdo()->lastInsertId();
    	return Response::json(['data' => $result]);
    }
    
    public function getTourDetail()
    {
        $data = array();
        $data['default_selected_city'] = 30;
        //$data = Input::all();
        $sUrl = (isset($data['url']) ) ? $data['url'] : 'priya_test78';
        $nTourId = (isset($data['id']) ) ? $data['id'] : 15865;
        
        $flight_depart_city = $data['default_selected_city'];
        if (empty($flight_depart_city)) {
            $flight_depart_city = 30;
        }
        
        $results = Tour::getTourDetail($nTourId,$sUrl);
        //print_r($results);exit;
        $data['results'] = $results;

        $currentdate = date('Y-m-d');
        $atour['0']= 71;
        $tourFlightResult = Season::getSeasonFlightList($atour);

        if (!empty($tourFlightResult)) {
            $results->sumPrice = 0;
            foreach ($tourFlightResult as $key => $value) {
                if ($flight_depart_city == $value->flight_depart_city && $value->tour_id == $results->tour_id) {
                    $sumPrice = $value->AdultPrice + $value->flightPrice;
                    if (empty($results->sumPrice) || $results->sumPrice > $sumPrice) {
                        $results->sumPrice = $sumPrice;
                        $results->price = $value->AdultPrice;
                        $results->season_id = $value->season_id;
                        $results->flightDepart = $value->name;
                        $results->flightPrice = $value->flightPrice;
                    }
                }
            }
        }
        $data['reviews'] = $results->reviews;

        $countries = Tour::getTourCountries($nTourId);
        $data['countries'] = $countries;

        $images = Tour::getTourImages($nTourId);
        $data['images'] = $images;

        $category = Tour::getTourCategory($nTourId);
        $data['category'] = $category;

        $provider_id = $results->provider_id;
        $tour_id = $results->tour_id;
        $tour_currency = $results->tour_currency;

        $provider_tour_id = $results->provider_tour_id;
        $tour_code = $results->code;

        $dates = array();

        $datesSean = Season::getTourSeason($results->tour_id);
        
        if ($datesSean) {
            $i = 0;
            foreach ($datesSean as $date) {
                $currentdate = date('Y-m-d', strtotime('+' . ($date->adv_purchase + 1) . ' days'));
                $sdates = Dates::getTourDate($results->tour_id,$currentdate,$date->season_id);

                if ($sdates) {
                    foreach ($sdates as $sdate) {
                        $dates[$i]['departureCode'] = $results->tour_code; //$date->season_id;
                        $dates[$i]['departureName'] = '';
                        $dates[$i]['startDate'] = $sdate->StartDate;
                        $dates[$i]['finishDate'] = $sdate->EndDate;
                        $dates[$i]['startCity'] = $results->departure;
                        $dates[$i]['finishCity'] = $results->destination;
                        $dates[$i]['availability'] = $sdate->Availability;
                        $dates[$i]['currency'] = $tour_currency;
                        $dates[$i]['base'] = $sdate->AdultPrice;
                        $dates[$i]['total'] = $sdate->AdultPrice;
                        $dates[$i]['singlePrice'] = $sdate->AdultPriceSingle;
                        $dates[$i]['supplement'] = $sdate->AdultSupplement;
                        $dates[$i]['season_id'] = $sdate->season_id;
                        $i++;
                    }
                    //$dates= $dates;
                } else {
                    //$dates = array();
                }
            }
        } else {
            $dates = array();
        }
        //}
        //usort($dates, 'sort_by_date');
        $data['Seasons'] = $datesSean;
        $data['dates'] = $dates;

        Tour::where('tour_id', '=', $tour_id)->increment('views');

        return Response::json(['data' => $data]);
    }
    
    public function getTotalTourByCountryLists() 
    {
        $aData = array(
                //'date' => '2018-06-31',
                'tour_type' => 'single'              
                );
        
        $oCountry = Tour::getAvailableTourCountry($aData);
        return Response::json(array(
                    'status' => 200,
                    'data' => $oCountry));
    }

    public function getTourCitiesAvailable() {
        //echo "fii";exit;
        $oTourCity = Tour::getAvailableTourCities();

        return Response::json(array(
                    'status' => 200,
                    'data' => $oTourCity));
    }

}
