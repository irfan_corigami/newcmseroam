<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Activity;
use App\Currency;
use App\ActivityOperator;
use App\ActivitySupplier;
use App\City;
use App\Label;
use App\ActivityLabelPivot;
use App\ActivityMarkup;
use App\ActivityBasePrice;
use App\ActivityPrice;
use App\ActivityMarkupPercentage;
use App\ActivityMarkupAgentCommission;
use App\ActivityMarkupSupplierCommission;

use Session;
use File;
use Image;
use DB;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callActivityList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity')
            $oRequest->session()->forget('activity');

        session(['page_name' => 'activity']);
        $aData = session('activity') ? session('activity') : array();

        $oRequest->session()->forget('activity');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'a.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oActivityList = Activity::getActivityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivityList->currentPage(),'activity');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._more_activity_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_list' : 'WebView::activity._activity_list_ajax';
        
        return \View::make($oViewName, compact('oActivityList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));  
    }
    
    public function callActivityDelete($nIdActivity) 
    {
        $oActivity = Activity::find($nIdActivity);
        $oActivity->delete();
        return 1;
    }
    public function callActivityCreate(Request $oRequest,$nIdActivity='') 
    {
        
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'                         => 'required',
                                    'city_id'                      => 'required',
                                    'has_accomodation'             => 'required',
                                    'default'                      => 'required',
                                    'pickup'                       => 'required',
                                    'dropoff'                      => 'required',
                                    'has_transport'                => 'required',
                                    'default_activity_supplier_id' => 'required',
                                    'activity_operator_id'         => 'required',
                                    'currency_id'                  => 'required',
                                    'ranking'                      => 'required',
                                    'description'                  => 'required',
                                    'labels'                       => 'required'
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $aLabels = $oRequest->labels;
            $aData  = $oRequest->except([ 'has_destination_city','labels' ]);

            $oActivity = Activity::create($aData);
            if(isset($aLabels)){
                foreach ($aLabels as $label) {
                        $pivot_add = [
                                        'label_id' => $label,
                                        'activity_id' => $oActivity->id
                        ];
                        $pivot = ActivityLabelPivot::create($pivot_add);
                }
            }
            $aArray['from_activity'] = 1;
            $aArray['id'] = $oActivity->id;
            return redirect()->route('activity.activity-season-create',[$aArray['id'],$aArray['from_activity']]);
        }
        $aLable=array();
        if($nIdActivity != '')
        {
            $oActivity = Activity::find($nIdActivity);
            $aLable = ActivityLabelPivot::where('activity_id','=',$nIdActivity)->pluck('label_id')->toArray();
        }
        
        $currencies =  ['' => 'Select Currency'] + Currency::pluck('code','id')->toArray();
        $labels   =  Label::pluck('name','id');
        $cities  = ['' => 'Select City'] + City::pluck('name','id')->toArray();;
        $suppliers = ['' => 'Select Supplier'] + ActivitySupplier::pluck('name','id')->toArray();
        $operators = ['' => 'Select Operator'] + ActivityOperator::pluck('name','id')->toArray();

        return \View::make('WebView::activity.activity_create', compact('oActivity','aLable','cities','activity_types','suppliers','operators', 'currencies','labels'));
    }
     
    public function callActivitySeasonCreate(Request $oRequest,$nId='',$nFromFlag='') 
    {
        $data = Input::except('price');
        if ($oRequest->isMethod('post'))
    	{
            $aRule = array();
            if($nFromFlag == '' && $nId != '')
                $data = Input::except(['price','name','activity_id','date_from','date_to']); // this fields are not updatable
            else {
                $aRule = [  'name' => 'required',
                            'activity_id' => 'required_without:from_flag',
                            'date_from'   => 'required|date',
                            'date_to'     => 'required|date|after:date_from'];
            }
            array_merge($aRule, ['allotment'   => 'required',
                                    'release'     => 'required',
                                    'minimum_pax' => 'required',
                                    'operates'    => 'required',
                                    'currency_id'    => 'required']);
            
            $oValidator = Validator::make($oRequest->all(), $aRule);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $nSeasonId='';
            if($oRequest->from_flag !='')
                $nActivityId = $oRequest->id;
            else{
                $nSeasonId = $oRequest->id;
                $nActivityId= $oRequest->activity_id;
            } 
            $oActivity = Activity::find($nActivityId);
            //print_r($oRequest->days);
            $cancellation_array = [];
            $i = 0;
            foreach ($oRequest->days as $key => $days ) {
                $percent = $data['percent'][$key];
                if($days && $percent)
                {
                    $cancellation_array[$i]['days'] = $days;
                    $cancellation_array[$i]['percent'] = $percent;
                    $i++;
                }
            }
            //print_r($cancellation_array);exit;
            $data['cancellation_formula'] = ($cancellation_array) ? json_encode($cancellation_array) : NULL;
            unset($data['days']);
            unset($data['percent']);
            unset($data['activity_id']);
            
            $base_price = ActivityBasePrice::updateOrCreate(['activity_price_id' => $nSeasonId],['base_price' => $oRequest->price]);
            $data['operates'] = implode(",", $data['operates']);
            $markup = ActivityMarkup::where(['is_default' => TRUE])->first();
            $data = array_merge($data, [
                                        'activity_base_price_id' => $base_price->id,
                                        'activity_markup_id' => $markup->id,
                                        'activity_markup_percentage_id' => $markup->activity_markup_percentage_id,
                                        'activity_supplier_id' => $oActivity->default_activity_supplier_id,
                                        'activity_id' => $nActivityId
                                    ]);

            $season = ActivityPrice::updateOrCreate(['id' => $nSeasonId], $data);
            ActivityBasePrice::where('id','=',$base_price->id)->update(['activity_price_id' => $season->id]);
            if($oRequest->from_flag !=''){
                Session::flash('message', "Successfully added Activity and Activity Season!");
                return Redirect::to('activity/activity-list');
            }else{
                Session::flash('message', "Successfully added Activity Season!");
                return Redirect::to('activity/activity-season-list');
            }
        }
        if($nFromFlag == '' && $nId != ''){
            $oSeason = ActivityPrice::with('currency')->where('id',$nId)->with('base_price')->first();
            $cancellation_formula = $oSeason->cancellation_formula;
            $decoded_formula = json_decode($cancellation_formula);
            $oSeason['operates'] = explode(',',$oSeason['operates']);
        }
        $markups = ActivityMarkup::orderBy('is_default', 'ASC')->where('is_active',1)->with('markup_percentage')->get();
        $activities = ['' => 'Select Activity']+ Activity::pluck('name','id')->toArray();
        $operates   = [ '1' => 'Monday',
                        '2' => 'Tuesday',
                        '3' => 'Wednesday',
                        '4' => 'Thursday',
                        '5' => 'Friday',
                        '6' => 'Saturday',
                        '7' => 'Sunday'
                        ];
        $currencies = ['' => 'Select Currency']+Currency::pluck('code','id')->toArray();
        return \View::make('WebView::activity.activity_season_create',compact('activities','operates','currencies','nId','nFromFlag','oSeason','decoded_formula'));
    }
    
     public function callActivitySeasonList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_season')
            $oRequest->session()->forget('activity_season');

        session(['page_name' => 'activity_season']);
        $aData = session('activity_season') ? session('activity_season') : array();

        $oRequest->session()->forget('activity_season');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        //DB::enableQueryLog();
        switch($sSearchBy)
        {
            case "price":
                    if (is_numeric($sSearchStr)) {
                            $oMarkup         = ActivityMarkup::with(['markup_percentage'])->where(['is_default' => 1])->first();
                            $nBasePrice     = round(( $sSearchStr / 100 ) * (100 - $oMarkup->markup_percentage->percentage ));
                            $aBasePriceIds = ActivityBasePrice::where(['base_price' => $nBasePrice])->pluck('id');
                            $aActivityIds   = ActivityPrice::whereIn('activity_base_price_id', $aBasePriceIds)->pluck('activity_id');
                            $aPriceIds      = ActivityPrice::whereIn('activity_base_price_id', $aBasePriceIds)->pluck('id');
                            $oActivitySeasonList     = Activity::with(['city', 'operator',
                                                                'price' => function($query) use ($aPriceIds)
                                                                        {
                                                                        $query->with(['currency','supplier'])
                                                                            ->whereIn('zactivityprices.id', $aPriceIds);
                                                                        }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                    }
                    break;
            case "allotment":
                    if (is_numeric($sSearchStr)) {
                            $aActivityIds = ActivityPrice::where('allotment', $sSearchStr)->pluck('activity_id');
                            $aPriceIds    = ActivityPrice::where('allotment', $sSearchStr)->pluck('id');
                            $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                    'price' => function($query) use ($aPriceIds)
                                                                            {
                                                                            $query->with(['currency','supplier'])
                                                                                ->whereIn('zactivityprices.id', $aPriceIds);
                                                                            }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                            //$activities->appends(Request::only('search','search-by'))->links();	
                    }
                    break;				
            case "pax":
                    if (is_numeric($sSearchStr)) {
                            $aActivityIds = ActivityPrice::where('minimum_pax', $sSearchStr)->pluck('activity_id');
                            $aPriceIds    = ActivityPrice::where('minimum_pax', $sSearchStr)->pluck('id');
                            $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                    'price' => function($query) use ($aPriceIds)
                                                                            {
                                                                            $query->with(['currency','supplier'])
                                                                                ->whereIn('zactivityprices.id', $aPriceIds);
                                                                            }
                                                            ])
                                                    ->whereIn('id', $aActivityIds)
                                                    ->orderBy($sOrderField, $sOrderBy)
                                                    ->paginate($nShowRecord);
                            //$activities->appends(Request::only('search','search-by'))->links();
                    }
                    break;
            case "name":
                    $aSeasonIds   = ActivityPrice::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $aActivityIds = ActivityPrice::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('activity_id');
                    $oActivitySeasonList   = Activity::with(['city', 'operator',
                                                                'price' => function($query) use ($aSeasonIds)
                                                                        {
                                                                        $query->with(['currency','supplier']);
                                                                        $query->whereIn('zactivityprices.id', $aSeasonIds);
                                                                        }
                                                        ])
                                                ->whereIn('id', $aActivityIds)
                                                ->orderBy($sOrderField, $sOrderBy)
                                                ->paginate($nShowRecord);					
                    //$activities->appends(Request::only('search','search-by'))->links();
                    break;
            case "supplier":
                    $aSupplierIds = ActivitySupplier::where('name', 'LIKE', '%' . $sSearchStr . '%')->pluck('id');
                    $aActivityIds = ActivityPrice::whereIn('activity_supplier_id', $aSupplierIds)->pluck('activity_id');
                    $aPriceIds    = ActivityPrice::whereIn('activity_supplier_id', $aSupplierIds)->pluck('id');
                    $oActivitySeasonList   = Activity::whereIn('id', $aActivityIds)
                                            ->with(['City', 'Operator',
                                                            'price' => function($query) use ($aPriceIds, $aSupplierIds)
                                                            {
                                                                    $query->whereIn('zActivityPrices.id', $aPriceIds)
                                                                            ->with(['currency',
                                                                                    'supplier' => function($query) use ($aSupplierIds)
                                                                                    {
                                                                                    $query->whereIn('zactivitysuppliers.id', $aSupplierIds);
                                                                                    }
                                                                            ]);
                                                            }
                                                    ])
                                            ->orderBy($sOrderField, $sOrderBy)
                                            ->paginate($nShowRecord);
                    //$activities->appends(Request::only('search','search-by'))->links();
                    break;
            default:
                $oActivitySeasonList = Activity::getActivitySeasonList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        }
        
        //dd(DB::getQueryLog());
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivitySeasonList->currentPage(),'activity_season');
        
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._more_activity_season_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_season_list' : 'WebView::activity._activity_season_list_ajax';
        
        return \View::make($oViewName, compact('oActivitySeasonList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function callActivitySeasonDelete($nIdActivity) 
    {
        $oSeason = ActivityPrice::with('base_price')->find($nIdActivity);
        $oSeason->delete();
        return 1;
    }
    
    public function callActivitySupplierList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_supplier')
            $oRequest->session()->forget('activity_supplier');

        session(['page_name' => 'activity_supplier']);
        $aData = session('activity_supplier') ? session('activity_supplier') : array();

        $oRequest->session()->forget('activity_supplier');
        
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function() use($nPage) {
            return $nPage;
        });
        
        $oActivitySupplierList = ActivitySupplier::getActivitySupplierList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);

        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oActivitySupplierList->currentPage(),'activity_supplier');
        
        if($oRequest->page > 1)
            $oViewName =  'WebView::activity._more_activity_supplier_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::activity.activity_supplier_list' : 'WebView::activity._activity_supplier_list_ajax';
        
        return \View::make($oViewName, compact('oActivitySupplierList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function callActivitySupplierCreate(Request $oRequest,$nIdActivitySupplier='')
    {
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name' => 'required',
                                    'marketing_contact_title' => 'required',
                                    'marketing_contact_name' => 'required',
                                    'marketing_contact_email' => 'required',
                                    'marketing_contact_phone' => 'required',
                                    'reservation_contact_name' => 'required',
                                    'reservation_contact_email' => 'required',
                                    'reservation_contact_landline' => 'required',
                                    'reservation_contact_free_phone' => 'required',
                                    'accounts_contact_name' => 'required',
                                    'accounts_contact_email' => 'required',
                                    'accounts_contact_title' => 'required',
                                    'accounts_contact_phone' => 'required',
                                    'logo' => 'required_without:id_activity|image',
                                    'description' => 'required',
                                    'special_notes' => 'required',
                                    'remarks' => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aData = $oRequest->except([ 'id_activity','_token' ]);
            if(Input::hasFile('logo')){
                    $image   = Input::file('logo');
                    $image_validator = image_validator($image);
                // check if image is valid
                if( $image_validator['fails'] ){
                    //$error = ['message' => $image_validator['message']];
                }else{
                    $filename  = time(). '_' . $image->getClientOriginalName();
                    $destination = public_path('uploads/activities/supplier');
                    File::makeDirectory( public_path($destination), 0775, FALSE, TRUE);
                    $image->move($destination, $filename);
                    $data['logo'] = $filename;
                }
            }    
            
            //insert data
            $oSupplier = ActivitySupplier::updateOrCreate(['id' => $oRequest->id_activity],$aData);
            Session::flash('message', "Successfully added Activity Supplier!");
            return Redirect::route('activity.activity-supplier-list');
        }
        $oActivitySupplier = ActivitySupplier::where('id',$nIdActivitySupplier)->first();
        return \View::make('WebView::activity.activity_supplier_create',compact('nIdActivitySupplier','oActivitySupplier'));
    }
    
    public function callActivityMarkupList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'activity_mark')
            $oRequest->session()->forget('activity_mark');

        session(['page_name' => 'activity_mark']);
        $aData = session('activity_mark') ? session('activity_mark') : array();

        $oRequest->session()->forget('activity_mark');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'am.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        
        $oActivityMarkupList = ActivityMarkup::getActivityMarkupList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','activity_mark');
        
        if ($oActivityMarkupList) {
            foreach ($oActivityMarkupList as $key => $value) { //print_r($value);exit;
                switch ($value->allocation_type) {
                    case 'activity':
                        //echo 'activity'.$value->allocation_type,$value['allocation_id'];exit;
                        $sActivity = Activity::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'Activity - ' .$sActivity;
                        break;
                    case 'city':
                        $sCity = City::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'City - ' .$sCity;
                        break;
                    case 'country':
                        $sCountry = Country::where('id',$value['allocation_id'])->value('name');
                        $oActivityMarkupList[$key]['allocation_name'] = 'Country - ' . $sCountry;
                        break;
                    case 'all':
                        $oActivityMarkupList[$key]['allocation_name'] = 'All';
                        break;
                }
            }
        }

        $oViewName =  ($oRequest->isMethod('Get')) ? 'WebView::activity.activity_markup_list' : 'WebView::activity._more_activity_markup_list';
        
        
        return \View::make($oViewName, compact('oActivityMarkupList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy')); 
    }
    
    public function callActivityMarkupCreate(Request $oRequest, $nIdMarkup ='') 
    {
        if ($oRequest->isMethod('post'))
    	{
            $allocation_id_validator = '';
            if($oRequest->allocation_type != 'all')
                $allocation_id_validator = 'required';
            
            $data = Input::only('name','description','allocation_id');
            $data['allocation_type'] =  $oRequest->allocation_type;
            $data['is_active']  = ( !$oRequest->is_active ) ? FALSE: TRUE;
            $data['is_default'] = ( !$oRequest->is_default ) ? FALSE: TRUE;
            //print_r($data);exit; 
            $oValidator = Validator::make($oRequest->all(), [
                                        'name'                => 'required',
                                        'description'         => 'required',
                                        'allocation_type'     => 'required',
                                        'allocation_id'       => $allocation_id_validator,
                                        'markup_percentage'   => 'required',
                                        'agent_percentage'    => 'required',
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            if( $oRequest->is_default )
                ActivityMarkup::where('is_default', TRUE)->update(['is_default' => FALSE]);
            
            $oActivityMarkup = ActivityMarkup::updateOrCreate(['id' => $oRequest->id_activity ], $data );
            
            $oAMP = ActivityMarkupPercentage::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]); 
            $oAMP->percentage =  $oRequest->markup_percentage;
            $oAMP->save();
            
            $oAMAC = ActivityMarkupAgentCommission::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]);
            $oAMAC->percentage =  $oRequest->agent_percentage;
            $oAMAC->save();
            
            $oAMS = ActivityMarkupSupplierCommission::firstOrNew(['activity_markup_id' 	=> $oActivityMarkup->id]);
            $oAMS->percentage =  $oRequest->supplier_percentage;
            $oAMS->save();
            ActivityMarkup::where('id', $oActivityMarkup->id)->update([
                            'activity_markup_percentage_id' => $oAMP->id,
                            'activity_markup_agent_commission_id' => $oAMAC->id,
                            'activity_markup_supplier_commission_id' => $oAMS->id
                    ]);
            
            return Redirect::to('activity/activity-markup-list');
	   
        }
        if($nIdMarkup !=''){
            $oActivityMarkup = ActivityMarkup::where('id',$nIdMarkup)->with(['markup_percentage','agent_commission','supplier_commission'])->first()->toArray();
            $oActivityMarkup['markup_percentage'] = $oActivityMarkup['markup_percentage']['percentage'];
            $oActivityMarkup['agent_percentage'] = $oActivityMarkup['agent_commission']['percentage'];
            $oActivityMarkup['supplier_percentage'] = $oActivityMarkup['supplier_commission']['percentage'];
        }
                
        return \View::make('WebView::activity.activity_markup_create',compact('nIdMarkup','oActivityMarkup'));
    }
    //markup create
    public function getAllActivity(){
        return Activity::select('id', 'name')->get();
    }
}
