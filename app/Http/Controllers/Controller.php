<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\User;
use Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function loginUser($credentials) {
        $username = $credentials['username'];
        $password = $credentials['password'];

        $user = User::where(['username' => $username])->first();
        
        if ($user && Hash::check($password, $user->password) && ($user->type == 'admin' || $user->type == 'licensee' || $user->type == 'agent')) {
            // exists
            $user['auth'] = 'valid';
            // exsits but inactive
            if ($user->active != 1) {
                $user['auth'] = 'inactive';
            }
        } else {
            //does not exists
            $user['auth'] = 'invalid';
        }
        return $user;
    }

}
