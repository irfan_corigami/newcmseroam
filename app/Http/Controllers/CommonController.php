<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;

use App\Label;
use App\Region;
use App\Country;
use App\City;
use App\CitiesLatLong;
use App\Timezone;
use App\HBDestination;
use App\AECity;
use App\AOTLocation;
use App\AOTMapSupplierCity;
use App\HBDestinationsMapped;
use App\AECitiesMapped;
use App\ViatorLocationsMapped;
use App\CityImage;
use App\Coupon;
use App\Currency;

use App\HotelCategory;
use App\HotelRoomType;
use App\Nationality;
use App\AgeGroup;
        
use Session;
use File;
use Image;

class CommonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function callLabelList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'label')
            $oRequest->session()->forget('label');

        session(['page_name' => 'label']);
        $aData = session('label') ? session('label') : array();
        $oRequest->session()->forget('label');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;   
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oLabelList = Label::geLabelList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,$oLabelList->currentPage(),'label');

        if($oRequest->page > 1)
            $oViewName =  'WebView::common._more_label_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.label_list' : 'WebView::common._label_list_ajax';
        
        return \View::make($oViewName, compact('oLabelList','sSearchStr','sOrderField','sOrderBy','nShowRecord'));
    }
    
    public function callLabelCreate(Request $oRequest,$nIdLabel='') 
    {
        session(['page_name' => 'label']);
        if($nIdLabel != '')
            $oLabel = Label::find($nIdLabel);
        if ($oRequest->isMethod('post'))
    	{
            $oValidator = Validator::make($oRequest->all(), [
                                    'name'=> 'required|unique:zlabels,name' 
                                    ]);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $data=array();
            $data['name'] = $oRequest->name;
            $oLabel = Label::firstOrNew(['id' => $oRequest->label_id],$data);
            $oLabel->save();
            Session::flash('message', trans('messages.label_success_msg'));
            return Redirect::back();
        }
        return \View::make('WebView::common.label_create',compact('oLabel'));
    }
    
    public function callLabelDelete($nIdLabel)
    {
        $oLabel = Label::find($nIdLabel);
        $oLabel->delete();
    }
    
    public function callRegionList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'region')
            $oRequest->session()->forget('region');

        session(['page_name' => 'region']);
        $aData = session('region') ? session('region') : array();
        $oRequest->session()->forget('region');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oRegionList = Region::geRegionList($sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,$nShowRecord,'','region');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.region_list' : 'WebView::common._region_list_ajax';
        
        return \View::make($oViewName, compact('oRegionList','sSearchStr','sOrderField','sOrderBy','nShowRecord')); 
    }
    
    public function callRegionSwitch(Request $oRequest,$nRegionId) 
    {
        session(['page_name' => 'region']);
        if ($oRequest->isMethod('post') && $nRegionId !='')
    	{
            $oRegion = Region::find($nRegionId );
            $oRegion->show_on_eroam = $oRequest->show;
            $oRegion->update();
        }
    }
    
    public function callCountryList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'country')
            $oRequest->session()->forget('country');

        session(['page_name' => 'country']);
        $aData = session('country') ? session('country') : array();
        $oRequest->session()->forget('country'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'c.name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
        
        $oCountryList = Country::geCountryList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oCountryList->currentPage(),'country');
        if($oRequest->page > 1)
            $oViewName =  'WebView::common._more_country_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.country_list' : 'WebView::common._country_list_ajax';
        
        return \View::make($oViewName, compact('oCountryList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callCountrySwitch(Request $oRequest,$nCountryId) 
    {
        session(['page_name' => 'country']);
        if ($oRequest->isMethod('post') && $nCountryId !='')
    	{
            $oCountry = Country::find($nCountryId);
            $oCountry->show_on_eroam = $oRequest->show;
            $oCountry->update();
        }
    }
    
    public function callCityList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'city')
            $oRequest->session()->forget('city');

        session(['page_name' => 'city']);
        $aData = session('city') ? session('city') : array();
        $oRequest->session()->forget('city'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'c.name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });
      
        $oCityList = City::geCityList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oCityList->currentPage(),'city');

        if($oRequest->page > 1)
            $oViewName =  'WebView::common._more_city_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.city_list' : 'WebView::common._city_list_ajax';
        
        return \View::make($oViewName, compact('oCityList','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callCityCreate(Request $oRequest,$nIdCity='') 
    {
        session(['page_name' => 'city']);
        if ($oRequest->isMethod('post'))
    	{
            //echo "<pre>";var_dump(json_decode($oRequest->aot_location_codes));exit;
            $aValidationRules = [
                                    'name'=>'required',
                                    'description'=>'required',	
                                    'optional_city'=>'required',
                                    'default_nights'=>'required|numeric',
                                    'country_id' =>'required',
                                    'lat' => 'required',
                                    'lng' => 'required'
                                    ];
            if($oRequest->city_id!='')
                $aValidationRules['timezone_id'] = 'required';
            
            //print_r($aValidationRules);exit;
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRules);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            $oCity = City::firstOrNew(['id' => $oRequest->city_id]);
            $oCity->name = $oRequest->name;
            $oCity->description = $oRequest->description;
            $oCity->default_nights = $oRequest->default_nights;
            $oCity->optional_city = $oRequest->optional_city;
            $oCity->country_id = $oRequest->country_id;
            $oCity->is_disabled =  ($oRequest->enabled ) ? FALSE : TRUE;
            $oCity->region_id =  0;
            if($nIdCity != '')
                $oCity->timezone_id = $oRequest->timezone_id;
            $oCity->save();
            
            $oLatlong = CitiesLatLong::firstOrNew(['city_id' => $oCity->id]);
            $oLatlong->city_id = $oCity->id;
            $oLatlong->lat = $oRequest->lat; 
            $oLatlong->lng = $oRequest->lng; 
            $oLatlong->save();
            
            if($oRequest->city_id != '')
            {
                //Hbdestination mapped table entry
                //there is no data for mapped with country id this insert is not in use
                $aHbDestinations = json_decode($oRequest->hb_destination_codes);
                HBDestinationsMapped::where('city_id', $oRequest->city_id)->delete();
                if($aHbDestinations && count($aHbDestinations)>0)
                {
                    foreach( $aHbDestinations as $aHb )
                    {
                        $aHbDestination = HBDestinationsMapped::withTrashed()
                                                            ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                            'hb_destination_id' => $aHb
                                                                        ]);
                        if ($aHbDestination->trashed()) 
                            $aHbDestination->restore();
                        else
                            $aHbDestination->save();
                    }
                }
                //AotSupplier mapped table entry
                $aAotSupplier = json_decode($oRequest->aot_location_codes);
                AOTMapSupplierCity::where('city_id', $oRequest->city_id)->delete();
                if($aAotSupplier && count($aAotSupplier)>0)
                {
                    foreach( $aAotSupplier as $aAot )
                    {
                        $oAotSupplier = AOTMapSupplierCity::withTrashed()
                                                        ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                        'location_id' => $aAot
                                                                    ]);
                        if ($oAotSupplier->trashed()) 
                            $oAotSupplier->restore();
                        else
                            $oAotSupplier->save();
                    }
                }
                //viator destination mapped table entry
                //there is no data for mapped with country id this insert is not in use
                $aViatorDestinations = json_decode($oRequest->viator_destination_ids);
                ViatorLocationsMapped::where('city_id', $oRequest->city_id)->delete();
                if($aViatorDestinations && count($aViatorDestinations)>0)
                {
                    foreach( $aViatorDestinations as $aVd )
                    {
                        $aViatorDestination = ViatorLocationsMapped::withTrashed()
                                                                    ->firstOrNew([ 'city_id'=> $oRequest->city_id,
                                                                                    'viator_destination_id' => $aVd
                                                                                ]);
                        if ($aViatorDestination->trashed()) 
                            $aViatorDestination->restore();
                        else
                            $aViatorDestination->save();
                    }
                }
                //Ae City mapped table entry
                $aAeCities = json_decode($oRequest->ae_region_ids);
                //print_r($aAeCities);exit;
                AECitiesMapped::where('eroam_city_id', $oRequest->city_id)->delete();
                if($aAeCities && count($aAeCities)>0)
                {
                    foreach( $aAeCities as $aAe )
                    {
                        $oAeCity = AECitiesMapped::withTrashed()
                                                ->firstOrNew([ 'eroam_city_id'=> $oRequest->city_id,
                                                                'ae_city_id' => $aAe
                                                            ]);
                        if ($oAeCity->trashed()) 
                            $oAeCity->restore();
                        else
                            $oAeCity->save();
                    }
                }
            }
                        
            Session::flash('message', trans('messages.label_success_msg'));
            return Redirect::back();
        }
        
        if($nIdCity != '')
        {
            $oCity = City::where('id', $nIdCity)
                        ->with([
                                'country','image',
                                'latlong', 
                                'timezone','hb_destinations_mapped',
                                'aot_supplier_locations_mapped' => function( $aot_mapped_q ){
                                    $aot_mapped_q->with(['locations' => function( $aot_location_q ){
                                            $aot_location_q->addSelect('id', 'LocationCode', 'LocationName', 'LocationType');
                                    }]);
                                },
                                'viator_locations_mapped' => function( $viator_mapped_q ){
                                    $viator_mapped_q->with(['viator_location' => function( $viator_destination_q ){
                                            $viator_destination_q->addSelect('id', 'destinationId', 'destinationName', 'destinationType');
                                    }]);
                                },
                                'ae_city_mapped' => function( $ae_mapped_q ){
                                    $ae_mapped_q->with(['ae_city' => function( $ae_city_q ){
                                            $ae_city_q->addSelect('id', 'RegionId', 'Name');
                                    }]);
                                }
                            ])
                        ->first();
            // AOT
            $selected_aot_suppier_location = $oCity->aot_supplier_locations_mapped->toArray();
            $selected_aot_suppier_locations = array_column($selected_aot_suppier_location, 'locations');
            
            $aAeCityMapSelected = $oCity->ae_city_mapped->toArray();
            $aAeCityMapSelected = array_column($aAeCityMapSelected, 'ae_city');
            
            $oTimezone = Timezone::where('country_code', $oCity->country->code)->select('name','id')->get();
            //there is no use of hb destination coz no country code addess
            //$oHBDestinations = HBDestination::getHbDestination($oCity->country->code);
            $oRegion = AECity::getAECity($oCity->country->code)->toArray();
            $oRegion = json_encode($oRegion);
        }
        $oCountry = Country::orderBy('name','asc')->get();
        
        return \View::make('WebView::common.city_create',compact('oCountry','nIdCity','oTimezone','oHBDestinations','oRegion','oCity','aAeCityMapSelected','selected_aot_suppier_locations'));
    }
    
    public function callCityDelete($nIdCity)
    {
        $oCity = City::find($nIdCity);
        $oCity->delete();
    }
    
    public function getAotLocations($sType) 
    {
        $oAotLocation = AOTLocation::getAotlocations($sType);
        return $oAotLocation;
    }
    
    public function callCityImageUpload(Request $oRequest) 
    {
        //echo "sdfsd";exit;
        $success = FALSE;
    	$data    = array();
    	$error   = array();    
        if(Input::hasFile('city_image'))
        {

            $nCityId        = $oRequest->city_id;
            $image           = Input::file('city_image')[0];
            $image_validator = image_validator($image);
            // check if image is valid
            if( $image_validator['fails'] )
            {
                $error = ['message' => $image_validator['message']];
            }
            else
            {
                $nCount = CityImage::where(['city_id' => $nCityId, 'is_primary' => 1 ])->count(); 
                // if there is NO existing image for the resource then set current uploaded image as primary
                $bIsPrimary = ($nCount) ? 0 : 1;
                $values     = array('city_id' => $nCityId, 'is_primary' => $bIsPrimary);
                $filename   = $nCityId.'_'.date('YmdHis').'.'.$image->getClientOriginalExtension();
                // the path where the image is saved
                $destination = 'uploads/cities/'.$nCityId;
                // CREATE ORIGINAL IMAGE
                $original_path = $destination.'/'.$filename;		    		
                File::makeDirectory( public_path($destination), 0775, FALSE, TRUE); // create directory if not existing yet
                $image->move($destination, $filename); 
                $values = array_merge($values, ['original' => $original_path]);

                // CREATE THUMBNAIL IMAGE
                $thumbnail_path = $destination.'/thumbnail/'.$filename; // set thumbnail path
                $thumbnail_img  = Image::make($original_path); // create image intervention object from original image
                $thumbnail_img  = resize_image_to_thumbnail( $thumbnail_img ); // resize; found in helpers.php
                File::makeDirectory( public_path($destination).'/thumbnail', 0775, FALSE, TRUE);
                $thumbnail_img->save($thumbnail_path); // store image 
                $values = array_merge($values, ['thumbnail' => $thumbnail_path]); // thumbnail path to be stored in db

                // CREATE SMALL IMAGE
                $small_path = $destination.'/small/'.$filename; // set small path
                $small_img  = Image::make($original_path); // create image intervention object from original image
                $small_img  = resize_image_to_small( $small_img ); // resize; found in helpers.php
                File::makeDirectory( public_path($destination).'/small', 0775, FALSE, TRUE);
                $small_img->save($small_path); // store image 
                $values = array_merge($values, ['small' => $small_path]); // small path to be stored in db		

                // CREATE MEDIUM IMAGE
                $medium_path = $destination.'/medium/'.$filename; // set medium path
                $medium_img  = Image::make($original_path); // create image intervention object from original image
                $medium_img  = resize_image_to_medium( $medium_img ); // resize; found in helpers.php
                File::makeDirectory( public_path($destination).'/medium', 0775, FALSE, TRUE);
                $medium_img->save($medium_path); // store image 
                $values = array_merge($values, ['medium' => $medium_path]); // medium path to be stored in db	

                // CREATE LARGE IMAGE
                $large_path = $destination.'/large/'.$filename; // set large path
                $large_img  = Image::make($original_path); // create image intervention object from original image
                $large_img  = resize_image_to_large( $large_img ); // resize; found in helpers.php
                File::makeDirectory( public_path($destination).'/large', 0775, FALSE, TRUE);
                $large_img->save($large_path); // store image 
                $values = array_merge($values, ['large' => $large_path]); // large path to be stored in db	

                // STORE TO DATABASE
                $data = CityImage::create( $values );

                if( $data )
                {
                        $success = TRUE;
                        $data    = ['message' => 'Successfully added image.', $data->toArray()];
                    }
                    else
                    {
                        $error   = ['message' => 'Failed to add image.'];
                    }
            }			
        }
        else
        {
                $error = ['message' => 'You have not chosen a file.'];
        }		
        return response_format($success, $data, $error);
    }
    
    public function callCityImageDelete($nIdCityImage) 
    {
        $data    = array();
        $success = TRUE;
        $image = CityImage::where(['id' => $nIdCityImage])->first();
        if( $image )
        {
            $error = array();
            File::delete( public_path( $image->thumbnail ) );
            File::delete( public_path( $image->small ) );
            File::delete( public_path( $image->medium ) );
            File::delete( public_path( $image->large ) );
            File::delete( public_path( $image->original ) );
            $delete = CityImage::where(['id' => $nIdCityImage])->delete();
            if(!$delete){
                    $success = FALSE;
                    $error = ['message' => 'An error has occured while trying to delete the image.'];
            }else
            {
                $data = ['message' => 'Successfully deleted image.'];
                if($image->is_primary == TRUE){
                    $count = CityImage::where(['city_id' => $image->city_id])->count();
                    if( $count > 1){
                        $new_primary_image = CityImage::where(['city_id' => $image->city_id])->first();
                        CityImage::where(['id' => $new_primary_image->id])->update(['is_primary' => TRUE]);
                    }	
                }
            }
        }else{
                $error = ['message' => 'An error has occured. Image not found.'];
        }
        return response_format($success, $data, $error);
        
    }
    
    public function callSetCityImagePrimary($nIdCityImage) 
    {
        $data    = array();
        $success = FALSE;
        $oCityImages = CityImage::where(['id' => $nIdCityImage])->first();
        if( $oCityImages ){

            $error = array();
            // get the current primary picture of the city and change "is_primary" to false;
            $images = CityImage::where(['city_id' => $oCityImages->city_id, 'is_primary' => TRUE])->get()->toArray();
            if( $images ){
                foreach($images as $image){
                    CityImage::where(['id' => $image['id']])->update(['is_primary' => FALSE]);
                }
            }
            $result = CityImage::where(['id' => $nIdCityImage])->update(['is_primary' => TRUE]);
            if($result){
                $success = TRUE;
                $data    = ['message' => 'Successfully updated primary picture.'];
            }
            else
                $error = ['message' => 'An error has occured. Failed to udpate primary picture.'];

        }else{
            $error = ['message' => 'An error has occured. Image not found.'];
        }
        return response_format($success, $data, $error);
        
    }
    
    public function callCouponList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'coupon')
            $oRequest->session()->forget('coupon');

        session(['page_name' => 'coupon']);
        $aData = session('coupon') ? session('coupon') : array();
        $oRequest->session()->forget('coupon'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'title');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
      
        $oCouponList = Coupon::geCouponList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,'','','coupon');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.coupon_list' : 'WebView::common._more_coupon_list';
        
        return \View::make($oViewName, compact('oCouponList','sSearchStr','sOrderField','sOrderBy','sSearchBy'));
    }
    
    public function callCouponCreate(Request $oRequest,$nIdCoupon='') 
    {
        session(['page_name' => 'coupon']);
        if($nIdCoupon != '')
            $oCoupon = Coupon::find($nIdCoupon);
        if ($oRequest->isMethod('post'))
    	{
            $aValidation = [
                                'title'=>'required',
                                'code'=>'required|unique:zcoupons,code,'.$oRequest->coupon_id,
                                'start_date'=>'required',
                                'end_date'=>'required',
                                'coupon_type'=>'required',
                            ];
            
            if($oRequest->coupon_type == 'flat'){
                $aValidation ['coupon_currency']= 'required';
                $aValidation ['amount']= 'required|numeric';
            } else{
                $aValidation ['amount']= 'required|numeric|between:0,100';
            }
            $oValidator = Validator::make($oRequest->all(), $aValidation);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aCouponData = $oRequest->all();
            unset($aCouponData['_token'],  $aCouponData['btnAddCoupon'],$aCouponData['coupon_id']);
            
            $aCouponData['is_active'] = 1;
            $aCouponData['is_deleted']= 0;
            $oCoupon = Coupon::updateOrCreate(['id' => $oRequest->coupon_id],$aCouponData);
            //$oCoupon->update($aCouponData);
            
            Session::flash('message', trans('messages.label_success_msg'));
            return Redirect::back();
        }
        $oCurrencies  = [''=>'Select Currency'] + Currency::pluck('code','id')->toArray();
        return \View::make('WebView::common.coupon_create',compact('oCoupon','oCurrencies','nIdCoupon'));
    }
    
    public function callManageAction(){
        
        session(['page_name' => 'coupon']);
        $aCoupons = Input::get('coupons');
        $sAction = Input::get('action');

        switch ($sAction) {
            case 'delete':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_deleted' => 1, 'deleted_by' => 'Admin');
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon Deleted Successfully.';
                break;

            case 'activate':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_active' => 1);
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon Activated Successfully.';
                break;
            case 'deactivate':
                if ($aCoupons) {
                    foreach ($aCoupons as $coupon) {
                        $update_data = array('is_active' => 0);
                        Coupon::where('id', $coupon)->update($update_data);
                    }
                }
                $msg = 'Coupon De-activated Successfully.';
                break;
            default:
                break;
        }
        Session::flash('message', $msg);
        return 'success';
    }
    
    public function getCityCountryWise()
    {
        $country_id = Input::get('country_id');	
        return City::select('id', 'name', 'country_id')->where('country_id', $country_id)->orderBy('name')->get();
    }
    public function getCountryRegionWise()
    {
        $region_id = Input::has( 'region_id' ) ? Input::get( 'region_id' ) : null;
        return Country::where( ['region_id' => $region_id] )->orderBy('name', 'asc')->get();
    }
    
    public function callCurrencyList(Request $oRequest) 
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'currency')
            $oRequest->session()->forget('currency');

        session(['page_name' => 'currency']);
        $aData = session('currency') ? session('currency') : array();
        $oRequest->session()->forget('currency'); 
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'name');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'asc');
      
        $oCurrencyList = Currency::geCurrencyList($sSearchStr,$sOrderField,$sOrderBy);
        setSession($sSearchStr,'',$sOrderField,$sOrderBy,'','','currency');
        
        $oViewName = $oRequest->isMethod('GET') ? 'WebView::common.currency_list' : 'WebView::common._more_currency_list';
        
        return \View::make($oViewName, compact('oCurrencyList','sSearchStr','sOrderField','sOrderBy'));
    }
    
    public function callCurrencyCreate(Request $oRequest,$nIdCurrency='')
    {
        session(['page_name' => 'currency']);
        if($nIdCurrency != '')
            $oCurrency = Currency::find($nIdCurrency);
        if ($oRequest->isMethod('post'))
    	{
            $aValidation = [
                            'name'=>'required|unique:zCurrencies,name,'.$oRequest->currency_id,
                            'code'=>'required|unique:zCurrencies,code,'.$oRequest->currency_id,
                            ];

            $oValidator = Validator::make($oRequest->all(), $aValidation);
            if($oValidator->fails()) 
            {
                return Redirect::back()->withErrors($oValidator)->withInput();
            }
            
            $aCurrencyData['name'] = $oRequest->name;
            $aCurrencyData['code'] = $oRequest->code;

            $oCoupon = Currency::updateOrCreate(['id' => $oRequest->currency_id],$aCurrencyData);

            Session::flash('message', trans('messages.currency_success_msg'));
            return Redirect::route('common.currency-list');
        }
        return \View::make('WebView::common.currency_create',compact('oCurrency','nIdCurrency','nIdCurrency'));
    }
    
    public function getAllCountry(){
        return Country::select('id', 'name')->orderBy('name', 'asc')->get();
    }
    public function getAllCity(){
        return City::select('id', 'name')->orderBy('name', 'asc')->get();
    }
}
