<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;

use App\RoutePlan;
use App\City;
use App\Route;
use App\RouteLeg;
use App\Transport;

use DB;
class RoutePlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getRouteList(Request $oRequest)
    {
        //remove session when it comes from sidebar
        if(session('page_name') != 'route')
            $oRequest->session()->forget('route');

        session(['page_name' => 'route']);
        $aData = session('route') ? session('route') : array();

        $oRequest->session()->forget('route');
        
        $sSearchStr = ($oRequest->has('search_str')) ? $oRequest->search_str : ((count($aData)) ? $aData['search_str'] : Null);
        $sSearchBy = ($oRequest->has('search_by')) ? $oRequest->search_by : ((count($aData)) ? $aData['search_by'] : 'name');
        $sOrderField = ($oRequest->has('order_field')) ? $oRequest->order_field : ((count($aData)) ? $aData['order_field'] : 'r.id');
        $sOrderBy = ($oRequest->has('order_by')) ? $oRequest->order_by : ((count($aData)) ? $aData['order_by'] : 'desc');
        $nStart = ($oRequest->has('start')) ? $oRequest->start : NULL;
        $nFinish = ($oRequest->has('finish')) ? $oRequest->finish : NULL;
        $nShowRecord = ($oRequest->has('show_record')) ? $oRequest->show_record : ((count($aData)) ? $aData['show_record'] : 10);
        $nPage = ($oRequest->has('page')) ? $oRequest->page : ((count($aData)) ? $aData['page_number'] : 1);
        if(count($aData) &&  $sSearchStr != $aData['search_str'])
            $nPage = 1;
        
        Paginator::currentPageResolver(function () use ($nPage) {
            return $nPage;
        });

        $oRouteList = RoutePlan::getRouteList($sSearchBy,$sSearchStr,$sOrderField,$sOrderBy,$nShowRecord,$nStart,$nFinish);
        setSession($sSearchStr,$sSearchBy,$sOrderField,$sOrderBy,$nShowRecord,$oRouteList->currentPage(),'route');
        
        $oCities = City::with('country')->orderBy('name')->get();
        if($oRequest->page > 1)
            $oViewName =  'WebView::route._more_route_list';
        else
            $oViewName = $oRequest->isMethod('GET') ? 'WebView::route.route_list' : 'WebView::route._route_list_ajax';
        return \View::make($oViewName,compact('oRouteList', 'oCities','sSearchStr','sOrderField','sOrderBy','nShowRecord','sSearchBy'));
    }
    
    public function callRouteCreate(Request $oRequest) 
    {
        if($oRequest->isMethod('post'))
        {
            $city_ids = $oRequest->cityIds;
		
            // INSERT ROUTEPLAN
            $oRoutePlan = RoutePlan::firstOrNew([
                    'name' => City::find(current($city_ids))->name . ' to ' .City::find(end($city_ids))->name,
                    'from_city_id' => current($city_ids),
                    'to_city_id' => end($city_ids)
            ]);
            $oRoutePlan->save();
            // set default if not found
            $is_default = Route::where(['route_plan_id' => $oRoutePlan->id])->first() ? 0 : 1;

            // INSERT ROUTE
            $route = Route::create([
                            'route_plan_id' => $oRoutePlan->id,
                            'is_default' => $is_default
                        ]);

            // INSERT ROUTE LEGS
            foreach ($city_ids as $key => $value) {
                RouteLeg::create([
                        'route_id' => $route->id,
                        'city_id' => $value,
                        'sequence' => $key
                ]);
            }

            return $route;
        }
        $cities = City::with('country')->with('latlong')->get();
        return \View::make('WebView::route.route_create',compact('cities'));
    }
    
    public function callRouteView($nIdRoute) 
    {
        $route_plan = RoutePlan::with('routes')->find($nIdRoute);
        if (!$route_plan) {
                return Redirect::to('route-plans');
        }
        $routes = $route_plan->routes;
        return \View::make('WebView::route.route_view',compact('route_plan', 'routes'));
    }
    
    public function getTransportByCity(Request $oRequest)
    {
        $aInput = $oRequest->all();

        $oTransports = Transport::with('price','transporttype', 'currency', 'operator')
                                ->where('from_city_id',$aInput['from_city_id'])
                                ->where('to_city_id',$aInput['to_city_id'])
                                ->where('[default]',1)
                                ->get();					   


        $result = array();

        foreach($oTransports as $transport_key => $transport)
        {	
                $price_count = count($transport->price);
                foreach($transport->price as $price_key => $price)
                {
                    if($price->transport_supplier_id != $transport->default_transport_supplier_id)
                    {
                            $price_count--;
                            unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
                    }

                    if( $price->price == "0.0" )
                    {
                            $price_count--;
                            unset($transport->price[$price_key]); // remove price/room if the supplier is not the hotel's default supplier
                    }
                }

                if($price_count > 0) // remove hotel if there is no price/room
                {
                    array_push($result, $transport);
                }			
        }				   
        return \Response::json(['status'=>200,'data'=>$result]);
    }
    
    public function callDeleteRoute(Request $oRequest)
    {
        $nRouteId = $oRequest->routeId;
        $oRoute = Route::find($nRouteId);
        $nRoutePlanId = $oRoute->route_plan_id;
        $oRoute->delete();

        // CHECK IF DEFAULT WAS DELETED
        $oRoutePlan = Route::where(['route_plan_id' => $nRoutePlanId])->first();
        if ($oRoute->is_default == 1 && $oRoutePlan) {
                $oRoutePlan->is_default = 1;
                $oRoutePlan->save();
        }
        $oRoutePlans = RoutePlan::find($nRoutePlanId);
        if (count($oRoutePlans->routes) == 0) {
            $oRoutePlans->delete();
        }
    }
    public function callSetDefaultRoute(Request $oRequest) {
        $nRouteId = $oRequest->routeId;
        $oRoutes = Route::where(['route_plan_id' => $oRequest->routePlanId ])->get();
        foreach ($oRoutes as $aRoute) {
            $aRoute->is_default = ($aRoute->id == $nRouteId) ? 1 : 0;
            $aRoute->save();
        }
    }
}
