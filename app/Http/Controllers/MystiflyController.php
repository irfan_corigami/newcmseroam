<?php

namespace App\Http\Controllers;

use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use DB;
class MystiflyController extends Controller
{

	private $client;
	const ACCOUNT_NUMBER = 'MCN000201';
	const USERNAME       = 'ATGXML';
	const PASSWORD       = 'ATG2017_XML';
	const TARGET         = 'Test';    
    const ENDPOINT_URL   = 'http://onepointdemo.myfarebox.com/V2/OnePoint.svc?singleWsdl';

	public function __construct(){	
		$this->client = new SoapClient(	self::ENDPOINT_URL, 
										array(	'trace'      => 1, 
												'cache_wsdl' => WSDL_CACHE_NONE
												)
										);	
		}

	// FUNCTIONS TO WORK ON
	private function airMessageQueue(){}
	private function airRemoveMessage(){}


	public function create_session_id(){
		$session_id = $this->createSession(	self::ACCOUNT_NUMBER, 
											self::USERNAME, 
											self::PASSWORD, 
											self::TARGET
											);
		MystiflySession::where(['id' => 1])->update(['session_id' => $session_id]);
		// echo json_encode( $session_id );die;
		return $session_id;
	}

	// --------------------------------------- CREATE SESSION --------------------------------------

	public function get_session_id(){
		$data = MystiflySession::where([ 'id' => 1 ])->first();
		if( $this->sessionIsExpired( $data->updated_at ) ){
			// Log::info('session is expired');			
			$session_id = $this->create_session_id();
		}else{
			if( $data->session_id != NULL ){
				// Log::info('session is not expired');
				$session_id = $data->session_id;
			}else{
				// Log::info('session is expired');	
				$session_id = $this->create_session_id();
			}
		}
		return $session_id;
	}

	public function create_session(){
		$session_id = $this->createSession(	self::ACCOUNT_NUMBER, 
											self::USERNAME, 
											self::PASSWORD, 
											self::TARGET
											);
		MystiflySession::truncate();
		$values = array(
			'account_number' => self::ACCOUNT_NUMBER,
			'username'       => self::USERNAME,
			'password'       => self::PASSWORD,
			'target'         => self::TARGET,
			'endpoint_url'   => self::ENDPOINT_URL,
			'session_id'     => $session_id 
			);
		return MystiflySession::create($values);
	}

	/**
	 * Function name: createSession
	 * This function is used to generate a session_id. 
	 * If successful, it will store the SessionId to the the session variable "session_id"
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $AccountNumber
	 * @param string $UserName
	 * @param string $Password
	 * @param enum   $Target
	 *		choices : "Test", "Production"
	 * @return string || NULL
	 */
	private function createSession($AccountNumber, $UserName, $Password, $Target){
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->AccountNumber = $AccountNumber;
		$params->rq->UserName      = $UserName;
		$params->rq->Password      = $Password;
		$params->rq->Target        = $Target;
		$this->logRequest('CreateSession', $params);
		try {
			$response              = $this->client->CreateSession( $params );
			$res                   = get_object_vars($response);
			$this->logResponse('CreateSession', $response);
			$create_session_result = get_object_vars($res['CreateSessionResult']);
			$session_id            = $create_session_result['SessionId'];
			return $session_id;
		} catch (Exception $e) {
			// return 'An errors has occured while calling the mystifly api create session';
			Log::error('Caught exception @TestController->createSession() : '. $e->getMessage() );
		}
	}

	/**
	 * Function name: getSessionExpiryTime
	 * This function will add 19 mins to the current datetime and return it 
	 * The SessionId returned by the MystiflyAPI is only good for 20 mins
	 * @return dateTime (YYYY-MM-DDTHH:MM:SS) 
	 */
	private function getSessionExpiryTime($dt){
		$datetime = new DateTime($dt, new DateTimeZone(Config::get('app.timezone')));
		$datetime->add(new DateInterval('PT19M'));
		return $datetime; 
	}

	/**
	 * Function name: isSessionExpired
	 * This function will check if session is expired
	 * @return bool
	 */	
	private function sessionIsExpired($updated_at){
		$now = new DateTime(NULL, new DateTimeZone(Config::get('app.timezone')));
		if( $this->getSessionExpiryTime( $updated_at ) < $now ){
			return TRUE;
		}
		return FALSE;
	}

	//------------------------------------ AIRLOWFARESEARCH ----------------------------------------

	public function airLowFareSearchController(){
		$success = TRUE;
		$data    = array();
		$error   = array();
		$result = $this->airLowFareSearch(
				$this->get_session_id(),								//SessionId
				Input::get('PricingSourceType', 'All'),    				//PricingSourceType
				$this->convertToBoolean(							
					Input::get('IsRefundable', 0)					//IsRefundable
					),						
				$this->passengerTypeQuantities(  					//PassengerTypeQuantities
					$this->passengerTypeQuantity(					//PassengerTypeQuantity
						Input::get('Code'),          				//Code
						Input::get('Quantity') 					//Quantity
						)
					),	
				Input::get('RequestOptions', 'Fifty'),     			//RequestOptions
				$this->convertToBoolean(
					Input::get('IsResidentFare', 0)					//IsResidentFare
					),				
				self::TARGET,												//Target
				$this->convertToBoolean(
					Input::get('NearByAirports', 0)					//NearByAirports
					),				
				$this->originDestinationInformations( 				//originDestinationInformations
					$this->originDestinationInformation( 			//originDestinationInformation
						$this->departureDateTime(					//DepartureDateTime
							Input::get('DepartureDate'),
							Input::get('DepartureTime', '00:00:00')
							),
						Input::get('DestinationLocationCode'),		//DestinationLocationCode
						Input::get('OriginLocationCode')			//OriginLocationCode
						)
					), 
				$this->travelPreferences(							//travelPreferences
					Input::get('AirTripType', 'OneWay'),  					//AirTripType
					Input::get('CabinPreference', 'Y'),					//CabinPreference
					Input::get('MaxStopsQuantity', 'Direct'),					//MaxStopsQuantity
					$this->cabinClassPreference(				//cabinClassPreference
						Input::get('CabinType', NULL), 					//CabinType
						Input::get('PreferenceLevel', NULL)  				//PreferenceLevel
						),
					Input::get('VendorPreferenceCodes', NULL) 			//VendorPreferenceCodes
					)
				);

		$origin_airport_temp      = CityIata::where(['iata_code' => Input::get('OriginLocationCode')])->first();
		$destination_airport_tmp  = CityIata::where(['iata_code' => Input::get('DestinationLocationCode')])->first();
		$origin_airport_info      = !empty($origin_airport_temp) ? $origin_airport_temp->toArray() : [];
		$destination_airport_info = !empty($destination_airport_tmp) ? $destination_airport_tmp->toArray() : [];

		$file_path = storage_path()."/mystifly_logs/mystifly.txt";
		File::put($file_path,json_encode($result));

 		$result                   = json_decode( json_encode($result), TRUE);

		$result                   = $result['AirLowFareSearchResult'];
		$result                   = array_merge($result, [
			'origin_airport_info' => $origin_airport_info,
			'destination_airport_info' => $destination_airport_info
			]);

		if( $result['Success'] === TRUE )
		{
			if( count( $result['PricedItineraries']) >= 1 )
			{
				$priced_itineraries = $result['PricedItineraries']['PricedItinerary'];
				foreach( $priced_itineraries as $pr_it_key => $priced_itinerary )
				{
					if( isset( $priced_itinerary['OriginDestinationOptions'] ) )
					{
						$flight_segments = $priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];	
						if(! isset($flight_segments['FlightSegment']['ArrivalAirportLocationCode'])  )
						{
							foreach($flight_segments['FlightSegment'] as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data           = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data         = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();
								$marketing_airline      = Airline::where([
										'airline_code' => $marketing_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $marketing_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => ($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => ($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								$operating_airline = Airline::where([
										'airline_code' => $operating_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $operating_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segments['FlightSegment'][$fl_se_key] = array_merge( $flight_segments['FlightSegment'][$fl_se_key], $flight_segment );		

								if( $operating_airline )
								{
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}							
								$flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}

						}
						else
						{

							foreach($flight_segments as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data      = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data    = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$marketing_airline = Airline::where([
											'airline_code' => $marketing_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $marketing_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => !empty($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => !empty($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => !empty($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$operating_airline = Airline::where([
											'airline_code' => $operating_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $operating_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segments[$fl_se_key] = array_merge( $flight_segments[$fl_se_key], $flight_segment );
								if( !empty($operating_airline) ){
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}		
								$flight_segments[$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments[$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}
						}
						$priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'] = $flight_segments;
						$priced_itineraries[ $pr_it_key ] = $priced_itinerary;
						$validating_airline_code          = $priced_itinerary['ValidatingAirlineCode'];
						$airline                          = Airline::where(['airline_code' => $validating_airline_code])->first();
						if( !empty($airline) ){
							$validating_airline_name        = array('ValidatingAirlineName' => $airline->airline_name);
							$priced_itineraries[$pr_it_key] = array_merge($priced_itineraries[$pr_it_key], $validating_airline_name );
						}	

					}
					else
					{
						$success = FALSE;
						foreach( $result['Errors'] as $k => $v){
							array_push($error, $v);
						}	
					}	
				}
				$result['PricedItineraries']['PricedItinerary'] = $priced_itineraries;
				$data = $result;

			}else{
				$success = FALSE;
				foreach( $result['Errors'] as $k => $v){
					array_push($error, $v);
				}	
			}
		}else{
			$success = FALSE;
			foreach( (array)$result['Errors'] as $k => $v){
				array_push($error, $v);
			}
		}

		// if( (int)$result['AirLowFareSearchResult']->Success === 1 ){
		// 	if( count( (array)$result['AirLowFareSearchResult']->PricedItineraries ) >= 1 ){
		// 		$data = (array)$result['AirLowFareSearchResult'];
		// 	}else{
		// 		$success = FALSE;
		// 		foreach((array)$result['AirLowFareSearchResult']->Errors as $k => $v){
		// 			array_push($error, $v);
		// 		}	
		// 	}
		// }else{
		// 	$success = FALSE;
		// 	return (array)$result['AirLowFareSearchResult']->Success;
		// 	foreach((array)$result['AirLowFareSearchResult']->Errors as $k => $v){
		// 		array_push($error, $v);
		// 	}
		// }
		Log::error( '----------------in------------------------------' );
                    Log::error( [
                       'headers' => 'Mystiflychintan',
                       'error' => $error
                   ] );
		return response_format($success, $data, $error);		
	}
	public function airLowFareSearchController1(){

		
		Input::merge(['Quantity' => array(1)]);
		Input::merge(['Code' => array('ADT')]);
		Input::merge(['DepartureDate' => '2018-05-31']);
		Input::merge(['DestinationLocationCode' => 'MOW']);
		Input::merge(['OriginLocationCode' => 'AMS']);
 
		$success = TRUE;
		$data    = array();
		$error   = array();
		$result = $this->airLowFareSearch(
				$this->get_session_id(),								//SessionId
				Input::get('PricingSourceType', 'All'),    				//PricingSourceType
				$this->convertToBoolean(							
					Input::get('IsRefundable', 0)					//IsRefundable
					),						
				$this->passengerTypeQuantities(  					//PassengerTypeQuantities
					$this->passengerTypeQuantity(					//PassengerTypeQuantity
						Input::get('Code'),          				//Code
						Input::get('Quantity') 					//Quantity
						)
					),	
				Input::get('RequestOptions', 'Fifty'),     			//RequestOptions
				$this->convertToBoolean(
					Input::get('IsResidentFare', 0)					//IsResidentFare
					),				
				self::TARGET,												//Target
				$this->convertToBoolean(
					Input::get('NearByAirports', 0)					//NearByAirports
					),				
				$this->originDestinationInformations( 				//originDestinationInformations
					$this->originDestinationInformation( 			//originDestinationInformation
						$this->departureDateTime(					//DepartureDateTime
							Input::get('DepartureDate'),
							Input::get('DepartureTime', '00:00:00')
							),
						Input::get('DestinationLocationCode'),		//DestinationLocationCode
						Input::get('OriginLocationCode')			//OriginLocationCode
						)
					), 
				$this->travelPreferences(							//travelPreferences
					Input::get('AirTripType', 'OneWay'),  					//AirTripType
					Input::get('CabinPreference', 'Y'),					//CabinPreference
					Input::get('MaxStopsQuantity', 'Direct'),					//MaxStopsQuantity
					$this->cabinClassPreference(				//cabinClassPreference
						Input::get('CabinType', NULL), 					//CabinType
						Input::get('PreferenceLevel', NULL)  				//PreferenceLevel
						),
					Input::get('VendorPreferenceCodes', NULL) 			//VendorPreferenceCodes
					)
				);

		$origin_airport_temp      = CityIata::where(['iata_code' => Input::get('OriginLocationCode')])->first();
		$destination_airport_tmp  = CityIata::where(['iata_code' => Input::get('DestinationLocationCode')])->first();
		$origin_airport_info      = !empty($origin_airport_temp) ? $origin_airport_temp->toArray() : [];
		$destination_airport_info = !empty($destination_airport_tmp) ? $destination_airport_tmp->toArray() : [];

		$file_path = storage_path()."/mystifly_logs/mystifly.txt";
		File::put($file_path,json_encode($result));
		

 		$result                   = json_decode( json_encode($result), TRUE);

		$result                   = $result['AirLowFareSearchResult'];

		$result                   = array_merge($result, [
			'origin_airport_info' => $origin_airport_info,
			'destination_airport_info' => $destination_airport_info
			]);
		if(empty($result['Success'])){
			$success = FALSE;
			foreach( (array)$result['Errors'] as $k => $v){
				array_push($error, $v);
			}
			return response_format($success, $data, $error);
			
		}
		echo "111<pre>";print_r($result);exit();
		if( $result['Success'] === TRUE )
		{
			if( count( $result['PricedItineraries']) >= 1 )
			{
				$priced_itineraries = $result['PricedItineraries']['PricedItinerary'];
				foreach( $priced_itineraries as $pr_it_key => $priced_itinerary )
				{
					if( isset( $priced_itinerary['OriginDestinationOptions'] ) )
					{
						$flight_segments = $priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];	
						if(! isset($flight_segments['FlightSegment']['ArrivalAirportLocationCode'])  )
						{
							foreach($flight_segments['FlightSegment'] as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data           = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data         = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();
								$marketing_airline      = Airline::where([
										'airline_code' => $marketing_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $marketing_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => ($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => ($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								$operating_airline = Airline::where([
										'airline_code' => $operating_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $operating_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segments['FlightSegment'][$fl_se_key] = array_merge( $flight_segments['FlightSegment'][$fl_se_key], $flight_segment );		

								if( $operating_airline )
								{
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}							
								$flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}

						}
						else
						{

							foreach($flight_segments as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data      = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data    = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$marketing_airline = Airline::where([
											'airline_code' => $marketing_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $marketing_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => !empty($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => !empty($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => !empty($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								if( !empty($arrival_data->country_code) && !empty($departure_data->country_code) ){
									$operating_airline = Airline::where([
											'airline_code' => $operating_airline_code,
											'country_code' => $arrival_data->country_code
										])->orWhere([
											'airline_code' => $operating_airline_code,
											'country_code' => $departure_data->country_code
										])->first();
								}

								$flight_segments[$fl_se_key] = array_merge( $flight_segments[$fl_se_key], $flight_segment );
								if( !empty($operating_airline) ){
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}		
								$flight_segments[$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments[$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}
						}
						$priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'] = $flight_segments;
						$priced_itineraries[ $pr_it_key ] = $priced_itinerary;
						$validating_airline_code          = $priced_itinerary['ValidatingAirlineCode'];
						$airline                          = Airline::where(['airline_code' => $validating_airline_code])->first();
						if( !empty($airline) ){
							$validating_airline_name        = array('ValidatingAirlineName' => $airline->airline_name);
							$priced_itineraries[$pr_it_key] = array_merge($priced_itineraries[$pr_it_key], $validating_airline_name );
						}	

					}
					else
					{
						$success = FALSE;
						foreach( $result['Errors'] as $k => $v){
							array_push($error, $v);
						}	
					}	
				}
				$result['PricedItineraries']['PricedItinerary'] = $priced_itineraries;
				$data = $result;

			}else{
				$success = FALSE;
				foreach( $result['Errors'] as $k => $v){
					array_push($error, $v);
				}	
			}
		}else{
			$success = FALSE;
			foreach( (array)$result['Errors'] as $k => $v){
				array_push($error, $v);
			}
		}

		// if( (int)$result['AirLowFareSearchResult']->Success === 1 ){
		// 	if( count( (array)$result['AirLowFareSearchResult']->PricedItineraries ) >= 1 ){
		// 		$data = (array)$result['AirLowFareSearchResult'];
		// 	}else{
		// 		$success = FALSE;
		// 		foreach((array)$result['AirLowFareSearchResult']->Errors as $k => $v){
		// 			array_push($error, $v);
		// 		}	
		// 	}
		// }else{
		// 	$success = FALSE;
		// 	return (array)$result['AirLowFareSearchResult']->Success;
		// 	foreach((array)$result['AirLowFareSearchResult']->Errors as $k => $v){
		// 		array_push($error, $v);
		// 	}
		// }
		Log::error( '----------------in------------------------------' );
                    Log::error( [
                       'headers' => 'Mystifly',
                       'error' => $error
                   ] );
		return response_format($success, $data, $error);		
	}



	/**
	 * Function name: airLowFareSearch
	 * This function is used to search for low fare flights. 
	 * If successful, it will store the SessionId to the the session variable "session_id"
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $SessionId 
	 * @param enum   $PricingSourceType
	 *		choices: "Public", "Private", "All"	  
	 * @param bool   $IsRefundable	 (use FALSE for test "Target")
	 * @param array  $PassengerTypeQuantities ( use passengerTypeQuantities() )
	 * @param enum   $RequestOptions
	 *		choices: "Fifty", "Hundred", "	"
	 * @param bool   $IsResidentFare (not required)
	 * @param bool   $NearByAirports (not required)	 
	 * @param enum   $Target
	 *		choices: "Test", "Production"	 	 
	 * @param array  $OriginDestinationInformations ( use originDestinationInformations() )
	 * @param array  $TravelPreferences ( use travelPreferences() )
	 * @return void
	 */
	private function airLowFareSearch(
		$SessionId, 
		$PricingSourceType, 
		$IsRefundable, 
		$PassengerTypeQuantities, 
		$RequestOptions, 
		$IsResidentFare, 
		$Target, 
		$NearByAirports, 
		$OriginDestinationInformations, 
		$TravelPreferences 
	){	
		$ptq_max_count = 2; // PassengerTypeQuantity maximum times can be nested in PassengerTypeQuantities
		$ptq_count     = 0;
		$params = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId                     = $SessionId;	
		$params->rq->OriginDestinationInformations = $OriginDestinationInformations;
		$params->rq->TravelPreferences             = $TravelPreferences;
		$params->rq->PricingSourceType             = $PricingSourceType;		
		$params->rq->IsRefundable                  = $IsRefundable;
		$params->rq->PassengerTypeQuantities       = array();
		foreach($PassengerTypeQuantities as $key => $value)
		{
			if($ptq_count < $ptq_max_count)
			{
				$params->rq->PassengerTypeQuantities = array_merge( $params->rq->PassengerTypeQuantities, array($key => $value) );
				$ptq_count++;
			}
		}
		$params->rq->RequestOptions                = $RequestOptions;
		$params->rq->NearByAirports                = $NearByAirports;
		$params->rq->IsResidentFare                = $IsResidentFare;
		$params->rq->Target                        = $Target;
		//echo json_encode($params);
		// $this->logRequest('AirLowFareSearch', $params);
		$file_path = storage_path()."/mystifly_logs/mystifly_req.txt";
				File::put($file_path,json_encode(json_encode($params)));
		$send_success = TRUE;
		// while( $send_success )
		// {
			try{

				$response = $this->client->AirLowFareSearch( $params );
				$send_success = FALSE;
			}catch(Exception $e){
				//Log::error('An error has occured during a guzzle call on CacheController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
				Log::error('ERROR MESSAGE test chintan:'.$e->getMessage() );
			}
		//}
		
		// $this->logResponse('AirLowFareSearch', $response);
		return (array)$response;
	}

	/**
	 * Function name: originDestinationInformations
	 * This function will return an OriginDestinationInformations array
	 * which will be used in the airLowFareSearch() function
 	 * @param array $OriginDestinationInformation
	 * @return array
	 */
	private function originDestinationInformations($OriginDestinationInformation){
		$data = array();
		$data['OriginDestinationInformation'] = $OriginDestinationInformation;
		return $data;
	}

	/**
	 * Function name: originDestinationInformation
	 * This function will be used in the originDestinationInformations()
	 * @param dateTime $DepartureDateTime (YYYY-MM-DDTHH:MM:SS) 
	 * @param string   $DestinationLocationCode (Airports By IATA Airport Code) e.g. "AUS" = Austin Bergstrom International, United States
	 * @param string   $OriginLocationCode (Airports By IATA Airport Code) e.g. "LHR" = London Heathrow, United Kingdom
	 * @return array
	 */
	private function originDestinationInformation(
		$DepartureDateTime, 
		$DestinationLocationCode, 
		$OriginLocationCode)
	{
		$data   = array();
		$values = array();
		if(count($DepartureDateTime) == 1){
			$data['DepartureDateTime']       = $DepartureDateTime;
			$data['DestinationLocationCode'] = $DestinationLocationCode;
			$data['OriginLocationCode']      = $OriginLocationCode;
			}
		if(count($DepartureDateTime) > 1){
			foreach($DepartureDateTime as $key => $value){
				$values['DepartureDateTime']       = $value;
				$values['DestinationLocationCode'] = $DestinationLocationCode[$key];
				$values['OriginLocationCode']      = $OriginLocationCode[$key];
				array_push($data, $values);
				}
			}
		return $data;
	}

	/**
	 * Function name: passengerTypeQuantities
	 * This function will be used in the airLowFareSearch() function
 	 * @param array $PassengerTypeQuantity ( use passengerQuantity())
	 * @return array
	 */
	private function passengerTypeQuantities($PassengerTypeQuantity){
		$data = array();
		$data['PassengerTypeQuantity'] = array();
		if($PassengerTypeQuantity){
			foreach($PassengerTypeQuantity as $key => $value){
				$data['PassengerTypeQuantity'] = array_merge($data['PassengerTypeQuantity'], array($key => $value));
			}
		}
		return $data;
	}	

	/**
	 * Function name: passengerTypeQuantity
	 * which will be used in the airLowFareSearch() function
 	 * @param int   $Quantity
	 * @param enum  $Code
	 *		choices: "ADT", "CHD", "INF"
	 * @return array
	 */
	private function passengerTypeQuantity($Code, $Quantity){
		$data = array();
		foreach($Code as $key => $code){
			$data['Code'] = $code;
			$data['Quantity'] = $Quantity[$key];
		}			
		return $data;
	}	

	/**
	 * Function name: TravelPreferences
	 * This function will return a TravelPreferences array
	 * which will be used in the airLowFareSearch() function
	 * @param enum $AirTripType	
	 *		choices: "OneWay", "Return", "Circle", "OpenJaw", "Other"
	 * @param enum $CabinPreference 
	 *		choices: Y-Economy, C-Business, F-First
	 * @param enum $MaxStopsQuantity
	 *		choices: "Direct", "OneStop", "All"
	 * @param array $Preferences ( use cabinClassPreference() )
	 * @param enum  $VendorPreferenceCodes
	 *		choices: NULL, string[]
	 * @return array
	 */
	private function travelPreferences(
		$AirTripType, 
		$CabinPreference, 
		$MaxStopsQuantity,
		$Preferences, 
		$VendorPreferenceCodes)
	{
		$data = array();
		$data['AirTripType']      = $AirTripType;
		$data['CabinPreference']  = $CabinPreference;
		$data['MaxStopsQuantity'] = $MaxStopsQuantity;		
		$data['Preferences']      = $Preferences;
		return $data;
	}

	/**
	 * Function name: cabinClassPreference
	 * This function will return an Array
	 * which will be used in the travelPreferences function
	 * @param enum $CabinType
	 *		choices: "Y", "S", "C", "J", "F", "P"
	 * @param enum $PreferenceLevel
	 *		choices: "Restricted", "Preferred"
	 * @return array
	 */
	private function cabinClassPreference($CabinType, $PreferenceLevel){
		$data = array();
		$data['CabinType']        = $CabinType;
		$data['PreferenceLevel']  = $PreferenceLevel;
		return $data;
	}	

	public function departureDateTime($date, $time){
		$t = new DateTime('2000-01-01');
		return $date.'T'.$t->format('H:i:s');
	}

	//---------------------------------------- FareRules1_1 ------------------------------------------

	public function fareRules1_1Controller(){
		$success = TRUE;
		$data    = array();
		$error   = array();
		try
		{
			$result = $this->fareRules1_1(
				$this->get_session_id(),
				Input::get('FareSourceCode'), 		//FareSourceCode
				self::TARGET
				);
			if( (int)$result['FareRules1_1Result']->Success === 1 ){
				$data = ['BaggageInfos' => (array)$result['FareRules1_1Result']->BaggageInfos,
						'FareRules' => (array)$result['FareRules1_1Result']->FareRules
						];
			}else{
				$success = FALSE;
				foreach( (array)$result['FareRules1_1Result']->Errors as $k => $v ){
					array_push($error, $v);
				}	
			}
		}
		catch(Exception $e)
		{
			$success = FALSE;
			$error = ['message' => 'An error has occured.'];
		}
		return response_format($success, $data, $error);
	}

	/**
	 * Function name: fareRules1_1
	 * This function is used to request the rules and conditions of a particular fare result per
	 *	unique fare-basis code 
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $SessionId 
	 * @param string $FareSourceCode ( provided in the response of airLowFareSearch() )
	 * @param enum   $Target
 	 *		choices: "Test", "Production"	 	 
	 * @return void
	 */
	private function fareRules1_1($SessionId, $FareSourceCode, $Target){
		$params     = new stdClass();
		$params->rq = new stdClass();
		$params->rq->SessionId      = $SessionId;
		$params->rq->FareSourceCode = $FareSourceCode;
		$params->rq->Target         = $Target;
		$response                   = $this->client->FareRules1_1( $params );
		$res                        = get_object_vars($response);
		// $this->logRequest('FareRules1_1', $params);
		// $this->logResponse('FareRules1_1', $response);
		return $res;
		//echo response()->json(get_object_vars($response));
	}

	//---------------------------------------- AddBookingNotes ------------------------------------------

	public function addBookingNotesController(){
		$this->addBookingNotes(
			$this->get_session_id(),
			Input::get('UniqueID'),	//FareSourceCode
			Input::get('Notes'),			//Notes
			self::TARGET
			);
	}

	/**
	 * Function name: addBookingNotes
	 * This function is used to add remarks / comments / special service request to the
	 * bookings made which in-turn will reflect in the TripDetail response.
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $SessionId 
	 * @param string $UniqueID 
	 * @param string $Notes
	 * @param enum   $Target
 	 *		choices: "Test", "Production"	 	 
	 * @return void
	 */
	private function addBookingNotes(
		$SessionId,
		$UniqueID,
		$Notes,
		$Target)
	{
		$params                = new stdClass();
		$params->rq            = new stdClass();
		$params->rq->SessionId = $SessionId;
		$params->rq->UniqueID  = $UniqueID;
		$params->rq->Notes     = $Notes;
		$params->rq->Target    = $Target;
		$response              = $this->client->AddBookingNotes( $params );
		$res                   = get_object_vars($response);
		// $this->logRequest('AddBookingNotes', $params);
		// $this->logResponse('AddBookingNotes', $response);
		echo json_encode($res);	
		//echo response()->json(get_object_vars($response));	
	}

	//---------------------------------------- AirRevalidate ------------------------------------------

	public function airRevalidateController(){
		$success = FALSE;
		$data    = [];
		$error   = [];
		try {
			$result = $this->airRevalidate(
				$this->get_session_id(),
				Input::get('FareSourceCode'),	//FareSourceCode
				self::TARGET
			);
			if( $result->Success && $result->IsValid ) {
				$success = TRUE;
				$data = $result->PricedItineraries;
			} else {
				foreach( $result->Errors as $k => $v ) {
					array_push( $error, $v );
				}	
			}
		}
		catch( Exception $e ) {
			$error = ['An error has occured during mystifly airRevalidateCall'];
		}
		return response_format( $success, $data, $error );
	}

	/**
	 * Function name: airRevalidate
	 * This function is used to determine the availability of the selected fare by re-pricing. If
	 * the selected fare is not available, this service will return the next available fare on the selected
	 * flights if any.
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $SessionId 
	 * @param string $FareSourceCode 
	 * @param enum   $Target
 	 *		choices: "Test", "Production"	 	 
	 * @return void
	 */
	private function airRevalidate(
		$SessionId,
		$FareSourceCode,
		$Target)
	{
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $SessionId;
		$params->rq->FareSourceCode  = $FareSourceCode;
		$params->rq->Target          = $Target;
		$response              = $this->client->AirRevalidate( $params );
		// $this->logRequest('AirRevalidate', $params);
		// $this->logResponse('AirRevalidate', $response);		
		return $response->AirRevalidateResult;		
		//echo response()->json(get_object_vars($response));
	}

	//---------------------------------------- BookFlight ------------------------------------------


	public function bookFlightController(){
		$this->bookFlight(
			$this->get_session_id(), 							//SessionID
			Input::get('FareSourceCode'), 								//FareSourceCode
			$this->travelerInfo(										//TravelerInfo
				$this->airTravelers(									//AirTravelers
					$this->airTraveler(									//AirTraveler
						$this->setDateTimeWithDateOnly(
							Input::get('DateOfBirth')					//DateOfBirth
							),						
						$this->extraServices1_1( 						//ExtraServices1_1
							$this->services1_1(							//Services
								Input::get('ExtraServiceId', NULL)		//ExtraServiceId
								)	
							),
						Input::get('Gender'),						//Gender
						$this->passengerName(						//PassengerName
							Input::get('PassengerTitle'),			//PassengerTitle
							Input::get('PassengerFirstName'),		//PassengerFirstName
							Input::get('PassengerLastName')			//PassengerLastName
							),
						Input::get('PassengerType'),			//PassengerType
						$this->passport(						//Passport
							Input::get('Country'), 				//Country
							$this->setDateTimeWithDateOnly(  	
								Input::get('ExpiryDate')		//ExpiryDate
								), 			
							Input::get('PassportNumber')		//PassportNumber
							),
						$this->specialServiceRequest(				//SpecialServiceRequest
							Input::get('MealPreference', 'Any'), 	//MealPreference
							Input::get('SeatPreference', 'Any')		//SeatPreference
							)
						)
					),
				Input::get('AreaCode'),		//AreaCode
				Input::get('Country'),			//CountryCode
				Input::get('PhoneNumber'),		//PhoneNumber
				Input::get('Email')			//Email
				),
			self::TARGET								//Target
			);
	}

	public function test(){
		// $str = '{"FareSourceCode":"ODAxMDM2JlRGJlRGJmQ1MDc2ZTY4LTBmNDUtNDcxOC1iNzZmLTIyMTI1MDQ1ODI5NCZURiY=","passenger_contact_no":"9909811223","passenger_email":"sahista@corigami.com","passenger_title":["MR"],"passenger_first_name":["Irfan"],"passenger_last_name":["Bagwala"],"passenger_gender":["M"],"passenger_dob":["1990-07-07T00:00:00"],"passenger_country":["IN"],"passenger_type":["ADT"],"passenger_passport_expiry_date":["2018-07-07T00:00:00"],"passenger_passport_number":["HG412310"],"passenger_meal_preference":["Any"],"passenger_seat_preference":["Any"]}';
		// $data = (array)json_decode($str);
		// $DateOfBirth = $data['passenger_dob'];
		// $PhoneNumber = $data['passenger_contact_no'];
		// $Email = $data['passenger_email'];
		// $FareSourceCode = $data['FareSourceCode'];
		// if(count($DateOfBirth) == 1){

		// 	$DateOfBirth = $data['passenger_dob'][0];
		// 	$ExtraServiceId='';
		// 	$Gender = $data['passenger_gender'][0];
		// 	$PassengerTitle = $data['passenger_title'][0];
		// 	$PassengerFirstName = $data['passenger_first_name'][0];
		// 	$PassengerLastName =  $data['passenger_title'][0];
		// 	$PassengerType      = $data['passenger_type'][0];
		// 	$Country            = $data['passenger_country'][0];
		// 	$ExpiryDate         = $data['passenger_passport_expiry_date'][0];
		// 	$PassportNumber     = $data['passenger_passport_number'][0];
		// 	$MealPreference     = $data['passenger_meal_preference'][0];
		// 	$SeatPreference     = $data['passenger_seat_preference'][0];
		// }else{

		// 	$DateOfBirth = $data['passenger_dob'];
		// 	$ExtraServiceId= array();
		// 	$Gender = $data['passenger_gender'];
		// 	$PassengerTitle = $data['passenger_title'];
		// 	$PassengerFirstName = $data['passenger_first_name'];
		// 	$PassengerLastName =  $data['passenger_title'];
		// 	$PassengerType      = $data['passenger_type'];
		// 	$Country            = $data['passenger_country'];
		// 	$ExpiryDate         = $data['passenger_passport_expiry_date'];
		// 	$PassportNumber     = $data['passenger_passport_number'];
		// 	$MealPreference     = $data['passenger_meal_preference'];
		// 	$SeatPreference     = $data['passenger_seat_preference'];
		// }
		

		$DateOfBirth = Input::get('passenger_dob');
		$PhoneNumber = Input::get('passenger_contact_no');
		$Email = Input::get('passenger_email');
		$FareSourceCode = Input::get('FareSourceCode');
		if(count($DateOfBirth) == 1){

			$DateOfBirth = Input::get('passenger_dob')[0];
			$ExtraServiceId='';
			$Gender = Input::get('passenger_gender')[0];
			$PassengerTitle = Input::get('passenger_title')[0];
			$PassengerFirstName = Input::get('passenger_first_name')[0];
			$PassengerLastName = Input::get('passenger_title')[0];
			$PassengerType      = Input::get('passenger_type')[0];
			$Country            = Input::get('passenger_country')[0];
			$ExpiryDate         = Input::get('passenger_passport_expiry_date')[0];
			$PassportNumber     = Input::get('passenger_passport_number')[0];
			$MealPreference     = Input::get('passenger_meal_preference')[0];
			$SeatPreference     = Input::get('passenger_seat_preference')[0];
		}else{

			$DateOfBirth        = Input::get('passenger_dob');
			$ExtraServiceId     = array();
			$Gender     = Input::get('passenger_gender');
			$PassengerTitle = Input::get('passenger_title');
			$PassengerFirstName = Input::get('passenger_first_name');
			$PassengerLastName = Input::get('passenger_last_name');
			$PassengerType      = Input::get('passenger_type');
			$Country            = Input::get('passenger_country');
			$ExpiryDate         = Input::get('passenger_passport_expiry_date');
			$PassportNumber     = Input::get('passenger_passport_number');
			$MealPreference     = Input::get('passenger_meal_preference');
			$SeatPreference     = Input::get('passenger_seat_preference');
		}
		
		
		$res = $this->bookFlight(
			$this->get_session_id(),//SessionID
			$FareSourceCode,//FareSourceCode
			$this->travelerInfo(						//TravelerInfo
				$this->airTravelers(					//AirTravelers
					$this->airTraveler(					//AirTraveler
						$DateOfBirth,			//DateOfBirth
						$this->extraServices1_1( 		//ExtraServices1_1
							$ExtraServiceId				//ExtraServiceId
							),
						$Gender,							//Gender
						$this->passengerName(			//PassengerName
							$PassengerTitle,						//PassengerTitle
							$PassengerFirstName,						//PassengerFirstName
							$PassengerLastName					//PassengerLastName
							),
						$PassengerType,							//PassengerType
						$this->passport(				//Passport
							$Country, 						//Country
							$ExpiryDate, 		//ExpiryDate
							$PassportNumber					//PassportNumber
							),
						$this->specialServiceRequest(	//SpecialServiceRequest
							$MealPreference, 					//MealPreference
							$SeatPreference							//SeatPreference
							)
						)
					),
				758,						//AreaCode
				0044,					//CountryCode
				$PhoneNumber,		//PhoneNumber
				$Email			//Email
				),
			self::TARGET
			);
		return $res;
		exit;
	}
	public function test1(){

		$DateOfBirth        = array();
		$ExtraServiceId     = array();
		$Gender             = array();
		$PassengerTitle     = array();
		$PassengerFirstName = array();
		$PassengerLastName  = array();
		$PassengerType      = array();
		$Country            = array();
		$ExpiryDate         = array();
		$PassportNumber     = array();
		$MealPreference     = array();
		$SeatPreference     = array();
		
		$DateOfBirth        = array_merge($DateOfBirth, array("1990-07-07T00:00:00", "2015-12-07T00:00:00"));
		$ExtraServiceId     = array();
		//$ExtraServiceId   = array_merge($ExtraServiceId, array( array( 1,2,3 ), array(4,5) ));
		$Gender             = array_merge($Gender, array("M", "F"));
		$PassengerTitle     = array_merge($PassengerTitle, array("MR", "INF"));
		$PassengerFirstName = array_merge($PassengerFirstName , array("Miguel", "Daisy"));
		$PassengerLastName  = array_merge($PassengerLastName , array("Sanchez", "Sanchez"));
		$PassengerType      = array_merge($PassengerType , array("ADT", "INF"));
		$Country            = array_merge($Country , array("IN", "IN"));
		$ExpiryDate         = array_merge($ExpiryDate , array("2018-07-07T00:00:00", "2018-07-07T00:00:00"));
		$PassportNumber     = array_merge($PassportNumber , array("HG412314", "HG412315"));
		$MealPreference     = array_merge($MealPreference , array("Any", "BBML"));
		$SeatPreference     = array_merge($SeatPreference , array("Any", "Any"));

		$res = $this->bookFlight(
			$this->get_session_id(), 			//SessionID
			"MTAwMDAyJkkxSkQmMUImY2Q2YTQyMTAtNDFiNS00YzE3LWE5MTYtMTlhMTIyYzA4YWQxJkdMNjgm", //FareSourceCode
			$this->travelerInfo(						//TravelerInfo
				$this->airTravelers(					//AirTravelers
					$this->airTraveler(					//AirTraveler
						$DateOfBirth,			//DateOfBirth
						$this->extraServices1_1( 		//ExtraServices1_1
							$ExtraServiceId				//ExtraServiceId
							),
						$Gender,							//Gender
						$this->passengerName(			//PassengerName
							$PassengerTitle,						//PassengerTitle
							$PassengerFirstName,						//PassengerFirstName
							$PassengerLastName					//PassengerLastName
							),
						$PassengerType,							//PassengerType
						$this->passport(				//Passport
							$Country, 						//Country
							$ExpiryDate, 		//ExpiryDate
							$PassportNumber					//PassportNumber
							),
						$this->specialServiceRequest(	//SpecialServiceRequest
							$MealPreference, 					//MealPreference
							$SeatPreference							//SeatPreference
							)
						)
					),
				758,						//AreaCode
				0044,					//CountryCode
				44072749,				//PhoneNumber
				"ABC@XYZ.com"			//Email
				),
			self::TARGET
			);
		echo $res;exit;
	}
 


	/**
	 * Function name: bookFlight
	 * This function is used to determine the availability of the selected fare by re-pricing. If
	 * the selected fare is not available, this service will return the next available fare on the selected
	 * flights if any.
	 * This function will also display the data returned by the Mystifly API in JSON Format
	 * @param string $SessionID 
	 * @param string $FareSourceCode 
	 * @param array  $TravelerInfo ( use travelerInfo() )
	 * @param enum   $Target
 	 *		choices: "Test", "Production"	 	 
	 * @return void
	 */
	private function bookFlight1(){
		$res = new stdClass();
		$res->BookFlightResult = new stdClass();
		$res->BookFlightResult->ClientUTCOffset =1;
		$res->BookFlightResult->Errors = new stdClass();
		$res->BookFlightResult->Errors->Error = new stdClass();
		$res->BookFlightResult->Errors->Error->Code ="ERBUK080";
		$res->BookFlightResult->Errors->Error->Message ="Pending airline PNR - Booking UnConfirmed.";
		$res->BookFlightResult->Status ="";
		$res->BookFlightResult->Success ="";
		$res->BookFlightResult->Target ="";
		$res->BookFlightResult->TktTimeLimit ="";
		$res->BookFlightResult->UniqueID ="";
		//$t_array = array('successful'=>true,'SessionId'=>$SessionId,'FareSourceCode'=>$FareSourceCode,'TravelerInfo'=>$TravelerInfo,'Target'=>$Target);
		$res->successful=true;
		$res = get_object_vars($res);
		
		$res = json_encode($res);
		return $res;
	}
	private function bookFlight(
		$SessionId,
		$FareSourceCode,
		$TravelerInfo,
		$Target)
	{
		
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $SessionId;
		$params->rq->FareSourceCode  = $FareSourceCode;
		$params->rq->TravelerInfo    = $TravelerInfo;
		$params->rq->Target          = $Target;
		
		$response              = $this->client->BookFlight( $params );
		$res                   = get_object_vars($response);
		$res['successful'] =true;
		return json_encode($res);		
	}

	/**
	 * Function name: travelerInfo
	 * This function will be used in bookFlight()
	 * @param array  AirTravelers 
	 * @param int    $AreaCode 
	 * @param int    $CountryCode 
	 * @param int    $PhoneNumber 
	 * @param string $Email 	 	 
	 * @return array
	 */
	private function travelerInfo(
		$AirTravelers,
		$AreaCode,
		$CountryCode,
		$PhoneNumber,
		$Email )
	{

		$data = array(
					'AirTravelers' => $AirTravelers,
					'AreaCode'     => $AreaCode,
					'CountryCode'  => $CountryCode,
					'Email'        => $Email,
					'PhoneNumber'  => $PhoneNumber
					);

		return $data;
	}

	/**
	 * Function name: airTravelers
	 * This function will be used in travelerInfo()
	 * @param array $AirTraveler (use airTraveler(), max of 8)
	 * @return array
	 */
	private function airTravelers($AirTraveler){
		
		$data = array();
		$data['AirTraveler'] = array();
		foreach($AirTraveler as $key => $value){
			$data['AirTraveler'] = array_merge($data['AirTraveler'], array($key => $value));
		}
		return $data;
	}

	/**
	 * Function name: airTraveler
	 * This function will be used in airTravelers()
	 * @param dateTime $DateOfBirth (YYYY-MM-DDT00:00:00)
	 * @param array    $ExtraServices1_1 ( use extraServices1_1() )
	 * @param array    $ExtraServices ( use extraServices() )
	 * @param string   $FrequentFlyerNumber 
	 * @param enum     $Gender 
	 *		choices: "M", "F"
	 * @param array    $PassengerName ( use passengerName() )	 
	 * @param enum     $PassengerType  
	 *		choices: "ADT", "CHD", "INF"
	 * @param array    $Passport ( use passport() )
 	 * @param array    $SpecialServiceRequest ( use specialServiceRequest() )
	 * @return array
	 */
	private function airTraveler(
		$DateOfBirth,
		$ExtraServices1_1,
		$Gender,
		$PassengerName,
		$PassengerType,
		$Passport,
		$SpecialServiceRequest)
	{	
		$e = array();
		$e['ExtraServices1_1'] = array();
		$data = array();
		if(count($DateOfBirth) == 1){
			$data = array(
					'DateOfBirth'           => $DateOfBirth,
					'ExtraServices1_1'      => $ExtraServices1_1,
					'Gender'                => $Gender,				
					'PassengerName'         => $PassengerName,	
					'PassengerType'         => $PassengerType,
					'Passport'              => $Passport,
					'SpecialServiceRequest' => $SpecialServiceRequest
					);	
			return $data;
			}

		foreach($DateOfBirth as $key => $value){
			$values = array(
				'DateOfBirth'           => $value,
				'Gender'                => $Gender[$key],				
				'PassengerName'         => $PassengerName[$key],	
				'PassengerType'         => $PassengerType[$key],
				'Passport'              => $Passport[$key],
				'SpecialServiceRequest' => $SpecialServiceRequest[$key]
				);	
			if($ExtraServices1_1 && count($ExtraServices1_1) > 1){
				$values = array_merge($values, array('ExtraServices1_1' => $ExtraServices1_1[$key]));
				}
			else{
				$values = array_merge($values, array('ExtraServices1_1' => array('Services' => array() ) ) );
				}
			array_push($data, $values);
			}
		return $data;
	}

	/**
	 * Function name: extraServices1_1
	 * This function will be used in airTraveler()
	 * @param array $ExtraServiceId max of 8
	 * @return array
	 */
	private function extraServices1_1($ExtraServiceId){
		$esi_max_count = 8; // maximum of 8 values for Services1_1
		$d                  = array();
		$data               = array();
		$data['Services']   = array();
		if($ExtraServiceId){
			foreach($ExtraServiceId as $key => $value){
				$values = array();
				$values['Services'] = array();
				foreach($value as $k => $v){
					array_push($values['Services'], array('ExtraServiceId' => $v));
				}
				array_push($data, $values);				
			}	
		}	
		return $data;
	}		


	/**
	 * Function name: passengerName
	 * This function will be used in airTraveler()
	 * @param string $PassengerFirstName 
	 * @param string $PassengerLastName 
	 * @param enum   $PassengerTitle
 	 *		choices: "MR", "MRS", "SIR", "LORD", "LADY", "MISS", "INF"
	 * @return array
	 */
	private function passengerName($PassengerTitle, $PassengerFirstName, $PassengerLastName){
		$data = array();
		if(count($PassengerTitle) == 1){
			$data['PassengerTitle']     = $PassengerTitle;
			$data['PassengerFirstName'] = $PassengerFirstName;
			$data['PassengerLastName']  = $PassengerLastName;
			return $data;
			}
		if(count($PassengerTitle) > 1){
			$values = array();
			foreach($PassengerTitle as $key => $value){
				$values['PassengerTitle']     = $value;
				$values['PassengerFirstName'] = $PassengerFirstName[$key];
				$values['PassengerLastName']  = $PassengerLastName[$key];
				array_push($data, $values);
				}
			return $data;
			}
	}

	/**
	 * Function name: passport
	 * This function will be used in airTraveler()
	 * @param string   $Country 
	 * @param dateTime $ExpiryDate (YYYY-MM-DDT00:00:00)
	 * @param string   $PassportNumber
	 * @return array
	 */
	private function passport($Country, $ExpiryDate, $PassportNumber){
		$data = array();
		if(count($Country) == 1){
			$data['Country']     = $Country;
			$data['ExpiryDate'] = $ExpiryDate;
			$data['PassportNumber']  = $PassportNumber;
			return $data;
			}
		if(count($Country) > 1){
			$values = array();
			foreach($Country as $key => $value){
				$values['Country']     = $value;
				$values['ExpiryDate'] = $ExpiryDate[$key];
				$values['PassportNumber']  = $PassportNumber[$key];
				array_push($data, $values);
				}
			return $data;
			}
	}

	/**
	 * Function name: specialServiceRequest
	 * This function will be used in airTraveler()
	 * @param enum $MealPreference 
  	 *		choices: "Any" "LPML", "ORML", "HFML", "PRML", VJML", "VOML", "AVML", "BBML"
 	 * @param enum $SeatPreference
 	 *		choices: "Any" "W", "A"
	 * @return array
	 */
	private function specialServiceRequest($MealPreference, $SeatPreference){
		$data = array();
		if(count($MealPreference) == 1){
			$data['MealPreference'] = $MealPreference;
			$data['SeatPreference'] = $SeatPreference;
			return $data;
			}
		if(count($MealPreference) > 1){
			$values = array();
			foreach($MealPreference as $key => $value){
				$values['MealPreference'] = $value;
				$values['SeatPreference'] = $SeatPreference[$key];
				array_push($data, $values);
				}
			return $data;
			}
	}

	//---------------------------------------- TicketOrder ------------------------------------------

	public function ticketOrderController(){
		
		$this->ticketOrder(
			$this->get_session_id(),	//SessionID
			Input::get('FareSourceCode'),		//FareSourceCode
			Input::get('UniqueID'),			//UniqueID
			self::TARGET 								//Target
			);
	}

	/**
	 * Function name: ticketOrder
	 * This function will be used to order the issuance of the Booking made from Public and Private
	 * fare types only. This request has to be made within the Ticketing Time Limit received in the
	 * BookFlight response to secure the fare.
	 * @param string $SessionID 
	 * @param string $FareSourceCode 
 	 * @param string $UniqueID (Generated from response of bookFlight() )
	 * @param enum $Target  	 
	 * @return array
	 */
	public function ticketOrder($SessionId, $FareSourceCode, $UniqueID, $Target){
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $SessionId;
		$params->rq->FareSourceCode  = $FareSourceCode;
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->Target          = $Target;
		//echo json_encode($params);
		$response              = $this->client->TicketOrder( $params );
		$res                   = get_object_vars($response);
		$this->logRequest('TicketOrder', $params);
		$this->logResponse('TicketOrder', $response);		
		echo json_encode($res);	
		//echo response()->json(get_object_vars($response));		
	}

	//---------------------------------------- TRIPDETAILS ------------------------------------------

	public function tripDetailsController()
	{	
		$this->tripDetails(
			$this->get_session_id(),   //SessionID
			Input::get('UniqueID'),				//UniqueID
			self::TARGET 								//Target
			);
	}

	/**
	 * Function name: tripDetails
	 * This function is used to request trip details for the giving booking reference number after
	 * OrderTicket.
	 * @param string $SessionID 
 	 * @param string $UniqueID (Generated from response of bookFlight() )
	 * @param enum $Target  	 
	 * @return array
	 */
	public function tripDetails($SessionId, $UniqueID, $Target){
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $SessionId;
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->Target          = $Target;
		//echo json_encode($params);
		$response              = $this->client->TripDetails( $params );
		$res                   = get_object_vars($response);
		$this->logRequest('TripDetails', $params);
		$this->logResponse('TripDetails', $response);			
		echo json_encode($res);			
		//echo response()->json(get_object_vars($response));
	}

	//---------------------------------------- CancelBooking ------------------------------------------

	public function cancelBookingController(){
		$this->tripDetails(
			$this->get_session_id(),   //SessionID
			Input::get('UniqueID'),			//UniqueID
			self::TARGET 								//Target
			);
	}

	/**
	 * Function name: cancelBooking
	 * This function is used to request OnePoint to cancel the booking identified by the
	 * UniqueID element.
	 * @param string $SessionID 
 	 * @param string $UniqueID (Generated from response of bookFlight() )
	 * @param enum $Target  	 
	 * @return array
	 */
	public function cancelBooking($SessionId, $UniqueID, $Target){
		$params                      = new stdClass();
		$params->rq                  = new stdClass();
		$params->rq->SessionId       = $SessionId;
		$params->rq->UniqueID        = $UniqueID;
		$params->rq->Target          = $Target;
		//echo json_encode($params);
		$response              = $this->client->CancelBooking( $params );
		$res                   = get_object_vars($response);
		$this->logRequest('CancelBooking', $params);
		$this->logResponse('CancelBooking', $response);			
		echo json_encode($res);			
		//echo response()->json(get_object_vars($response));
	}

	public function get_modal_data(){
		$data        = array();
		$origin      = Input::get('originCityId');
		$destination = Input::get('destinationCityId');
		$date        = Input::get('travelDate');
		$data = array_merge($data, ['origin_info' => $this->get_city_name_and_country_name_by_city_id($origin) ]);
		$data = array_merge($data, ['destination_info' => $this->get_city_name_and_country_name_by_city_id($destination) ]);		

		$iata = $this->get_iata_code_by_city_id( $origin ); //get rows from zCityIata table by origin city_id
		if(! $iata )
			return response_format( FALSE, NULL, ['message' => 'Iata code not available for city_id '.$origin] );
		$data = array_merge($data, ['origin_airports_info' => $iata]);

		$iata = $this->get_iata_code_by_city_id( $destination ); //get rows from zCityIata table by destination city_id
		if(! $iata )
			return response_format( FALSE, NULL, ['message' => 'Iata code not available for city_id '.$destination] );
		$data = array_merge($data, ['destination_airports_info' => $iata]);

		return response_format( TRUE, $data, NULL);
	}

	private function get_iata_code_by_city_id( $city_id ){
		$result = CityIata::where(['city_id' => $city_id ])->get();
		return $result->toArray();
	}

	private function get_city_name_and_country_name_by_city_id( $city_id ){
		$city    = City::where(['id' => $city_id])->pluck('name');
		$country = City::find( $city_id )->country;	
		return $city.', '.$country->name;
	}


	//---------------------------------------- Caching ------------------------------------------






	/**
	 * This function returns the current time added by the cache duration in minutes
	 * @return array
	 */
	public function cached_flight_search(){
		$success = TRUE;
		$data    = array();
		$error   = array();

		$key = $this->create_key( Input::all() );

		if(! Cache::has( $key ) ) { // if key does not exist, call mystifly api, store results to cache and return results

			$value = $this->airLowFareSearch(
				$this->get_session_id(),								//SessionId
				Input::get('PricingSourceType', 'All'),    				//PricingSourceType
				$this->convertToBoolean(							
					Input::get('IsRefundable', 0)					//IsRefundable
					),						
				$this->passengerTypeQuantities(  					//PassengerTypeQuantities
					$this->passengerTypeQuantity(					//PassengerTypeQuantity
						Input::get('Code'),          				//Code
						Input::get('Quantity') 					//Quantity
						)
					),	
				Input::get('RequestOptions', 'Fifty'),     			//RequestOptions
				$this->convertToBoolean(
					Input::get('IsResidentFare', 0)					//IsResidentFare
					),				
				self::TARGET,												//Target
				$this->convertToBoolean(
					Input::get('NearByAirports', 0)					//NearByAirports
					),				
				$this->originDestinationInformations( 				//originDestinationInformations
					$this->originDestinationInformation( 			//originDestinationInformation
						$this->departureDateTime(					//DepartureDateTime
							Input::get('DepartureDate'),
							Input::get('DepartureTime', '00:00:00')
							),
						Input::get('DestinationLocationCode'),		//DestinationLocationCode
						Input::get('OriginLocationCode')			//OriginLocationCode
						)
					), 
				$this->travelPreferences(							//travelPreferences
					Input::get('AirTripType', 'OneWay'),  					//AirTripType
					Input::get('CabinPreference', 'Y'),					//CabinPreference
					Input::get('MaxStopsQuantity', 'Direct'),					//MaxStopsQuantity
					$this->cabinClassPreference(				//cabinClassPreference
						Input::get('CabinType', NULL), 					//CabinType
						Input::get('PreferenceLevel', NULL)  				//PreferenceLevel
						),
					Input::get('VendorPreferenceCodes', NULL) 			//VendorPreferenceCodes
					)
				);

			$origin_airport_info = CityIata::where(['iata_code' => Input::get('OriginLocationCode')])->first();
			if(! $origin_airport_info )
				return response_format(FALSE, NULL, ['message' => 'Origin location iata code unknown']);
			$destination_airport_info = CityIata::where(['iata_code' => Input::get('DestinationLocationCode')])->first();
			if(! $destination_airport_info )
				return response_format(FALSE, NULL, ['message' => 'Destination location iata code unknown']);
			$value = json_encode($value);
			$value = json_decode($value, TRUE);
			$value = $value['AirLowFareSearchResult'];
			$value = array_merge($value, ['origin_airport_info' => $origin_airport_info, 'destination_airport_info' => $destination_airport_info]);
			$result = json_encode($value);
			Cache::put( $key, $result, $this->expiry_time() );

		}else{

			$result = Cache::get($key, FALSE); // if key exists get value

			if(!$result){ // if value is expired, call mystifly api, store results to cache and return results

				$value = $this->airLowFareSearch(
					$this->get_session_id(),					//SessionId
					Input::get('PricingSourceType', 'All'),    				//PricingSourceType
					$this->convertToBoolean(							
						Input::get('IsRefundable', 0)					//IsRefundable
						),						
					$this->passengerTypeQuantities(  					//PassengerTypeQuantities
						$this->passengerTypeQuantity(					//PassengerTypeQuantity
							Input::get('Code'),          				//Code
							Input::get('Quantity') 					//Quantity
							)
						),	
					Input::get('RequestOptions', 'Fifty'),     			//RequestOptions
					$this->convertToBoolean(
						Input::get('IsResidentFare', 0)					//IsResidentFare
						),				
					self::TARGET,												//Target
					$this->convertToBoolean(
						Input::get('NearByAirports', 0)					//NearByAirports
						),				
					$this->originDestinationInformations( 				//originDestinationInformations
						$this->originDestinationInformation( 			//originDestinationInformation
							$this->departureDateTime(					//DepartureDateTime
								Input::get('DepartureDate'),
								Input::get('DepartureTime', '00:00:00')
								),
							Input::get('DestinationLocationCode'),		//DestinationLocationCode
							Input::get('OriginLocationCode')			//OriginLocationCode
							)
						), 
					$this->travelPreferences(							//travelPreferences
						Input::get('AirTripType', 'OneWay'),  					//AirTripType
						Input::get('CabinPreference', 'Y'),					//CabinPreference
						Input::get('MaxStopsQuantity', 'Direct'),					//MaxStopsQuantity
						$this->cabinClassPreference(					//cabinClassPreference
							Input::get('CabinType', NULL), 					//CabinType
							Input::get('PreferenceLevel', NULL)  				//PreferenceLevel
							),
						Input::get('VendorPreferenceCodes', NULL) 			//VendorPreferenceCodes
						)
					);			
				$origin_airport_info = CityIata::where(['iata_code' => Input::get('OriginLocationCode')])->first()->toArray();
				$destination_airport_info = CityIata::where(['iata_code' => Input::get('DestinationLocationCode')])->first()->toArray();
				$value = json_encode($value);
				$value = json_decode($value, TRUE);
				$value = $value['AirLowFareSearchResult'];
				$value = array_merge($value, ['origin_airport_info' => $origin_airport_info, 'destination_airport_info' => $destination_airport_info]);
				$result = json_encode($value);

				Cache::put( $key, $result, $this->expiry_time() );
			}

		}

		$result = json_decode($result, TRUE);

		if( $result['Success'] === TRUE )
		{
			if( count( $result['PricedItineraries']) >= 1 )
			{
				$priced_itineraries = $result['PricedItineraries']['PricedItinerary'];
				foreach( $priced_itineraries as $pr_it_key => $priced_itinerary )
				{
					if( isset( $priced_itinerary['OriginDestinationOptions'] ) )
					{
						$flight_segments = $priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];	
						if(! isset($flight_segments['FlightSegment']['ArrivalAirportLocationCode'])  )
						{
							foreach($flight_segments['FlightSegment'] as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data           = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data         = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();
								$marketing_airline      = Airline::where([
										'airline_code' => $marketing_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $marketing_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segment = array_merge( $flight_segment, [
									'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => ($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => ($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								$operating_airline = Airline::where([
										'airline_code' => $operating_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $operating_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segments['FlightSegment'][$fl_se_key] = array_merge( $flight_segments['FlightSegment'][$fl_se_key], $flight_segment );		

								if( $operating_airline )
								{
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}							
								$flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments['FlightSegment'][$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}

						}
						else
						{

							foreach($flight_segments as $fl_se_key => $flight_segment)
							{
								$marketing_airline_code = $flight_segment['MarketingAirlineCode'];
								$operating_airline_code = $flight_segment['OperatingAirline']['Code'];
								$arrival_data      = CityIata::where([
										'iata_code' => $flight_segment['ArrivalAirportLocationCode']
									])->first();
								$departure_data    = CityIata::where([
										'iata_code' => $flight_segment['DepartureAirportLocationCode']
									])->first();

								$marketing_airline      = Airline::where([
										'airline_code' => $marketing_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $marketing_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segment    = array_merge( $flight_segment, [
									'MarketingAirlineName' => ($marketing_airline) ? $marketing_airline->airline_name: NULL,
									'ArrivalData'          => ($arrival_data) ? $arrival_data->airport_name.', '.$arrival_data->iata_code.' '.$arrival_data->city_name: NULL,
									'DepartureData'        => ($departure_data) ? $departure_data->airport_name.', '.$departure_data->iata_code.' '.$departure_data->city_name: NULL,
									]
								);

								$operating_airline = Airline::where([
										'airline_code' => $operating_airline_code,
										'country_code' => $arrival_data->country_code
									])->orWhere([
										'airline_code' => $operating_airline_code,
										'country_code' => $departure_data->country_code
									])->first();

								$flight_segments[$fl_se_key] = array_merge( $flight_segments[$fl_se_key], $flight_segment );
								if( $operating_airline ){
									$flight_segment['OperatingAirline'] = array_merge( $flight_segment['OperatingAirline'],
										['Name' => $operating_airline->airline_name]
										);
								}		
								$flight_segments[$fl_se_key]['OperatingAirline'] = array_merge( $flight_segments[$fl_se_key]['OperatingAirline'], $flight_segment['OperatingAirline'] );
							}
						}	
						$priced_itinerary['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'] = $flight_segments;
						$priced_itineraries[ $pr_it_key ] = $priced_itinerary;
						$validating_airline_code          = $priced_itinerary['ValidatingAirlineCode'];
						$airline                          = Airline::where(['airline_code' => $validating_airline_code])->first();
						if($airline){
							$validating_airline_name        = array('ValidatingAirlineName' => $airline->airline_name);
							$priced_itineraries[$pr_it_key] = array_merge($priced_itineraries[$pr_it_key], $validating_airline_name );
						}	

					}
					else
					{
						$success = FALSE;
						foreach( $result['Errors'] as $k => $v){
							array_push($error, $v);
						}	
					}	
				}
				$result['PricedItineraries']['PricedItinerary'] = $priced_itineraries;
				$data = $result;

			}else{
				$success = FALSE;
				foreach( $result['Errors'] as $k => $v){
					array_push($error, $v);
				}	
			}
		}else{
			$success = FALSE;
			foreach( (array)$result['Errors'] as $k => $v){
				array_push($error, $v);
			}
		}
		return response_format($success, $data, $error);		

	}

	

	/**
	 * This function returns the current time added by the cache duration in minutes
	 * @return array
	 */
	private function expiry_time(){
		$cache_duration = 1440; // cache duration in minutes
		$expires_at = new DateTime(NULL, new DateTimeZone( Config::get('app.timezone') ));
		$expires_at->add(new DateInterval('PT'.$cache_duration.'M'));
		return $expires_at->format('i');
	}

	/**
	 * This function is used for checking if cached search results for the given search parameters already exists.
	 * @param string $date 
 	 * @param int    $origin_city_id
 	 * @param int    $destination_city_id
 	 * @param enum   $cabin_preference 
 	 * 		choices: Y-Economy, C-Business, F-First
 	 * @param enum   $code
 	 *		choices: "ADT", "CHD", "INF"
 	 * @param int    $quantity	 
	 * @return array
	 */
	private function create_key( $request ){
		return json_encode($request);
	}


	//---------------------------------------- Utility Functions ------------------------------------------

	/**
	 * Function name: setDateTimeWithDateOnly
	 * This function is used to change the date into an accpeted datetime format by Mystifly 
 	 * @param int $val (1 or 0)
	 * @return bool
	 */
	public function convertToBoolean($val){
		if($val == 0)
			return FALSE;
		return TRUE;
	}	

	/**
	 * Function name: setDateTimeWithDateOnly
	 * This function is used to change the date into an accpeted datetime format by Mystifly 
 	 * @param date $date (YYYY-MM-DD)
	 * @return dateTime (YYYY-MM-DDT00:00:00)
	 */
	public function setDateTimeWithDateOnly($date){
		$d = new DateTime($date);
		return $d->format('Y-m-d').'T00:00:00';
	}

	/**
	 * Function name: logRequest
	 * This function is used to log requests sent to MystiflyAPI
	 * @param string $function_name 
 	 * @param object $request
	 * @return void
	 */
	private function logRequest($function_name, $request){
		// Log::info($function_name.' Request : '. json_encode($request) );
	}

	/**
	 * Function name: logResponse
	 * This function is used to log reponses from MystiflyAPI
	 * @param string $function_name 
 	 * @param object $response
	 * @return void
	 */	
	private function logResponse($function_name, $response){
		$res    = get_object_vars($response);
		$rs     = get_object_vars($res[$function_name.'Result']);
		$errors = get_object_vars($rs['Errors']);
		// if($errors)
		// 	Log::error($function_name.' Response : '. json_encode($res) );
		// else
		// 	Log::info($function_name.' Response : '. json_encode($res) );
	}

	public function response_format($success, $data, $error){
		if(is_array($data)){
			if(count($data) < 1)
				$data = NULL;
		}
		if(is_array($error)){
			if(count($error) < 1)
				$error = NULL;
		}		
		$result = array(
					'success' => $success,
					'data' => $data,
					'error' => $error
					);
		return Response::json( $result );
	}


}
