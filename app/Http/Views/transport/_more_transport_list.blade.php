@foreach ($oTransportList as $aTransport)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aTransport->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aTransport->id;?>" value="<?php echo $aTransport->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aTransport->id ])}}">
                {{ $aTransport->from_city_name }}
            </a>
        </td>
        <td>{{ $aTransport->to_city_name }}</td>
        <td>{{ $aTransport->transport_type }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aTransport->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('activity.activity-delete',['nIdActivity'=> $aTransport->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach