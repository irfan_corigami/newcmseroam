<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ trans('messages.eroam_header_title') }}</title>
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/pushy.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/media.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/prettify.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/developer.css') }}" rel="stylesheet" />
        
        
    </head>
    <body class="has-js pushy-open-left">
        <!-- ======================================================================================== -->
        <section class="content-wrapper">
            <div class="container">
              <div class="login-wrapper">
                <h2 class="text-center">{{ trans('messages.eroam_name') }}</h2>
                <div class="login-inner m-t-30">
                    <form action="{{ URL::to('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.user_name_label') }}</label>
                            <input type="text" name="username" class="form-control" placeholder="User Name" autofocus=""> 
                        </div>
                        <div class="form-group">
                            <label class="label-control">{{ trans('messages.password_label') }}</label>
                            <input type="password" name="password" class="form-control" placeholder="Password"> 
                        </div>
                        {{-- Error login --}}
                        @if ($errors->first())
                            <span class="error">{{ $errors->first() }}</span> 
                        @endif
                        <div class="m-t-80">
                            <button class="btn btn-primary btn-block button expand">{{ trans('messages.login_btn') }}</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
        </section>
    </body>
</html>