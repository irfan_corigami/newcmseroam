@if(count($oBookTours) > 0)
@foreach($oBookTours as $aBookTour)
<tr>
    <td>
        <label class="radio-checkbox label_check" for="checkbox-{{$aBookTour->request_id}}">
            <input type="checkbox" id="checkbox-{{$aBookTour->request_id}}" class="cmp_coupon_check" value="{{$aBookTour->request_id}}">&nbsp;
        </label>
    </td>
    <td class="text-center"> <a href="{{URL::to('tour_bookings/'.$aBookTour->request_id)}}">{{$aBookTour->request_id}}</a></td>
    <td>{{ date( 'd/m/y', strtotime( $aBookTour->request_date ) ) }}</td>
    <td>{{ date( 'd/m/y', strtotime( $aBookTour->departure_date ) ) }}</td>
    <td>{{$aBookTour->tour_title}}</td>
    <td>{{$aBookTour->FName}} {{$aBookTour->SurName}}</td>

    <td>{{$aBookTour->booking_currency}} {{number_format(str_replace(',', '', $aBookTour->grand_total) ,2, '.', '')}}</td>

    <td class="text-center">
        <a href="{{URL::to('booking/booked-tour-detail/'.$aBookTour->request_id)}}" class="button success tiny btn-primary btn-sm">Detail</a>
        <a href="" target="_blank" class="button success tiny btn-primary btn-sm">Tour</a>
    </td>
</tr>
@endforeach
@else
<div class="row full-width-row">
    <div class="small-12 column">
        {{ trans('messages.no_record') }}
    </div>
</div>

@endif