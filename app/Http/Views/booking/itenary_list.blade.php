@extends( 'layout/mainlayout' )
@section('custom-css')
<style>
    .success_message{
        color:green !important;
        text-align: center;
    }
    .update-region-btn i {
        margin-left: .5rem;
        font-size: 1.2rem;
    }
    .switch {
        margin-bottom: 0;
    }
</style>
@stop
@section('content')

@if($oItineraries)
<div class="content-container">
    <h1 class="page-title">{{ trans('messages.booking_managment') }}</h1> 
    <div class="box-wrapper">
        <p>{{count($oItineraries)}} Bookings</p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" name="search_str" value="{{ $sSearchStr }}" placeholder="Seach Bookings">
                    <span class="input-group-btn">
                        <button class="btn btn-default" onclick="getMoreListing('{{route('booking.itenary-list')}}',event,'table_record')" type="button"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle"  type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" disabled>ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">                                                                
                    </ul>
                </div>
            </div>
        </div>

        <div class="table-responsive m-t-20 table_record">
            @include('WebView::booking._itenary_list_ajax')
        </div>
    </div>
</div>


@else
<div class="row full-width-row">
    <div class="small-12 column">
        {{ trans('messages.no_record_found') }}
    </div>
</div>

@endif
@stop

@section('custom-js')
<script>
    function getTourSort(element,sOrderField)
    {
        if($(element).find( "i" ).hasClass('fa-caret-down'))
        {
            $(element).find( "i" ).removeClass('fa-caret-down');
            $(element).find( "i" ).addClass('fa-caret-up');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('desc');
            getMoreListing(siteUrl('booking/itenary-list?page=1'),event,'table_record');
        }
        else
        {
            $(element).find( "i" ).removeClass('fa-caret-up');
            $(element).find( "i" ).addClass('fa-caret-down');
            $("input[name='order_field']").val(sOrderField);
            $("input[name='order_by']").val('asc');
            getMoreListing(siteUrl('booking/itenary-list?page=1'),event,'table_record');
        }
    }
    $(document).on('click',".cmp_check",function () { 

        if($('.cmp_check:checked').length == $('.cmp_check').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
    });
    $(document).on('click','.label_check',function(){
        setupLabel();
    });             
    $(document).on('click','.update-status-btn',function(event) {
        var box = $(this).parent().siblings('.update-status-box');
        var span = $(this).parent();
        var statusId = span.data('status-id');
        box.html('<div class="row"><div class="large-8 columns form-group">' +
                '<select class="status-select form-control m-t-10">' +
                '<option value="Pending">Pending</option>' +
                '<option value="Confirm Status">Confirm Status</option>' +
                '<option value="Cancel Status">Cancel Status</option>' +
                '</select></div></div>' +
                '<div class="row"><div class="large-8 columns region_cls"><a href="#" data-status-id="' + statusId + '" class="button success tiny btn-primary btn-sm m-r-10 save-update-status-btn">Save</a><a href="#" class="button success tiny btn-primary btn-sm m-r-10 cancel-update-status-btn">Cancel</a>' +
                '</div></div>'
                ).hide().fadeIn(200);
    });
    $('body').on('click', '.save-update-status-btn', function(event) {
    event.preventDefault();
    var order_id = $(this).data('status-id');
    var status_id = $(this).parent().parent().prev().find('select.status-select').val();
    var span = $(this).parent().parent().parent().prev();
    var cancelBtn = $(this).next('.cancel-update-status-btn');
        $.ajax({
            method: 'post',
            url: '{{ url('booking/update-status') }}',
            data: {
            _token: '{{ csrf_token() }}',
                    order_id: order_id,
                    status_id: status_id
            },
            success: function(response) {
            span.find('.status-name').html(response).effect('highlight', {color: '#91E3AB'}, 300);
            span.data('status-id', order_id)
                    cancelBtn.click();
            }
        });                                             
    });
    $('body').on('click', '.cancel-update-status-btn', function(event) {
        event.preventDefault();
        var box = $(this).parent().parent().parent('.update-status-box');
        var span = box.prev('span');
        box.html('');
        span.show();
    });
    $(document).on('click','.update-voucher-btn',function(event) {
        var box = $(this).parent().siblings('.update-voucher-box');
        var span = $(this).parent();
        var voucherId = span.data('voucher-id');
        box.html('<div class="row"><div class="large-8 columns form-group">' +
                '<select class="voucher-select form-control m-t-10">' +
                '<option value="Sent">Sent</option>' +
                '<option value="Not Sent">Not Sent</option>' +
                '</select></div></div>' +
                '<div class="row"><div class="large-8 columns region_cls"><a href="#" data-voucher-id="' + voucherId + '" class="button success tiny btn-primary btn-sm m-r-10 save-update-voucher-btn">Save</a><a href="#" class="button success tiny btn-primary btn-sm m-r-10 cancel-update-voucher-btn">Cancel</a>' +
                '</div></div>'
                ).hide().fadeIn(200);
    });
    $('body').on('click', '.save-update-voucher-btn', function(event) {
        event.preventDefault();
        var order_id = $(this).data('voucher-id');
        var voucher_id = $(this).parent().parent().prev().find('select.voucher-select').val();
        var span = $(this).parent().parent().parent().prev();
        var cancelBtn = $(this).next('.cancel-update-voucher-btn');
        $.ajax({
        method: 'post',
                url: '{{ url('booking/update-voucher') }}',
                data: {
                _token: '{{ csrf_token() }}',
                        order_id: order_id,
                        voucher_id: voucher_id
                },
                success: function(response) {
                span.find('.voucher-name').html(response).effect('highlight', {color: '#91E3AB'}, 300);
                span.data('voucher-id', order_id)
                        cancelBtn.click();
                }
        });
    });
    $('body').on('click', '.cancel-update-voucher-btn', function(event) {
        event.preventDefault();
        var box = $(this).parent().parent().parent('.update-voucher-box');
        var span = box.prev('span');
        box.html('');
        span.show();
    });
</script>
@stop