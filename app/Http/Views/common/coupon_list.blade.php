@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">Manage {{ trans('messages.coupon') }}</h1>

    @if(Session::has('message'))
        <div class="small-6 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('common.create-coupon') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <p>{{ $oCouponList->count().' '. trans('messages.coupon')  }}</p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search {{ trans('messages.coupon') }}" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" onclick="callCouponListing(event,'table_record')"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                <select name="search-by" class="form-control m-t-10 search_by">
                    <option value="title" {{ $sSearchBy == 'title' ? 'selected' : '' }}>Title</option>
                    <option value="co.code" {{ $sSearchBy == 'c.code' ? 'selected' : '' }}>Code</option>
                </select>
                </div>
            </div>
        </div>
        <div class="row m-t-30 search-wrapper">
            <div class="col-md-7 col-sm-7 m-t-10"> Search Results  </div>
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="javascript://" id="activate">{{ trans('messages.activate') }}</a></li>
                        <li><a href="javascript://" id="deactivate">{{ trans('messages.deactivate') }}</a></li>
                        <li><a href="javascript://" id="delete">{{ trans('messages.delete_action') }}</a></li> 
                    </ul>
                </div>
            </div>
        </div>
        <div class="table-responsive m-t-20 table_record">
            @include('WebView::common._more_coupon_list')      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getCouponSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
        callCouponListing(element,'table_record');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
        callCouponListing(element,'table_record');
    }
}
	$('.alert').click(function(){
		  var c = confirm("Are you sure you want to delete this record?");
    	  return c;	
	});
        
	$(document).ready(function () {
            var cmp_coupon_multiple = [] ;
            $(document).on('click',".cmp_check,#checkbox-00",function () { 
//            $(".cmp_check").click(function () {
                if ($(this).is(':checked')) {
                    $('#dropdownMenu1').prop('disabled', false);
                }else{
                    var removeItem = $(this).val();
                    cmp_coupon_multiple = $.grep(cmp_coupon_multiple, function(value) {
                                return value != removeItem;
                              });
                    if(cmp_coupon_multiple.length == 1){
                        $('#dropdownMenu1').prop('disabled', false);
                    }else if(cmp_coupon_multiple.length < 1){
                        $('#dropdownMenu1').prop('disabled', true);
                    }else{
                        $('#dropdownMenu1').prop('disabled', false);
                    }  
                }
            });
            
            $('#delete, #activate, #deactivate ').click(function () {
                var action = $(this).attr('id');
                var cmp_coupon = [] ;
                $('.cmp_check:checked').each(function() {
                    cmp_coupon.push($(this).val());
                });

                if(cmp_coupon.length){
                    if(confirm('Are you sure?')){
                        $.ajax({
                            type:'POST',
                            url:siteUrl('common/coupon-manage-action'),
                            data:{coupons :cmp_coupon, action : action},
                            success:function (response) { 
                                if (response == 'success') {
                                    location.reload();
                                }
                            }
                        });
                    }
                }
            });
    });
$(document).on('click','.label_check',function(){
    setupLabel();
});   

$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
//function selectAllRow(ele)
//{
//    var cmp_tour = [];
//    
//    if($(ele).children().val() == true) {
//        // Iterate each checkbox
//        $('.cmp_check').each(function() {
//            this.checked = true;
//            cmp_tour.push($(this).val());
//        });
//        $('#dropdownMenu1').prop('disabled', false);
//    }else{
//        $('.cmp_check').each(function() {
//            this.checked = false;
//        });
//        cmp_tour = [] ;
//        $('#dropdownMenu1').prop('disabled', true);
//        $('#dropdownMenu1').prop('disabled', true);
//    }
//}
</script>
@stop