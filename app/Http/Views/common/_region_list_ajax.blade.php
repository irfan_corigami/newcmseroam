<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getUserSort(this,'name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th> {{ trans('messages.countries_thead') }} </th>
            <th>{{ trans('messages.show_eroam_thead') }} </th>
        </tr>
    </thead>
    <tbody class="label_list_ajax">
    @if(count($oRegionList) > 0)
        @foreach ($oRegionList as $aRegion)				
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-{{ $aRegion->id }}">
                        <input type="checkbox" class="cmp_check" id="checkbox-{{ $aRegion->id }}" value="{{ $aRegion->id }}">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="#">{{ $aRegion->name }}</a>
                </td>	
                <td>{{ $aRegion->country_count }}</td>	
                <td class="text-center">
                    <div class="switch tiny switch_cls">
                        <input class="show-on-eroam-btn switch1-state1" data-id="{{ $aRegion->id }}" id="show-on-eroam-{{ $aRegion->id }}" type="checkbox" {{ $aRegion->show_on_eroam == 1 ? 'checked' : '' }}>
                        <label for="show-on-eroam-{{ $aRegion->id }}"></label>
                    </div>
                </td>	
            </tr> 
        @endforeach
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>