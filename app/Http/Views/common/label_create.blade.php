@extends( 'layout/mainlayout' )

@section('content')
    <div class="content-container">
        <h1 class="page-title">{{ trans('messages.add_new_label') }}</h1>
        <div class="row">
            @if (Session::has('message'))
                <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
            @endif

        </div>	
        <br>
        <form action="{{ route('common.create-label') }}" method="post" id="create">
            {{ csrf_field() }}
            <div class="box-wrapper">
                <p>{{ trans('messages.label_detail') }}</p>
                <div class="form-group m-t-30">
                    <label class="label-control">{{ trans('messages.label_name') }}<span class="required">*</span></label>
                    <input type="text" id="name" class="form-control" placeholder="{{ trans('messages.label_name_placehoder') }}" name="name" value="{{ (isset($oLabel)) ? $oLabel->name : old('name') }}"/>
                </div>
                @if ( $errors->first( 'name' ) )
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
                
                <input type="hidden" name="label_id" value="{{ (isset($oLabel)) ? $oLabel->id : ''}}" />
                <div class="row">
                    <div class="m-t-20 row col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="button success btn btn-primary btn-block" type="submit" value="{{ trans('messages.save_btn') }}">
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('common.label-list') }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('custom-css')
<style type="text/css">
	.error{
            color:red !important;
	}
	.success-msg {
		background: #67BB67;
		color: #fff;
		padding: 5px;
	}
	.success-msg a {
		color: #fff;
		text-decoration: underline;
	}
	.error_message{
		color:red !important;
	}
	.with_error{
		border-color: red !important;
	}
	.success_message{
		color:green !important;
		text-align: center;
	}
	div .with_error{
		border:1px solid black;
	}
</style>
@stop
@section('custom-js')
<script type="text/javascript">
$( "#create" ).validate({
    rules: {
        name: {
          required: true
        }
    },
    messages: {
        name: "Name is required!"
    }
});
</script>
@stop
