<table class="table">
    <thead>
        <tr>
            <th width="20px">
                <label class="radio-checkbox label_check" for="checkbox-00" >
                    <input type="checkbox" id="checkbox-00" value="1" onclick="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCouponSort(this, 'code');">{{ trans('messages.coupon_code') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCouponSort(this, 'title');">{{ trans('messages.title') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'title')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCouponSort(this, 'amount');"> {{ trans('messages.amount') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'amount')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCouponSort(this, 'is_active');"> {{ trans('messages.active_status') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'is_active')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th>{{ trans('messages.action_head')}}</th>
        </tr>
    </thead>
    <tbody class="coupon_list_ajax">
        @if(count($oCouponList) > 0)
        @foreach ($oCouponList as $aCoupon)
        <tr>
            <td>
                <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCoupon->id; ?>">
                    <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aCoupon->id; ?>" value="<?php echo $aCoupon->id; ?>">&nbsp;
                </label>
            </td>
            <td>
                <a href="#">
                    {{$aCoupon->code }}
                </a>
            </td>
            <td>{{ $aCoupon->title }}</td>
            <td>
                @if($aCoupon->coupon_type == 'flat') 
                {{ $aCoupon->currency_code.' '.round($aCoupon->amount)}} 
                @else 
                {{ round($aCoupon->amount).'%'}} 
                @endif
            </td>
            <td>@if($aCoupon->is_active == 1) <span>{{ trans('messages.active') }}</span> @else <span>{{ trans('messages.inactive') }}</span> @endif</td>
            <td>
                <a href="{{ route('common.create-coupon',['nIdCoupon'=>$aCoupon->id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn')}}</a>
            </td>
        </tr> 
        @endforeach
        @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
        @endif
    </tbody>
</table>