@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">Manage {{ trans('messages.country') }}</h1>

    @if(Session::has('message'))
        <div class="small-6 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
    @endif
    <div class="box-wrapper">
<!--        <a href="" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>-->
        <p>{{ $oCountryList->count().' '. trans('messages.country')  }}</p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search {{ trans('messages.country') }}" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" onclick="callCountryListing(event,'table_record')"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="form-group">
                <select name="search-by" class="form-control m-t-10 search_by">
                    <option value="name" {{ $sSearchBy == 'name' ? 'selected' : '' }}>Name</option>
                    <option value="code" {{ $sSearchBy == 'code' ? 'selected' : '' }}>Code</option>
                </select>
                </div>
            </div>
        </div>
        <div class="m-t-30">
            <label>{{ trans('messages.show_record') }}</label>
            <select class="select-entry" name="show_record" onchange="callCountryListing(event,'table_record');">
                <option value="10" {{ ($nShowRecord == 10) ? 'selected="selected"' : '' }}>10</option>
                <option value="20" {{ ($nShowRecord == 20) ? 'selected="selected"' : '' }}>20</option>
                <option value="30" {{ ($nShowRecord == 30) ? 'selected="selected"' : '' }}>30</option>
                <option value="40" {{ ($nShowRecord == 40) ? 'selected="selected"' : '' }}>40</option>
            </select>
            <label>{{ trans('messages.entries') }}</label>
        </div>
        <div class="table-responsive m-t-20 table_record">
           
            @include('WebView::common._country_list_ajax')
      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">
$(document).on('ready',function(){
	$('.switch1-state1').bootstrapSwitch();
	$('tbody').on('switchChange.bootstrapSwitch','.switch1-state1', function (event, state) {
            switchChange(state,this);
	});
    });
function switchChange(state,oEle)
{
    var id = $(oEle).data('id');
    var checked = (state === true) ? 1 : 0;
    var label = $(this).next('label');
    showEroamSwitch("{{ route('common.country-switch') }}"+'/'+id , checked);
}
function getCountrySort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
        callCountryListing(element,'table_record');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
        callCountryListing(element,'table_record');
    }
}

$(document).on('click',".cmp_check",function () { 

    if($('.cmp_check:checked').length == $('.cmp_check').length){
        $('#checkbox-00').prop('checked',true);
    }else{
        $('#checkbox-00').prop('checked',false);
    }
});
$(document).on('click','.label_check',function(){
    setupLabel();
});  
</script>
@stop