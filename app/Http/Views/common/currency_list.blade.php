@extends( 'layout/mainlayout' )

@section('custom-css')
<style type="text/css">
.select-user-type {
	display: inline-block;
	border-radius: 4px;
	text-align: center;
	font-size: 0.9rem;
	background: #dcdcdc;
	padding: 10px 25px;
	color: #333;
	transition: all .2s;
}
.select-user-type:hover, .select-user-type.selected {
	background: #666666;
	color: #fff;
}
.select-user-type.selected {
	cursor: default;
}
.search-box {
	margin: 25px 0;
	position: relative;
}
.search-box i.fa {
	position: absolute;
	top: 10px;
	left: 7px;
}
#search-key {
	padding-left: 25px;
}
.fa-check {
	color: #1c812f;
}
.fa-times,
.fa-exclamation-circle {
	color: #bd1b1b;
}
.user-name a {
	color: #2b78b0;
	font-weight: bold;
}
.ajax-loader {
	font-size: 1.5rem;
	display: none;
}
</style>
@stop

@section('content')

<div class="content-container">
    <h1 class="page-title">Manage {{ trans('messages.currency') }}</h1>

    @if(Session::has('message'))
        <div class="small-6 small-centered columns success_message">{{ Session::get('message') }}</div>
        <br>
    @endif
    <div class="box-wrapper">
        <a href="{{ route('common.create-currency') }}" class="plus-icon" title="Add">
            <i class="icon-plus"></i>
        </a>
        <p>{{ $oCurrencyList->count().' '. trans('messages.currency')  }}</p>
        <div class="row m-t-20 search-wrapper">
            <div class="col-md-7 col-sm-7">
                <div class="input-group input-group-box">
                    <input type="text" class="form-control" placeholder="Search {{ trans('messages.currency') }}" name="search_str" value="{{ $sSearchStr }}">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit" onclick="getMoreListing(siteUrl('/common/currency-list'),event,'table_record');"><i class="icon-search-domain"></i></button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="order_field" value="{{ $sOrderField }}" />
            <input type="hidden" name="order_by" value="{{ $sOrderBy }}" />
            <div class="col-md-5 col-sm-5">
                <div class="dropdown">
                    <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>ACTION</button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
<!--                        <li><a href="javascript://" id="activate">{{ trans('messages.activate') }}</a></li>
                        <li><a href="javascript://" id="deactivate">{{ trans('messages.deactivate') }}</a></li>
                        <li><a href="javascript://" id="delete">{{ trans('messages.delete_action') }}</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row m-t-30 search-wrapper">
            <div class="col-md-7 col-sm-7 m-t-10"> Search Results  </div>
            
        </div>
        <div class="table-responsive m-t-20 table_record">
            @include('WebView::common._more_currency_list')      
        </div>
    </div>

</div>
@stop
@section('custom-js')
<script type="text/javascript">

function getCouponSort(element,sOrderField)
{
    if($(element).find( "i" ).hasClass('fa-caret-down'))
    {
        $(element).find( "i" ).removeClass('fa-caret-down');
        $(element).find( "i" ).addClass('fa-caret-up');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('desc');
        getMoreListing(siteUrl('/common/currency-list'),event,'table_record');
        //callCouponListing(element,'table_record');
    }
    else
    {
        $(element).find( "i" ).removeClass('fa-caret-up');
        $(element).find( "i" ).addClass('fa-caret-down');
        $("input[name='order_field']").val(sOrderField);
        $("input[name='order_by']").val('asc');
        getMoreListing(siteUrl('/common/currency-list'),event,'table_record');
    }
}
        
$(document).ready(function () {
    var cmp_coupon_multiple = [] ;
    $(".cmp_coupon_check").click(function () { 
        if ($(this).is(':checked')) {
            $('#dropdownMenu1').prop('disabled', false);
        }else{
            $('#checkbox-coupons').prop('checked', false);
            cmp_coupon_multiple.pop();
            if(cmp_coupon_multiple.length == 1){
                //$('#dropdownMenu1').prop('disabled', false);
            }else if(cmp_coupon_multiple.length < 1){
                //$('#dropdownMenu1').prop('disabled', true);
            }else{
                //$('#dropdownMenu1').prop('disabled', false);
            }    
        }
        if($('.cmp_coupon_check:checked').length == $('.cmp_coupon_check').length){
            $('#checkbox-00').prop('checked',true);
        }else{
            $('#checkbox-00').prop('checked',false);
        }
    });
});
$(document).on('click','.label_check',function(){
    setupLabel();
});   
function selectAllRow(ele)
{
    var cmp_tour = [];
    if(ele.checked === true) {
        // Iterate each checkbox
        $('.cmp_coupon_check').each(function() {
            this.checked = true;
            cmp_tour.push($(this).val());
        });
        
        $('#dropdownMenu1').prop('disabled', false);
    }else{
        $('.cmp_coupon_check').each(function() {
            this.checked = false;
        });
        cmp_tour = [] ;
        $('#dropdownMenu1').prop('disabled', true);
        $('#dropdownMenu1').prop('disabled', true);
    }
    setupLabel();
}
</script>
@stop