<table class="table">
    <thead>
        <tr>
            <th width="20px">
                <label class="radio-checkbox label_check" for="checkbox-00" >
                    <input type="checkbox" id="checkbox-00" value="1" onchange = "selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCouponSort(this, 'name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCouponSort(this, 'code');">{{ trans('messages.code') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'code')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>{{ trans('messages.action_head')}}</th>
        </tr>
    </thead>
    <tbody class="coupon_list_ajax">
        @if(count($oCurrencyList) > 0)
            @foreach ($oCurrencyList as $aCurrency)
            <tr>
                <td>
                    <label class="radio-checkbox label_check" for="checkbox-<?php echo $aCurrency->id; ?>">
                        <input type="checkbox" class="cmp_coupon_check" id="checkbox-<?php echo $aCurrency->id; ?>" value="<?php echo $aCurrency->id; ?>">&nbsp;
                    </label>
                </td>
                <td>
                    <a href="#">
                        {{$aCurrency->name }}
                    </a>
                </td>
                <td>{{ $aCurrency->code }}</td>

                <td>
                    <a href="{{ route('common.create-currency',['nIdCoupon'=>$aCurrency->id])}}" class="button success tiny btn-primary btn-sm pull-left m-r-10">{{ trans('messages.update_btn')}}</a>
                </td>
            </tr> 
            @endforeach
        @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
        @endif
    </tbody>
</table>