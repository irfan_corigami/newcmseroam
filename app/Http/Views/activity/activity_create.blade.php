@extends( 'layout/mainlayout' )

@section('content')
<div class="content-container">

    <h1 class="page-title">{{ trans('messages.add_activity') }}</h1>
    <div class="row">
        @if (Session::has('message'))
        <div class="small-6 small-centered columns success-box">{{ Session::get('message') }}</div>
        @endif

    </div>
    <br>
    @if(isset($oActivity))
    {{ Form::model($oActivity, array('url' => route('activity.activity-create') ,'method'=>'POST','enctype'=>'multipart/form-data')) }}
    @else
    {{Form::open(array('url' => 'activity/activity-create','method'=>'Post','enctype'=>'multipart/form-data')) }}
    @endif
    <div class="box-wrapper">

        <p>Activity Details</p>


        <div class="form-group m-t-30">
            <label class="label-control">Name <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::text('name',Input::old('name'),['id'=>'name','class'=>$attributes,'placeholder'=>'Enter Name'])}}

        </div>
        @if ( $errors->first( 'name' ) )
        <small class="error">{{ $errors->first('name') }}</small>
        @endif
        <div class="form-group m-t-30">
            <label class="label-control">City Name <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            {{Form::select('city_id',$cities,Input::old('city_id'),['id'=>'city_id','class'=>$attributes])}}

        </div>
        @if ( $errors->first( 'city_id' ) )
        <small class="error">{{ $errors->first('city_id') }}</small>
        @endif

        <div class="form-group m-t-30">

            <label class="label-control">{{ trans('messages.end_up_diff_city') }}</label>
            <div>
                <label class="radio-checkbox label_radio" for="radio-01">
                    <input type="radio" id="radio-01" value="1" class="has-destination-city" name="has_destination_city" {{ (Input::old('has_destination_city') == 1) ? 'checked': '' }}> {{ trans('messages.yes') }}
                </label> 
                <label class="radio-checkbox label_radio" for="radio-02">
                    <input type="radio" id="radio-02" value="0" class="has-destination-city" name="has_destination_city" {{ (!Input::old('has_destination_city')) ? 'checked': '' }}
                           {{ (Input::old('has_destination_city') == 0) ? 'checked': '' }}> {{ trans('messages.no') }}
                </label>
            </div>
        </div>
        <div class="form-group m-t-30 destination-city-id-container {{ (Input::old('has_destination_city') == 1) ? '' : 'hidden-field' }}">
            <label class="label-control">Destination City <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <select name="destination_city_id" id="destination-city-id" class="{{ $attributes }}" 
                    {{ (Input::old('has_destination_city') == 1) ? ' required ': ' disabled ' }}>
                    <option disabled {{ (!Input::old('destination_city_id')) ? 'selected="selected"': '' }}>
                    Select Destination City
            </option>
            <?php foreach ($cities as $city_id => $city_name): ?>

                <option value="{{ $city_id }}" 

                        <?php if (Input::old('destination_city_id')): ?>

                            {{ (Input::old('destination_city_id') == $city_id) ? 'selected="selected"': '' }} 	
                        <?php endif ?>
                        >{{ $city_name }}</option>
                    <?php endforeach ?>
        </select>
    </div>
    <!--  @if ( $errors->first( 'destination_city_id' ) )
                                           <small class="error">{{ $errors->first('destination_city_id') }}</small>
                           @endif -->
    <div class="form-group m-t-30">

        <label class="label-control">Are accommodations included?</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-03">
                <input type="radio" id="radio-03" value="1" class="has-accomodation" name="has_accomodation" {{ (Input::old('has_accomodation') == 1) ? 'checked': '' }}> {{ trans('messages.yes') }}
            </label> 
            <label class="radio-checkbox label_radio" for="radio-04">
                <input type="radio" id="radio-04" value="0" class="has-accomodation" name="has_accomodation" {{ (!Input::old('has_accomodation')) ? 'checked': '' }}
                {{ (Input::old('has_accomodation') == 0) ? 'checked': '' }}> {{ trans('messages.no') }}
            </label>
        </div>    
    </div>
    <div class="accommodation-details-container {{ (Input::old('has_accomodation')==1) ? '':'hidden-field' }}">
        <div class="form-group m-t-30">
            <label class="label-control">Accommodation Nights <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <input type="number" name="accommodation_nights" id="accommodation-nights" {{ (Input::old('has_accomodation')==1) ? 'required':'disabled' }} min="0" value="{{ Input::old('accommodation_nights') }}" class="{{$attributes}}" placeholder="Enter Accommodation Nights"/>
        </div>

        <div class="form-group m-t-30">
            <label class="label-control">Accommodation Details <span class="required">*</span></label>
            <?php
            $attributes = 'form-control';
            ?>
            <textarea name="accommodation_details" id="accommodation-details" cols="30" rows="6" 
            {{ (Input::old('has_accomodation')==1) ? 'required':'disabled' }} class="{{$attributes}}">{{ Input::old('accommodation_details') }}</textarea>
        </div>

    </div>	
    <div class="form-group m-t-30">

        <label class="label-control">Are accommodations included?</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-05">
                <input type="radio" id="radio-05" value="1" class="has-transport" name="has_transport" {{ (Input::old('has_transport') == 1) ? 'checked': '' }}> {{ trans('messages.yes') }}
            </label> 
            <label class="radio-checkbox label_radio" for="radio-06">
                <input type="radio" id="radio-06" value="0" class="has-transport" name="has_transport" {{ (!Input::old('has_transport')) ? 'checked': '' }}
                {{ (Input::old('has_transport') == 0) ? 'checked': '' }}> {{ trans('messages.no') }}
            </label>
        </div>    
    </div>
    <div class="form-group m-t-30 transport-details-container {{ (Input::old('has_transport')==1) ? '':'hidden-field' }}">
        <label class="label-control">Transport Details <span class="required">*</span></label>
        <?php
        $attributes = 'form-control';
        ?>
        <textarea placeholder="Enter Transport Details" name="transport_details" id="transport-details" cols="30" rows="6" 
        {{ (Input::old('has_transport')==1) ? 'required':'disabled' }} class="{{$attributes}}">{{ Input::old('transport_details') }}</textarea>
    </div>

</div>	
<div class="box-wrapper">
    <p>Activity-Settings</p>
    <div class="form-group m-t-30">

        <label class="label-control">Default</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-07">
                <input type="radio" id="radio-07" value="1" name="default"> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-08">
                <input type="radio" id="radio-08" value="0" name="default" checked> No
            </label>
        </div>
    </div>
    <div class="form-group m-t-30">

        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Pick Up <span class="required">*</span></label>

        {{Form::text('pickup',Input::old('pickup'),['id'=>'pickup','class'=>$attributes,'placeholder'=>'Enter Pick Up'])}}
    </div>
    @if ( $errors->first( 'pickup' ) )
    <small class="error">{{ $errors->first('pickup') }}</small>
    @endif
    <div class="form-group m-t-30">

        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Drop Off <span class="required">*</span></label>

        {{Form::text('dropoff',Input::old('dropoff'),['id'=>'dropoff','class'=>$attributes,'placeholder'=>'Enter Drop Off'])}}	
    </div>
    @if ( $errors->first( 'dropoff' ) )
    <small class="error">{{ $errors->first('dropoff') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Duration</label>
        {{Form::number('duration',Input::old('duration'),['id'=>'duration','min'=> '1','class'=>$attributes,'placeholder'=>'Enter Duration'])}}
    </div>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Supplier <span class="required">*</span></label>
        {{Form::select('default_activity_supplier_id',$suppliers,Input::old('default_activity_supplier_id'),['id'=>'default_activity_supplier_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'default_activity_supplier_id' ) )
    <small class="error">{{ $errors->first('default_activity_supplier_id') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Operator <span class="required">*</span></label>
        {{Form::select('activity_operator_id',$operators,Input::old('activity_operator_id'),['id'=>'activity_operator_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'activity_operator_id' ) )
    <small class="error">{{ $errors->first('activity_operator_id') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Currency <span class="required">*</span></label>
        {{Form::select('currency_id',$currencies,Input::old('currency_id'),['id'=>'currency_id','class'=>$attributes])}}
    </div>
    @if ( $errors->first( 'currency_id' ) )
    <small class="error">{{ $errors->first('currency_id') }}</small>
    @endif
    <div class="form-group m-t-30">

        <label class="label-control">Eroam Stamp Status</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-09">
                <input type="radio" id="radio-09" value="1" name="eroam_stamp"> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-10">
                <input type="radio" id="radio-10" value="0" name="eroam_stamp" checked> No
            </label>
        </div>
    </div>

</div> 
<div class="box-wrapper">
    <p>Activity Description</p>
    <div class="form-group m-t-30">

        <label class="label-control">Approve By Eroam</label>
        <div>
            <label class="radio-checkbox label_radio" for="radio-11">
                <input type="radio" id="radio-11" value="1" name="approved_by_eroam"> Yes
            </label> 
            <label class="radio-checkbox label_radio" for="radio-12">
                <input type="radio" id="radio-12" value="0" name="approved_by_eroam" checked> No
            </label>
        </div>
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Description <span class="required">*</span></label>
        {{Form::textarea('description',Input::old('description'),['id'=>'description','rows'=>"5",'class'=>$attributes])}}			
    </div>
    @if ( $errors->first( 'description' ) )
    <small class="error">{{ $errors->first('description') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Ranking <span class="required">*</span></label>
        {{Form::select('ranking',['' => 'Select Ranking','1' => '1','2' => '2','3' => '3','4'=>'4','5'=>'5'],Input::old('ranking'),['id'=>'ranking','class'=>$attributes])}}		
    </div>
    @if ( $errors->first( 'ranking' ) )
    <small class="error">{{ $errors->first('ranking') }}</small>
    @endif
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Voucher Comments</label>
        {{Form::text('voucher_comments',Input::old('voucher_comments'),['id'=>'voucher_comments','class'=>$attributes])}}
    </div>   
</div> 
<div class="box-wrapper">
    <p>Activity Location</p>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Address 1</label>
        {{Form::text('address_1',Input::old('address_1'),['id'=>'address_1','class'=>$attributes,'placeholder'=>'Enter Address 1'])}}
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Address 2</label>
        {{Form::text('address_2',Input::old('address_2'),['id'=>'address_2','class'=>$attributes,'placeholder'=>'Enter Address 2'])}}	
    </div>
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Reception Phone</label>
        {{Form::text('reception_phone',Input::old('reception_phone'),['id'=>'reception_phone','class'=>$attributes,'placeholder'=>'Enter Reception Phone'])}}	
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Reception Email</label>
        {{Form::text('reception_email',Input::old('reception_email'),['id'=>'reception_email','class'=>$attributes,'placeholder'=>'Enter Reception Email'])}}			
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Website</label>
        {{Form::text('website',Input::old('website'),['id'=>'website','class'=>$attributes,'placeholder'=>'Enter Website'])}}
    </div> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Pax Type</label>
        {{Form::text('pax_type',Input::old('pax_type'),['id'=>'pax_type','class'=>$attributes,'placeholder'=>'Enter Pax Type'])}}
    </div>  
</div> 
<div class="box-wrapper">
    <p>Activity Label</p> 
    <div class="form-group m-t-30">
        <?php
        $attributes = 'form-control';
        ?>
        <label class="label-control">Labels <span class="required">*</span></label>
        {{Form::select('labels[]',$labels,$aLable,['id'=>'labels','class'=>$attributes,'multiple' => 'true' ])}}
    </div>
    @if ( $errors->first( 'labels' ) )
    <small class="error">{{ $errors->first('labels') }}</small>
    @endif
</div>             					
<div class="m-t-20 row col-md-8 col-md-offset-2">
    <div class="row">
        <div class="col-sm-6">
            {{Form::submit('Save',['class'=>'button success btn btn-primary btn-block']) }}
        </div>
        <div class="col-sm-6">
            <a href="{{ route('activity.activity-list') }}" class="btn btn-primary btn-block">{{ trans('messages.cancel_btn') }}</a>
        </div>
    </div>
</div>	
{{Form::close()}}
</div>
@stop

@section('custom-css')
<style>

    .error{
        color:red !important;
    }
    .hidden-field{
        display:none;
    }
    .error_message{
        color:red !important;
    }
    .with_error{
        border-color: red !important;
    }
    .success_message{
        color:green !important;
        text-align: center;
    }
    div .with_error{
        border:1px solid black;
    }
    label {
        font-weight: bold;
    }
</style>
@stop

@section('custom-js')
<script>
    tinymce.init({
        selector: '#cancellation_policy',
        height: 200,
        menubar: false
    });
    tinymce.init({
        selector: '#voucher_comments',
        height: 200,
        menubar: false
    });
    tinymce.init({
        selector: '#description',
        height: 200,
        menubar: false
    });
</script>
<script>
    // show destination city field when #has-destination-city is "yes"
    $('.has-destination-city').change(
        function ()
        {
            if ($(this).val() == 1)
            {
                $('.destination-city-id-container').show();
                $('#destination-city-id').prop('required', true);
                $('#destination-city-id').prop('disabled', false);
            } else
            {
                $('.destination-city-id-container').hide();
                $('#destination-city-id').prop('required', false);
                $('#destination-city-id').prop('disabled', true);
            }
        }
    );
    // show accomodation nights and details field when #has-accomodation is "yes"
    $('.has-accomodation').change(
        function ()
        {
            if ($(this).val() == 1)
            {
                $('.accommodation-details-container').show();
                $('#accommodation-details').prop('required', true);
                $('#accommodation-details').prop('disabled', false);
                $('#accommodation-nights').prop('required', true);
                $('#accommodation-nights').prop('disabled', false);
            } else
            {
                $('.accommodation-details-container').hide();
                $('#accommodation-details').prop('required', false);
                $('#accommodation-details').prop('disabled', true);
                $('#accommodation-nights').prop('required', false);
                $('#accommodation-nights').prop('disabled', true);
            }
        }
    );
    // show transport nights and details field when #has-transport is "yes"
    $('.has-transport').change(
        function ()
        {
            if ($(this).val() == 1)
            {
                $('.transport-details-container').show();
                $('#transport-details').prop('required', true);
                $('#transport-details').prop('disabled', false);
            } else
            {
                $('.transport-details-container').hide();
                $('#transport-details').prop('required', false);
                $('#transport-details').prop('disabled', true);
            }
        }
    );
</script>
@stop
