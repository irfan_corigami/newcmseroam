<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getActivitySort(this,'a.name');">{{ trans('messages.activity') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'a.name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getActivitySort(this,'c.name');">{{ trans('messages.city') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'c.name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getActivitySort(this,'ao.name');">{{ trans('messages.operator') }} 
                <i class="{{ ( $sOrderBy == 'asc' && $sOrderField == 'ao.name' )? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th>{{ trans('messages.thead_season_name') }}</th>
            <th>{{ trans('messages.from') }}</th>
            <th>{{ trans('messages.to') }}</th>
            <th>{{ trans('messages.min_pax') }}</th>
            <th>{{ trans('messages.allotment') }}</th>
            <th>{{ trans('messages.price') }}</th>
            <th>{{ trans('messages.supplier') }}</th>
            <th class="text-center">{{ trans('messages.thead_action') }}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oActivitySeasonList) > 0)
        @include('WebView::activity._more_activity_season_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5">
        <p class="showing-result">
            {{ trans('messages.show_out_of_record',['current' => $oActivitySeasonList->count() , 'total'=>$oActivitySeasonList->total() ]) }}
        </p>
    </div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
      </ul>
    </div>
</div>

<script type="text/javascript">
$(function() {
    $('.pagination').pagination({
        pages: {{ $oActivitySeasonList->lastPage() }},
        itemsOnPage: 10,
        currentPage: {{ $oActivitySeasonList->currentPage() }},
        displayedPages:2,
        edges:1,
        onPageClick(pageNumber, event){
            if(pageNumber > 1)
                getMoreListing(siteUrl('activity/activity-season-list?page='+pageNumber),event,'city_list_ajax');
            else
                getMoreListing(siteUrl('activity/activity-season-list?page='+pageNumber),event,'tour_list_ajax');
            $('#checkbox-00').prop('checked',false);
            setupLabel();
        }
    });
});
</script>