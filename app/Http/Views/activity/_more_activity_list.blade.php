@foreach ($oActivityList as $aActivity)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aActivity->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aActivity->id;?>" value="<?php echo $aActivity->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aActivity->id ])}}">
                {{ $aActivity->name }}
            </a>
        </td>
        <td>{{ $aActivity->city_name }}</td>
        <td>{{ $aActivity->supplier_name }}</td>
        <td>{{ $aActivity->operator_name }}</td>
        <td>{{ $aActivity->duration }}</td>
        <td>{{ $aActivity->currency_code }}</td>
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('activity.activity-create',[ 'nIdActivity' => $aActivity->id ])}}" class="button success tiny btn-primary btn-sm">{{ trans('messages.update_btn') }}</a>
                <input type="button" class="button btn-delete tiny btn-primary btn-sm" value="{{ trans('messages.delete_btn') }}" onclick="callDeleteRecord(this,'{{ route('activity.activity-delete',['nIdActivity'=> $aActivity->id]) }}','{{ trans('messages.delete_label')}}')">
            </div>
        </td>
    </tr> 
@endforeach