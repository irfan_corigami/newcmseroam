<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00">
                    <input type="checkbox" id="checkbox-00" value="1" onchange="selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th onclick="getCountrySort(this,'a.name');">{{ trans('messages.name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'a.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'c.name');">{{ trans('messages.city') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'c.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'acs.name');">{{ trans('messages.supplier') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'acs.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }} "></i>
            </th>
            <th onclick="getCountrySort(this,'ao.name');"> {{ trans('messages.operator') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'ao.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'a.duration');"> {{ trans('messages.duration') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'a.duration')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getCountrySort(this,'cu.code');"> {{ trans('messages.thead_currency') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'cu.code')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th class="text-center">{{ trans('messages.thead_action')}}</th>
        </tr>
    </thead>
    <tbody class="city_list_ajax">
    @if(count($oActivityList) > 0)
        @include('WebView::activity._more_activity_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oActivityList->count() , 'total'=>$oActivityList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oActivityList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oActivityList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                if(pageNumber > 1)
                    callActivityListing(event,'city_list_ajax',pageNumber);
                else
                    callActivityListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
            }
        });
    });
</script>