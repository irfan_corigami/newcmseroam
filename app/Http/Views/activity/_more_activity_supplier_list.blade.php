@foreach ($oActivitySupplierList as $aActivity)				
    <tr>
        <td>
            <label class="radio-checkbox label_check" for="checkbox-<?php echo $aActivity->id;?>">
                <input type="checkbox" class="cmp_check" id="checkbox-<?php echo $aActivity->id;?>" value="<?php echo $aActivity->id;?>">&nbsp;
            </label>
        </td>
        <td>
            <a href="#">
                {{ $aActivity->name }}
            </a>    
        </td>
        <td>{{ $aActivity->marketing_contact_name }}</td>
        <td>{{ $aActivity->marketing_contact_phone }}</td>
        <td>{{ $aActivity->marketing_contact_email }}</td>
        
        <td class="text-center">
            <div class="switch tiny switch_cls">
                <a href="{{ route('activity.create-activity-supplier',[ 'nIdActivity' => $aActivity->id ])}}" class="button success tiny btn-primary btn-sm m-r-10">{{ trans('messages.update_btn') }}</a>
            </div>
        </td>
    </tr> 
@endforeach