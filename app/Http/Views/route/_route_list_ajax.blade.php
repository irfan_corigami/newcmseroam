<table class="table">
    <thead>
        <tr>
            <th>
                <label class="radio-checkbox label_check" for="checkbox-00" >
                    <input type="checkbox" id="checkbox-00" value="1" onclick = "selectAllRow(this);">&nbsp;
                </label>
            </th>
            <th>{{ trans('messages.route_type') }}</th>
            <th>{{ trans('messages.route_code') }}</th>
            <th onclick="getRouteSort(this,'name');"> {{ trans('messages.route_name') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getRouteSort(this,'cf.name');"> {{ trans('messages.route_start') }} 
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'cf.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th onclick="getRouteSort(this,'ct.name');">{{ trans('messages.route_finish') }}
                <i class="{{ ($sOrderBy == 'asc' && $sOrderField == 'ct.name')? 'fa fa-caret-down' : 'fa fa-caret-up' }}"></i>
            </th>
            <th>{{ trans('messages.route_night') }}</th>
            <th>{{ trans('messages.route_cost') }}</th>
        </tr>
    </thead>
    <tbody class="route_list_ajax">
    @if(count($oRouteList) > 0)
        @include('WebView::route._more_route_list')
    @else
        <tr><td colspan="10" class="text-center">{{ trans('messages.no_record_found') }}</td></tr>
    @endif
    </tbody>
</table>
<div class="clearfix">
    <div class="col-sm-5"><p class="showing-result">{{ trans('messages.show_out_of_record',['current' => $oRouteList->count() , 'total'=>$oRouteList->total() ]) }}</p></div>
    <div class="col-sm-7 text-right">
      <ul class="pagination">
        
      </ul>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.pagination').pagination({
            pages: {{ $oRouteList->lastPage() }},
            itemsOnPage: 10,
            currentPage: {{ $oRouteList->currentPage() }},
            displayedPages:2,
            edges:1,
            onPageClick(pageNumber, event){
                if(pageNumber > 1)
                    callRouteListing(event,'route_list_ajax',pageNumber);
                else
                    callRouteListing(event,'table_record',pageNumber);
                $('#checkbox-00').prop('checked',false);
                setupLabel();
            }
        });
    });
</script>