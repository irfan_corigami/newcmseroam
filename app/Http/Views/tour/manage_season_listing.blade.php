<div class="box-wrapper">
    {{ Form::open(array('url' => 'tours/manage/dates/'.$oTour['tour_id'],'method'=>'Get')) }} 
    <div class="row m-t-20 search-wrapper">
        <div class="col-md-7 col-sm-7">
            <div class="input-group input-group-box">
                <input type="text" class="form-control" name="search" value="{{Input::get('search')}}" placeholder="Search Seasons">
                <input type="hidden" name="search-by" value="Ref. Number">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="icon-search-domain"></i></button>
                </span>
            </div>
        </div>
        {{Form::close()}}
        <div class="col-md-5 col-sm-5">
            <div class="dropdown">
                <button class="btn btn-primary btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" disabled>ACTION</button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="javascript://" id="edit_season_details">{{ trans('messages.tour_edit_season')}}</a></li>
                    <li><a href="javascript://" id="edit_calender_dates">{{ trans('messages.tour_edit_calendar')}}</a></li>
                    <li><a href="javascript://" id="delete_season">{{ trans('messages.delete_season')}}</a></li>
                </ul> 
            </div>
        </div>
    </div>
    <div class="table-responsive m-t-20">
        <table class="table">
            <thead>
                <tr>
                    <th width="20px">
                        <label class="radio-checkbox label_check" for="checkbox-dates">
                            <input type="checkbox" id="checkbox-dates" value="1">&nbsp;
                        </label>
                    </th>
                    <th>{{ trans('messages.tour_season_name')}}</th>
                    <th>{{ trans('messages.start_date')}}</th>
                    <th>{{ trans('messages.end_date')}}</th>
                    <th>{{ trans('messages.adult_price')}}</th>
                    <th>{{ trans('messages.inventory')}}</th>
                    <th>{{ trans('messages.flights')}}</th>
                    <th>{{ trans('messages.local')}}</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($oDates) && count($oDates) > 0) {
                    $i = 0;
                    $todaydate = date('Y-m-d');
                    foreach ($oDates as $date) {
//                        $StartDateStart = Carbon\Carbon::createFromFormat('Y-m-01 H:i:s',$date->StartDate);
//                        $EndDateStart = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date->EndDate);
                        $StartDateStart = date('Y-m-01', strtotime($date->StartDate));
                        $EndDateStart = date('Y-m-d', strtotime($date->EndDate . '+1 day'));
                        //echo $StartDateStart;exit;
                        $StartDate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date->StartDate);
                        $EndDate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$date->EndDate);


                        $disDays = $StartDate->diffInDays($EndDate);
                        //$disDays = $diff->format("%R%a");

                        //$date3 = new DateTime($EndDate);
                        $visDays = $EndDate->diffInDays($StartDate);
                       //$visDays = $diff->format("%R%a");
                        //echo $visDays;exit;
                        $diffMonth = $StartDate->diffInMonths($EndDate);

                        if ($diffMonth > 3) {
                            $monthCount = round($diffMonth / 3);
                            $mod = round($diffMonth % 3);
                            if ($mod > 0) {
                                $monthCount = $monthCount + 1;
                            }
                            $numberOfMonths = ($monthCount * 3) - 1;
                            //echo 'diffMonth='.$diffMonth.'//monthCount='.$monthCount.'//mod='.$mod.'//numberOfMonths='.$numberOfMonths.'<br>';
                            $EndDateEnd = date('Y-m-t', strtotime($date->StartDate . '+' . $numberOfMonths . ' month'));
                        } else {
                            $EndDateEnd = date('Y-m-t', strtotime($date->EndDate));
                        }
                        ?>
                    <script type="text/javascript">
                        var keyId = <?php echo $date->season_id; ?>;
                        disableDates['key_' + keyId] = [
        <?php while (strtotime($StartDateStart) < strtotime($StartDate)) { ?>
                            '<?php echo $StartDateStart; ?>',
            <?php $StartDateStart = date("Y-m-d", strtotime("+1 day", strtotime($StartDateStart)));
        } ?>

        <?php while (strtotime($EndDateStart) <= strtotime($EndDateEnd)) { ?>
                            '<?php echo $EndDateStart; ?>',
            <?php $EndDateStart = date("Y-m-d", strtotime("+1 day", strtotime($EndDateStart)));
        } ?>
                        ];
                    </script>

                    <tr>
                        <td>
                            <label class="radio-checkbox label_check" for="checkbox-{{ $date->season_id }}">
                                <input type="checkbox" class="cmp_date_check" name="tour" id="checkbox-{{ $date->season_id }}" value="{{ $date->season_id }}" data-advPur="{{ $date->adv_purchase }}" data-id= "{{$i}}" data-day = "{{ $visDays }}" data-month = "{{ $diffMonth }}">&nbsp;
                            </label>
                        </td>
                        <td id="seasonname_{{$i}}">{{ $date->SeasonName }}</td>
                        <td>{{ date('d-m-Y', strtotime($date->StartDate)) }} <span id="startDate_{{$i}}" style="display:none;">{{ date('Y-m-d', strtotime($date->StartDate)) }}</span></td>
                        <td>{{ date('d-m-Y', strtotime($date->EndDate)) }}<span id="endDate_{{$i}}" style="display:none;">{{ date('Y-m-d', strtotime($date->EndDate)) }}</span></td>
                        <td>AUD$ {{ number_format($date->AdultPrice,2) }}</td>
                        <td id="availability_{{$i}}">{{ $date->Availability }}</td>
                        <?php
                        $local = 0;
                        if (!empty($localPayment)) {
                            foreach ($localPayment as $key1 => $value1) {
                                if ($date->season_id == $value1->season_id) {
                                    $local = $value1->Price;
                                }
                            }
                        }
                        ?>
                        <?php
                        $flightPrice = 0;
                        if (!empty($flightPayment)) {
                            foreach ($flightPayment as $key1 => $value1) {
                                if ($date->season_id == $value1->season_id) {
                                    $flightPrice = $value1->flightPrice;
                                }
                            }
                        }
                        ?>
                        <td>AUD$ {{ number_format($flightPrice,2) }}</td>
                        <td>AUD$ {{ number_format($local,2) }}</td>
                        <td>
                            <input type="hidden" id="adultPriceSingle_{{$i}}" value="{{ $date->AdultPriceSingle }}" >
                            <input type="hidden" id="adultPrice_{{$i}}" value="{{ $date->AdultPrice }}" >
                            <input type="hidden" id="adultSupplement_{{$i}}" value="{{ $date->AdultSupplement }}" >
                            <input type="hidden" id="childPrice_{{$i}}" value="{{ $date->ChildPrice }}" >
                            <input type="hidden" id="childPriceSingle_{{$i}}" value="{{ $date->ChildPriceSingle }}" >  
                            <input type="hidden" id="childSupplement_{{$i}}" value="{{ $date->ChildSupplement }}" >
                            <input type="hidden" name="season_id" id="season_id_{{$i}}" value="{{ $date->season_id }}" >
                            <input type="hidden" id="sDateSDAll_{{$i}}" value = "{{ count($seasonDates[$date->season_id]) }}" >
                            <input type="hidden" name="tour_id" id="tour_id" value="{{ $date->tour_id}}" >
                            <input type="hidden" name="adv_purchase" id="adv_purchase_{{$i}}" value="{{ $date->adv_purchase}}" >
                            <input type="hidden" name="is_week" id="is_week_{{$i}}" data-mon="{{ $date->is_mon}}" data-tue="{{ $date->is_tue}}" data-wed="{{ $date->is_wed}}" data-thu="{{ $date->is_thu}}" data-fri="{{ $date->is_fri}}" data-sat="{{ $date->is_sat}}" data-sun="{{ $date->is_sun}}" >
                            <input type="hidden" id="flight_currency_id_{{$i}}" value="{{ $date->flight_currency_id}}" >
                            <input type="hidden" id="flightPrice_{{$i}}" value="{{ $date->flightPrice }}" >
                            <input type="hidden" id="flightDescription_{{$i}}" value="{{ $date->flightDescription }}" >
                            <input type="hidden" id="flightDepart_{{$i}}" value="{{ $date->flightDepart }}" >
                            <input type="hidden" id="flightReturn_{{$i}}" value="{{ $date->flightReturn }}" >
                        </td>
                    </tr>
        <?php $i++;
    }
} else {
    ?>
                <tr><td colspan="9" class="text-center">I don't have any records!</td></tr>
<?php } ?>
            </tbody>    
        </table>
    </div>
</div>    