@extends( 'layout/mainlayout' )

@section('content')
    <div class="content-container">
        <h1 class="page-title">Add New {{ trans('messages.'.$sUserType) }}</h1>
        <div class="row">
            @if ( Session::has( 'message' ) && Session::get( 'message' ) == 'success' )
                <div class="small-6 small-centered columns success-box">
                    <a href="{{ URL::to( 'licensee/' . Session::get( 'user_id' ) ) }}">{{ Session::get( 'licensee_name' ) }}</a> 
                    account has been created.
                </div>
            @endif

        </div>	
        <br>
        <form action="{{ URL::to('user.create-user') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="type" value="{{ $sUserType }}" />
            <div class="box-wrapper">
                <p>Licensee Details</p>
                <div class="form-group m-t-30">
                    <label class="label-control">Licensee Name <span class="required">*</span></label>
                    <input type="text" id="name" class="form-control" placeholder="Enter Licensee Name" name="name"/>
                </div>
                @if ( $errors->first( 'name' ) )
                    <small class="error">{{ $errors->first('name') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">Domain <span class="required">*</span></label>
                    <input type="text" id="domain" class="form-control" placeholder="Enter Domain Name" name="domain"/>
                </div>
                @if ( $errors->first( 'domain' ) )
                    <small class="error">{{ $errors->first('domain') }}</small>
                @endif
                <div class="form-group m-t-30">
                    <label class="label-control">Email (username) <span class="required">*</span></label>
                    <input type="text" id="email" class="form-control" placeholder="Enter Email Address" name="username" />
                </div>
                @if ( $errors->first( 'username' ) )
                    <small class="error">{{ $errors->first('username') }}</small>
                @endif

                <div class="form-group m-t-30">
                    <label class="label-control">Password <span class="required">*</span></label>
                    <div class="small-2 column">
                        <a class="prefix toggle-password button secondary tiny btn-sm btn-primary">Hide</a> 
                        <input name="password" type="text" value="">
                        <a href="#" class="button postfix generate-password btn-sm btn-primary">Generate</a>
                    </div>
                </div>
                @if ( $errors->first( 'password' ) )
                    <small class="error">{{ $errors->first('password') }}</small>
                @endif
                
                @if($sUserType == config('constants.USERTYPELICENSEE'))
                    <div class="form-group m-t-30">
                        <label class="label-control">Duration of Availability (days) <span class="required">*</span></label>
                        <input min="0" id="duration" class="form-control" placeholder="Duration of Availability (days)" name="duration" type="number" value="7">

                    </div>
                    @if ( $errors->first( 'duration' ) )
                        <small class="error">{{ $errors->first('duration') }}</small>
                    @endif
                @else

                    <div class="form-group m-t-30">
                        <label class="label-control">Contact Number <span class="required">*</span></label>
                        <input type="text" name="contact_number" class="form-control" placeholder="Enter Contact Number"/>

                    </div>
                    @if ( $errors->first( 'contact_number' ) )
                        <div class="small-12 column">
                            <small class="error">{{ $errors->first('contact_number') }}</small>
                        </div>
                    @endif

                    <div class="form-group m-t-30">
                        <label class="label-control">Commission Percentage <span class="required">*</span> 
                            <span data-tooltip aria-haspopup="true" class="has-tip" title="This is the percentage taken from the total selling price of the booked itinerary which will serve as the commission of the agent.">
                                <i class="fa fa-question-circle fa-lg" aria-hidden="true"></i>
                            </span>
                        </label>
                        <div class="input-group input-group-box">
                            <input type="number" step="0.01" min="0" name="percentage" class="form-control" value="0.00" placeholder="Enter Commission Percentage"/>

                            <span class="input-group-btn">
                             <a class="button postfix btn btn-default">%</a>
                            </span>

                        </div>

                    </div>

                    @if ( $errors->first( 'percentage' ) )
                        <div class="small-12 column">
                            <small class="error">{{ $errors->first('percentage') }}</small>
                        </div>
                    @endif
                    <div class="form-group m-t-30">
                        <label class="label-control">Address <span class="required">*</span></label>
                        <textarea name="address" class="form-control" rows="3" placeholder="Enter Address"></textarea>
                    </div>
                    @if ( $errors->first( 'address' ) )
                        <div class="small-12 column">
                            <small class="error">{{ $errors->first('address') }}</small>
                        </div>
                    @endif
                @endif
                <div class="row">
                    <div class="m-t-20 row col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="button success btn btn-primary btn-block" type="submit" value="Create Account">
                            </div>
                            <div class="col-sm-6">
                                <a href="{{ route('user.list',['sUserType' => $sUserType ]) }}" class="btn btn-primary btn-block">Cancel</a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('custom-css')
<style type="text/css">
	.error{
			color:red !important;
	}
	.success-msg {
		background: #67BB67;
		color: #fff;
		padding: 5px;
	}
	.success-msg a {
		color: #fff;
		text-decoration: underline;
	}
	.error_message{
		color:red !important;
	}
	.with_error{
		border-color: red !important;
	}
	.success_message{
		color:green !important;
		text-align: center;
	}
	div .with_error{
		border:1px solid black;
	}
</style>
@stop

@section('custom-js')
<script type="text/javascript">
    $( function() {
        $( '.generate-password' ).click( function( e ) {
            e.preventDefault();
            var generatedHash = Math.random().toString(36).slice(-16).toUpperCase();
            $( 'input[name="password"]' ).val( generatedHash );
        });

        $( '.toggle-password' ).click( function( e ) {
            password = $( 'input[name="password"]' );
            if ( password.attr( 'type' ) == 'text' ) {
                   password.attr( 'type', 'password' );
                   $( this ).text( 'Show' );
            } else {
                   password.attr( 'type', 'text' );
                   $( this ).text( 'Hide' );
            }
        });
    });
</script>
@stop