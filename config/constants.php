<?php

return [
	
	'USERTYPEADMIN' => 'admin',
	'USERTYPEAGENT' =>'agent',
	'USERTYPELICENSEE' => 'licensee',
	'USERTYPECUSTOMER' => 'customer',
    
    //pagination
	'PERPAGERECORDS' => 10,
];