<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('WebView::home.dashboard');
});
Route::match(['get','post'],'/user/list/{sUserType}',  ['as' => 'user.list', 'uses' => 'UserController@callUserList']);
Route::match(['get','post'],'/login',  ['as' => 'login', 'uses' => 'UserController@callUserLogin']);
Route::match(['get','post'],'/logout',  ['as' => 'logout', 'uses' => 'UserController@callUserLogout']);
Route::post('user/username-exists', 'UserController@checkUserNameExists');
Route::match(['get','post'],'/user/create-licensee',  ['as' => 'user.create-licensee', 'uses' => 'UserController@callCreateUserLicensee']);
Route::match(['get','post'],'/user/create-agent',  ['as' => 'user.create-agent', 'uses' => 'UserController@callCreateUserAgent']);
Route::match(['get','post'],'/user/create-customer',  ['as' => 'user.create-customer', 'uses' => 'UserController@callCreateUserCustomer']);

Route::match(['get','post'],'/common/label-list',  ['as' => 'common.label-list', 'uses' => 'CommonController@callLabelList']);
Route::match(['get','post'],'/common/create-label/{nLabelId?}',  ['as' => 'common.create-label', 'uses' => 'CommonController@callLabelCreate']);
Route::match(['get','post'],'/common/delete-label/{nLabelId}',  ['as' => 'common.delete-label', 'uses' => 'CommonController@callLabelDelete']);
Route::match(['get','post'],'/common/region-list',  ['as' => 'common.region-list', 'uses' => 'CommonController@callRegionList']);
Route::match(['get','post'],'/common/region-switch/{nRegionId?}',  ['as' => 'common.region-switch', 'uses' => 'CommonController@callRegionSwitch']);
Route::match(['get','post'],'/common/country-list',  ['as' => 'common.country-list', 'uses' => 'CommonController@callCountryList']);
Route::match(['get','post'],'/common/country-switch/{nRegionId?}',  ['as' => 'common.country-switch', 'uses' => 'CommonController@callCountrySwitch']);
Route::match(['get','post'],'/common/city-list',  ['as' => 'common.city-list', 'uses' => 'CommonController@callCityList']);
Route::match(['get','post'],'/common/create-city/{nCityId?}',  ['as' => 'common.create-city', 'uses' => 'CommonController@callCityCreate']);
Route::match(['get','post'],'/common/delete-city/{nCityId}',  ['as' => 'common.delete-city', 'uses' => 'CommonController@callCityDelete']);
Route::match(['get','post'],'/common/city-images',  ['as' => 'common.city-images', 'uses' => 'CommonController@callCityImageUpload']);
Route::match(['get','post'],'/common/delete-city-images/{nCityId}',  ['as' => 'common.delete-city-images', 'uses' => 'CommonController@callCityImageDelete']);
Route::match(['get','post'],'/common/primary-city-images/{nCityId}',  ['as' => 'common.primary-city-images', 'uses' => 'CommonController@callSetCityImagePrimary']);
Route::get('common/aot-location/{sType?}',  ['as' => 'common.aot-location', 'uses' =>  'CommonController@getAotLocations']);
Route::match(['get','post'],'/common/coupon-list',  ['as' => 'common.coupon-list', 'uses' => 'CommonController@callCouponList']);
Route::match(['get','post'],'/common/create-coupon/{nCouponId?}',  ['as' => 'common.create-coupon', 'uses' => 'CommonController@callCouponCreate']);
Route::match(['get','post'],'/common/coupon-manage-action',  ['as' => 'common.manage-action', 'uses' => 'CommonController@callManageAction']);
Route::match(['get','post'],'/common/get-cities-by-country',['as' => 'common.get-cities-by-country' ,'uses'=> 'CommonController@getCityCountryWise']);
Route::match(['get','post'],'/common/get-countries-by-region',['as' => 'common.get-countries-by-region' ,'uses'=> 'CommonController@getCountryRegionWise']);
Route::match(['get','post'],'/common/currency-list',  ['as' => 'common.currency-list', 'uses' => 'CommonController@callCurrencyList']);
Route::match(['get','post'],'/common/create-currency/{nIdCurrency?}',  ['as' => 'common.create-currency', 'uses' => 'CommonController@callCurrencyCreate']);

//acomodation
Route::match(['get','post'],'/acomodation/hotel-list',  ['as' => 'acomodation.hotel-list', 'uses' => 'AcomodationController@callHotelList']);
Route::match(['get','post'],'/acomodation/hotel-create',  ['as' => 'acomodation.hotel-create', 'uses' => 'AcomodationController@callHotelCreate']);

//tour
Route::match(['get','post'],'/tour/tour-list',  ['as' => 'tour.tour-list', 'uses' => 'TourController@callTourList']);
Route::match(['get','post'],'/tour/tour-create/{nTourId?}',  ['as' => 'tour.tour-create', 'uses' => 'TourController@callTourCreate']);
Route::match(['get','post'],'/tour/tour-images', ['as' => 'tour.tour-images' ,'uses'=>'TourController@ImageUpload']);
Route::match(['get','post'],'/tour/provider-data', ['as' => 'tour.provider-data' ,'uses'=>'TourController@getProviderData']);
Route::match(['get','post'],'/tour/delete-tour-images/{nCityId}',  ['as' => 'tour.delete-tour-images', 'uses' => 'TourController@callTourImageDelete']);
Route::match(['get','post'],'tour/sort-tour-images', ['as' => 'tour.sort-tour-images', 'uses'=>'TourController@sortImages']);
Route::match(['get','post'],'tour/change-status', ['as' => 'tour.change-status', 'uses'=>'TourController@callChangeStatus']);
Route::post('tour/manage-view','TourController@callManageView');
Route::match(['get','post'],'tour/manage-dates/{nTourId?}',['as'=>'tour.manage-dates','uses'=>'TourController@callManageTourDates']);
Route::post('tour/remove-season','TourController@callRemoveSeason');
Route::match(['get','post'],'/tour/manage-flight-payment',['as'=>'tour.manage-flight-payment','uses'=>'TourController@callManageFlightPayment']);
Route::match(['get','post'],'/tour/manage-season',['as'=>'tour.manage-season','uses'=>'TourController@callManageSeason']);
Route::match(['get','post'],'/tour/manage-payment',['as'=>'tour.manage-payment','uses'=>'TourController@callManagePayment']);
Route::match(['get','post'],'tour/update/dates/{nTourId}',['as'=>'tour.update-dates','uses'=>'TourController@callUpdateSeasonDates']);
Route::match(['get','post'],'/tour/tour-type-logo-list',  ['as' => 'tour.tour-type-logo-list', 'uses' => 'TourController@callTourTypeLogoList']);
Route::match(['get','post'],'/tour/tour-logo-create/{nIdTourLogo?}',  ['as' => 'tour.tour-logo-create', 'uses' => 'TourController@callTourTypeLogoCreate']);
Route::match(['get','post'],'/tour/tour-logo-delete/{nIdTourLogo}',  ['as' => 'tour.tour-logo-delete', 'uses' => 'TourController@callTourTypeLogoDelete']);

//route module
Route::match(['get','post'],'/route/route-list',  ['as' => 'route.route-list', 'uses' => 'RoutePlanController@getRouteList']);
Route::match(['get','post'],'/route/route-create',  ['as' => 'route.route-create', 'uses' => 'RoutePlanController@callRouteCreate']);
Route::match(['get'],'route/route-view/{nRouteId}', ['as' => 'route.route-view', 'uses' => 'RoutePlanController@callRouteView']);
Route::match(['get','post'],'route/get-transport-by-city', ['as' => 'route.get-transport-by-city', 'uses' => 'RoutePlanController@getTransportByCity']);
Route::post('route/route-delete', ['as' => 'route.route-delete','before' => 'csrf', 'uses' => 'RoutePlanController@callDeleteRoute']);
Route::post('route/route-default', ['as' => 'route.route-default','before' => 'csrf', 'uses' => 'RoutePlanController@callSetDefaultRoute']);

//activity module
Route::match(['get','post'],'/activity/activity-list',  ['as' => 'activity.activity-list', 'uses' => 'ActivityController@callActivityList']);
Route::match(['get','post'],'/activity/activity-delete/{nIdActivity?}',  ['as' => 'activity.activity-delete', 'uses' => 'ActivityController@callActivityDelete']);
Route::match(['get','post'],'/activity/activity-create/{nIdActivity?}',  ['as' => 'activity.activity-create', 'uses' => 'ActivityController@callActivityCreate']);
Route::match(['get','post'],'/activity/activity-season-list',  ['as' => 'activity.activity-season-list', 'uses' => 'ActivityController@callActivitySeasonList']);
Route::match(['get','post'],'/activity/activity-season-create/{nId?}/{nFrom?}',  ['as' => 'activity.activity-season-create', 'uses' => 'ActivityController@callActivitySeasonCreate']);
Route::match(['get','post'],'/activity/activity-season-delete/{nIdActivity?}',  ['as' => 'activity.activity-season-delete', 'uses' => 'ActivityController@callActivitySeasonDelete']);
Route::match(['get','post'],'/activity/activity-supplier-list',  ['as' => 'activity.activity-supplier-list', 'uses' => 'ActivityController@callActivitySupplierList']);
Route::match(['get','post'],'/activity/create-activity-supplier/{nIdActivitySupplier?}',  ['as' => 'activity.create-activity-supplier', 'uses' => 'ActivityController@callActivitySupplierCreate']);
Route::match(['get','post'],'/activity/activity-markup-list',  ['as' => 'activity.activity-markup-list', 'uses' => 'ActivityController@callActivityMarkupList']);
Route::match(['get','post'],'/activity/create-activity-markup/{nIdActivityMarkup?}',  ['as' => 'activity.create-activity-markup', 'uses' => 'ActivityController@callActivityMarkupCreate']);
Route::match(['get','post'],'/activity/all-activity-list', ['as' => 'activity.all-activity-list', 'uses' =>'ActivityController@getAllActivity', ['before' => 'csrf']]);
Route::match(['get','post'],'/common/all-city-list', ['as' => 'activity.all-city-list', 'uses' =>'CommonController@getAllCity', ['before' => 'csrf']]);
Route::match(['get','post'],'/common/all-country-list', ['as' => 'activity.all-country-list', 'uses' =>'CommonController@getAllCountry', ['before' => 'csrf']]);

//transport
Route::match(['get','post'],'/transport/transport-list',  ['as' => 'transport.transport-list', 'uses' => 'TransportController@callTransportList']);
Route::match(['get','post'],'/transport/transport-supplier-list',  ['as' => 'transport.transport-supplier-list', 'uses' => 'TransportController@callTransportList']);
Route::match(['get','post'],'/transport/transport-season-list',  ['as' => 'transport.transport-season-list', 'uses' => 'TransportController@callTransportList']);
Route::match(['get','post'],'/transport/transport-markup-list',  ['as' => 'transport.transport-markup-list', 'uses' => 'TransportController@callTransportList']);
Route::match(['get','post'],'/transport/transport-logo-list',  ['as' => 'transport.transport-logo-list', 'uses' => 'TransportController@callTransportList']);

//booking
Route::match(['get','post'],'/booking/itenary-list',  ['as' => 'booking.itenary-list', 'uses' => 'BookingController@callItenaryList']);
Route::match(['get','post'],'/booking/show-itenary/{nItenaryId}',  ['as' => 'booking.show-itenary', 'uses' => 'BookingController@showItenary']);
Route::get('booking/itenary-pdf/{id}',['as' => 'booking.itenary-pdf','uses' => 'BookingController@viewItenaryPdf', 'before' => 'csrf']);
Route::post('booking/update-status', ['before' => 'csrf', 'uses' => 'BookingController@callUpdateStatus']);
Route::post('booking/update-voucher', ['before' => 'csrf', 'uses' => 'BookingController@callUpdateVoucher']);
Route::match(['get','post'],'booking/set-booking-id',['as' => 'booking.set-booking-id','uses' => 'BookingController@setBookingId']);
Route::match(['get','post'],'/booking/booked-tour-list',  ['as' => 'booking.booked-tour-list', 'uses' => 'BookingController@callBookedTourList']);
Route::match(['get','post'],'/booking/booked-tour-detail/{nBookId}',  ['as' => 'booking.booked-tour-detail', 'uses' => 'BookingController@callTourBookingShow']);
Route::post('booking/tour-update-status', ['before' => 'csrf', 'uses' => 'BookingController@callTourUpdateStatus']);

//test route
Route::match(['get','post'],'/user/test',  ['as' => 'user.test', 'uses' => 'UserController@test1']);
//Route::match(['get','post'],'/user/test2/{nId}/{nFrom?}',  ['as' => 'user.test2', 'uses' => 'UserController@test2']);

/*
	Status : Start
	Routes for APIS call from @frontend

*/
Route::prefix('map')->group(function () {
	Route::match(['get','post'],'related/cities/{country_id}','MapApiController@getAllRelatedCities');
	Route::match(['get','post'],'get/all/countries','MapApiController@getAllCountries');
	Route::match(['get','post'],'get/all/countriesBookingPro','MapApiController@getAllCountries');

	Route::match(['get','post'],'city/{id}','MapApiController@getCityByCityId');
	Route::match(['get','post'],'nearby-cities','MapApiController@getNearbyCities');
	Route::match(['get','post'],'get-transport-by-city-ids','MapApiController@getTransportByCitieIDs');
	Route::match(['get','post'],'auto-routes','MapApiController@getFixedCities');
	Route::match(['get','post'],'city','MapApiController@getAllCities');
	Route::match(['get','post'],'city-defaults','MapApiController@findAllDefault');
	Route::match(['get','post'],'get-hotels','MapApiController@get_hotels_by_city_id_v2');
	Route::match(['get','post'],'get-hotel-details','MapApiController@get_hotel_details');
	Route::match(['get','post'],'get-hotel-info','MapApiController@get_hotel_info');
	Route::match(['get','post'],'hotel-price-check','MapApiController@hotel_price_check');
	Route::match(['get','post'],'hotel-booking','MapApiController@hotel_booking');
	Route::match(['get','post'],'get-all-activity','MapApiController@get_all_activity_by_city_id_v2');
	Route::match(['get','post'],'hotel-by-city-id','MapApiController@hotel_by_city_id');
	Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
	Route::any('city/{id}',  'MapApiController@getCityByCityId');
	Route::any('city',  'MapApiController@getAllCities');
	Route::get('hotel-categories','MapApiController@get_all_hotel_categories');
	
});

/* 
	Calls Api Calls from frontend
*/
Route::post('activity',  'MapApiController@getActivity');
Route::get('transport-type',  'MapApiController@get_transport_types');
Route::get('room-type',  'MapApiController@get_room_types');
Route::get('eroam/hotel/{id}',  'MapApiController@viewEroamHotel');
Route::get('eroam/activity/{id}',  'MapApiController@viewEroamActivity');
Route::get('aot/hotel/{SupplierCode}','MapApiController@viewAOTHotel');
Route::match(['get','post'],'viator/category','MapApiController@get_all_label');
Route::match(['get','post'],'suburb-by-city/{city_id}','MapApiController@get_all_suburb_by_city');
Route::get('get_currencies', 'MapApiController@get_currency_values');
Route::get('all-currencies', 'MapApiController@get_all');
Route::get('regions-countries-cities-suburbs', 'MapApiController@getRegions');
Route::get('traveler-options',  'MapApiController@get_traveler_profile_options');
Route::match(['get','post'],'city/hotel','MapApiController@hotel_by_city_id');
/* 
	Calls related Viator
*/
Route::match(['get','post'],'service/search/products', 'ViatorApiController@activities');
Route::match(['get','post'],'service/taxonomy/locations', 'ViatorApiController@location');
Route::match(['get','post'],'service/booking/hotels', 'ViatorApiController@hotels');
Route::match(['get','post'],'service/location', 'ViatorApiController@search_location');	
Route::match(['get','post'],'search/product', 'ViatorApiController@view_activity');
Route::match(['get','post'],'booking/calculateprice', 'ViatorApiController@calculate_price');
Route::match(['get','post'],'save/viator/cagetory', 'ViatorApiController@save_category');
Route::match(['get','post'],'book-activities', 'ViatorApiController@bookActivities');	
Route::match(['get','post'],'get-tour-code', 'ViatorApiController@getTourCode');				
Route::match(['get','post'],'service/tours', 'ViatorApiController@create_tours');	
Route::match(['get','post'],'tour/dates', 'ViatorApiController@get_tour_dates');
Route::match(['get','post'],'labels','MapApiController@get_all_label');

/* 
	Calls related BusBud
*/

Route::match(['get','post'],'busbud/transportList', 'BusBudApiController@getAllTransport');	
Route::match(['get','post'],'bookBusBud', 'BusBudApiController@bookingProcess');
Route::match(['get','post'],'createBusBudCart','BusBudApiController@createBusBudCart');

/* 
	Calls related Itenary Save
*/

Route::resource('saveOrderDetail','ItenaryOrderController@saveOrderDetail');
Route::get('getItineraryDetail/{orderId}','ItenaryOrderController@getItineraryDetail');

/* 
	Calls related Mystifly
*/

Route::get('mystifly/modal-data',     'MystiflyController@get_modal_data');
Route::post('mystifly/flight/search', 'MystiflyController@airLowFareSearchController');
Route::get('mystifly/flight/search1', 'MystiflyController@airLowFareSearchController1');
Route::get('mystifly/fare-rules',     'MystiflyController@fareRules1_1Controller');
Route::get('mystifly/revalidate',     'MystiflyController@airRevalidateController');
Route::post('mystifly/flight/book',   'MystiflyController@bookFlightController');
Route::post('mystifly/ticket/order',  'MystiflyController@ticketOrderController');
Route::get('mystifly/trip-details',   'MystiflyController@tripDetailsController');
Route::post('testMystifly',   'MystiflyController@test');


/* 
	Calls related Mystifly
*/
Route::prefix('user')->group(function () {
	Route::any('check-customer','UserController@check_customer');
	Route::any('create_customer','UserController@customer_registeration');
	Route::any('get-by-id/{id}', 'UserController@user_get_by_id');
	Route::post('update-profile/step1','UserController@update_user_profile_step1');
	Route::post('update-profile/step2','UserController@update_user_profile_step2');
	Route::post('update-profile/step3','UserController@update_user_profile_step3');
	Route::post('update-profile/step4','UserController@update_user_profile_step4');
	Route::any('update-preferences', 'UserController@update_user_preferences');
	Route::any('get-itinerary', 'UserController@get_user_itinerary');
	Route::any('request-password-reset', 'UserController@request_change_password');
	Route::any('check-token', 'UserController@check_token');
	Route::any('reset-password', 'UserController@reset_user_password_from_app');
	Route::any('confirm-registration', 'UserController@confirm_registration');
	Route::any('get-itineraries', 'UserController@get_user_itineraries');
	Route::any('get_trips/{user_id}','UserController@get_user_trips');
});	


/*
	Status : End
	Routes for APIS call from @frontend
*/
