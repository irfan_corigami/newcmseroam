$(document).ready(function() {

	// enable fileuploader plugin
	$('input[name="path"]').fileuploader({

        changeInput: '<div class="fileuploader-input">' +
					      '<div class="fileuploader-input-inner">' +
							  '<h3 class="fileuploader-input-caption"><span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span></h3>' +
						  '</div>' +
					  '</div>' + 
					  '<div class="fileupload-link"><span>Click here to upload images from your computer</span> (Dimension 1000 X 259, max file size, 2mb. Supported file types, .jpeg, .jpg, .png).</div>',
        theme: 'dragdrop',
		upload: {
            url: siteUrl('hotel-images'),
            data: {hotel_id:$('#random_hotel_id').val()},
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            beforeSend: null,
            onSuccess: function(result, item) {
             
                var data = result;
                
				// if success
                // if (data.isSuccess && data.files[0]) {
                //     item.name = data.files[0].name;
                // }
                if(data.success === true){
                    $('.fileuploader-items').hide();
                    item.name = data.data[0].image_name;
				    item.image_id = data.data[0].id;

                    //Append to html
                    var img_html = '<tr data-id="'+data.data[0].id+'">';
                            img_html +='<td><a href="#" class="sortable-icon"><i class="icon-reorder-black"></i></a></td>';
                            img_html +='<td class="table-no-padding">';
                            img_html +='<div class="table-image">';
                            img_url = siteUrl(data.data[0].thumbnail);
                            img_html +='<img src="'+img_url+'" class="img-responsive">';
                            img_html +='</div>';
                            img_html +='</td>';
                            img_name= data.data[0].thumbnail
                            img_name= img_name.split("/");
                            img_html +='<td class="text-left blue-text">'+img_name[img_name.length-1]+'</td>';          
                            img_url = siteUrl(data.data[0].large);       
                            img_html +='<td><a href="'+img_url+'" class="action-icon" download><i class="icon-download"></i></a></td>';          
                            img_html +='<td><a href="javascript://" class="delete_hotel_img action-icon image-click-event" data-action="delete" data-id="'+data.data[0].id+'" data-type="hotel"><i class="icon-delete-forever"></i></a></td>';          
                        img_html +='</tr>';          
                    $('.sortable-list').append(img_html);
                }
				// if warnings
				if (data.hasWarnings) {
					for (var warning in data.warnings) {
						alert(data.warnings);
					}
					
					item.html.removeClass('upload-successful').addClass('upload-failed');
					// go out from success function by calling onError function
					// in this case we have a animation there
					// you can also response in PHP with 404
					return this.onError ? this.onError(item) : null;
				}
                
                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                setTimeout(function() {
                    item.html.find('.progress-bar2').fadeOut(400);
                }, 400);
            },
            onError: function(item) {
				var progressBar = item.html.find('.progress-bar2');
				
				if(progressBar.length > 0) {
					progressBar.find('span').html(0 + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
					item.html.find('.progress-bar2').fadeOut(400);
				}
                
                item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                    '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                ) : null;
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-bar2');
				
                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('span').html(data.percentage + "%");
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            },
            onComplete: null,
        },
		onRemove: function(item) {
			$.post(siteUrl('hotel-images/'+item.image_id),{
				_method: 'DELETE',_token:'{{ csrf_token() }}',
			});
		},
		captions: {
            feedback: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>',
            feedback2: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>',
            drop: '<span><i class="icon-add-photos"></i> Drag Activity Images here to upload.</span>'
        },
	});
});