<div class="page-sidebar-wrapper">
    <nav class="pushy pushy-left" style="visibility: visible;">
        <div class="pushy-height">
            <ul>
                <li class="expand-all"><a href="javascript://"><i class="icon-unfold-more"></i><span>Expand All Menu Items</span></a></li>
                <li class="collapse-all active"><a href="javascript://"><i class="icon-unfold-less"></i><span>Collapse All Menu Items</span></a></li>
                <div class="divider-text">Accounts &amp; Reporting</div>
                <li class=""><a href=""><i class="icon-dashboard"></i><span>Dashboard</span></a></li>

                <li class="pushy-submenu {{ (session('page_name')=='itenary' || session('page_name')=='book_tour') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                    <a href="javascript://"><i class="icon-booking"></i><span>{{ trans('messages.booking_management') }}</span></a>
                    <ul>
                        <li class="pushy_link_cls {{ (session('page_name') == 'itenary' ? 'active' : '') }}"><a href="{{ route('booking.itenary-list') }}">{{ trans('messages.itinerary_bookings') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'book_tour' ? 'active' : '') }}"><a href="{{ route('booking.booked-tour-list') }}">{{ trans('messages.tour_bookings') }}</a></li>
                    </ul>
                </li>
                <li class="{{ (session('page_name') && session('page_name') == 'currency' ? 'active' : '') }}">
                    <a href="{{ URL::to('/') }}"><i class="icon-currency"></i><span>{{ trans('messages.currency_management') }}</span></a>
                </li>

                <div class="divider-text">{{ trans('messages.administration') }}</div>
                <li class="pushy-submenu {{ (session('page_name') == 'licensee' || session('page_name') == 'agent' || session('page_name') == 'admin' || session('page_name') == 'customer') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                    <a href="javascript://">
                        <i class="icon-user"></i>
                        <span>{{ trans('messages.administration') }} {{ session('page_name') }}</span>
                    </a>
                    <ul>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'licensee' ? 'active' : '') }}">
                            <a href="{{ route('user.list',['sType' => config('constants.USERTYPELICENSEE') ]) }}">
                                {{ trans('messages.licensee') }}
                            </a>
                        </li>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'agent' ? 'active' : '') }}">
                            <a href="{{ route('user.list',['sType' => config('constants.USERTYPEAGENT') ]) }}">
                                {{ trans('messages.agent') }}
                            </a>
                        </li>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'admin' ? 'active' : '') }}">
                            <a href="{{ route('user.list',['sType' => config('constants.USERTYPEADMIN') ]) }}">
                                {{ trans('messages.admin') }}
                            </a>
                        </li>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'customer' ? 'active' : '') }}">
                            <a href="{{ route('user.list',['sType' => config('constants.USERTYPECUSTOMER') ]) }}">
                                {{ trans('messages.customer') }}
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="{{ (session('page_name') && session('page_name') == 'label' ? 'active' : '') }}">
                    <a href="{{ route('common.label-list') }}">
                        <i class="icon-label"></i>
                        <span>{{ trans('messages.label') }}</span>
                    </a>
                </li>

                <li class="pushy-submenu {{ (session('page_name') == 'region' || session('page_name') == 'city' || session('page_name') == 'country' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}"><a href="javascript://"><i class="icon-geodata"></i><span>{{ trans('messages.geo_data') }}</span></a>
                    <ul>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'region' ? 'active' : '') }}"><a href="{{ route('common.region-list') }}">{{ trans('messages.manage_region') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'country' ? 'active' : '') }}"><a href="{{ route('common.country-list') }}">{{ trans('messages.manage_country') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') && session('page_name') == 'city' ? 'active' : '') }}"><a href="{{ route('common.city-list') }}">{{ trans('messages.manage_city') }}</a></li>
                    </ul>
                </li>

                <li class="{{ (session('page_name') && session('page_name') == 'coupon' ? 'active' : '') }}">
                    <a href="{{ route('common.coupon-list') }}"><i class="fa fa-tags"></i><span>{{ trans('messages.coupon_management') }}</span></a>
                </li>

                <div class="divider-text">{{ trans('messages.product_management') }}</div>
                <li class="pushy-submenu"><a href="javascript://"><i class="icon-hotel"></i><span>{{ trans('messages.acomodation_management') }}</span></a>
                    <ul> 
                        <li class="pushy_link_cls"><a href="{{ route('acomodation.hotel-list') }}">{{ trans('messages.acomodation_management') }}</a></li>
                        <li class="pushy_link_cls "><a href="{{ URL::to('/') }}">{{ trans('messages.manage_suppliers') }}</a></li>
                        <li class="pushy_link_cls "><a href="{{ URL::to('/') }}">{{ trans('messages.manage_mapping') }}</a></li>
                        <li class="pushy_link_cls "><a href="{{ URL::to('/') }}">{{ trans('messages.manage_seasons') }}</a></li>
                        <li class="pushy_link_cls "><a href="{{ URL::to('/') }}">{{ trans('messages.manage_prices') }}</a></li>
                        <li class="pushy_link_cls "><a href="{{ URL::to('/') }}">{{ trans('messages.acomodation_management') }}</a></li>                  
                    </ul>
                </li>
                <li class="pushy-submenu {{ (session('page_name')=='transport' || session('page_name')=='transport_supplier' || session('page_name')=='transport_season' || session('page_name')=='transport_mark' || session('page_name')=='transport_logo' ) ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                    <a href="javascript://"><i class="icon-transport"></i><span>{{ trans('messages.manage_transport') }}</span></a>
                    <ul>      
                        <li class="pushy_link_cls {{ (session('page_name') == 'transport' ? 'active' : '') }}"><a href="{{ route('transport.transport-list') }}">{{ trans('messages.manage_transport') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'transport_supplier' ? 'active' : '') }}"><a href="{{ route('transport.transport-supplier-list') }}">{{ trans('messages.manage_suppliers') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'transport_season' ? 'active' : '') }}"><a href="{{ route('transport.transport-season-list') }}">{{ trans('messages.manage_seasons') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'transport_mark' ? 'active' : '') }}"><a href="{{ route('transport.transport-markup-list') }}">{{ trans('messages.manage_markup') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'transport_logo' ? 'active' : '') }}"><a href="{{ route('transport.transport-logo-list') }}">{{ trans('messages.manage_logo') }}</a></li>
                    </ul>
                </li>
                <li class="pushy-submenu {{ (session('page_name')=='activity' || session('page_name')=='activity_supplier' || session('page_name')=='activity_season' || session('page_name')=='activity_mark') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                    <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.activity_management') }}</span></a>
                    <ul>  
                        <li class="pushy_link_cls {{ (session('page_name') == 'activity' ? 'active' : '') }}">
                            <a href="{{ route('activity.activity-list') }}">{{ trans('messages.manage_activity') }}</a>
                        </li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_supplier' ? 'active' : '') }}"><a href="{{ route('activity.activity-supplier-list') }}">{{ trans('messages.manage_suppliers') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_season' ? 'active' : '') }}"><a href="{{ route('activity.activity-season-list') }}">{{ trans('messages.manage_seasons') }}</a></li>
                        <li class="pushy_link_cls {{ (session('page_name') == 'activity_mark' ? 'active' : '') }}"><a href="{{ route('activity.activity-markup-list') }}">{{ trans('messages.manage_markup') }}</a></li>
                    </ul>
                </li>

                <li class="{{ (session('page_name') && session('page_name') == 'route' ? 'active' : '') }}">
                    <a href="{{ route('route.route-list') }}"><i class="icon-route"></i><span>{{ trans('messages.route_plans') }}</span></a>
                </li>

                <div class="divider-text">{{ trans('messages.tour_management') }}</div>
                <li class="pushy-submenu {{ (session('page_name')=='tourlist' || session('page_name')=='addtour' || session('page_name')=='tourtypelogo') ? 'pushy-submenu-open active open_pushy_cls' : 'pushy-submenu-closed' }}">
                    <a href="javascript://"><i class="icon-activity"></i><span>{{ trans('messages.tour_management') }}</span></a>
                    <ul> 
                        <li class="pushy_link_cls {{ (session('page_name') == 'tourlist' ? 'active' : '') }}"><a href="{{ route('tour.tour-list') }}">{{ trans('messages.search_tour') }} </a></li>
                        <li class="pushy_link_cls {{(session('page_name') == 'addtour' ? 'active' : '') }}"><a href="{{ route('tour.tour-create') }}">{{ trans('messages.add_tour') }} </a></li>
                        <li class="pushy_link_cls {{(session('page_name') == 'tourtypelogo' ? 'active' : '') }}"><a href="{{ route('tour.tour-type-logo-list') }}">{{ trans('messages.tour_type_logo') }}</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</div>